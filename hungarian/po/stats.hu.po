# SOME DESCRIPTIVE TITLE.
# Copyright ©
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2016-01-13 19:43+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Debian web oldal fordítási statisztikák"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "%d oldalt kell lefordítani."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "%d ájtot kell lefodítani."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "%d szót kell lefodítani."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Rossz fordítási verzió."

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Az a fordítás nagyon elavult."

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Az eredeti frissebb, mint ez a fordítás"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Az eredeti már nem létezik"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "találatok száma nem ismert"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "találatok"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Kattints a diffstat adatok begyűjtéséhez"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "Készült ezzel: "

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Fordítási összefoglaló"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Nincs lefordítva"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Elavult"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Lefordított"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Naprakész"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "fájlok"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "bájtok"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Megjegyzés: az oldalak listája népszerűség szerint van rendezve. Menj az "
"egérrel az oldal neve fölé, hogy lásd a találatok számát."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Elavult fordítások"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "File"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Komment"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "Git parancssor"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Fordítás"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Karbantartó"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Státusz"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Fordító"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Dátum"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Nem lefordított általános oldalak"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Lefordítatlan általános oldalak"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Nem lefordított új elemek"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Lefordítatlan új elemek"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Nem lefordított szaktanácsadói/felhasználói oldalak"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Lefordítattlan szaktanácsadói/felhasználói oldalak"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Nem lefordított nemzetközi oldalak"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Lefordítatlan nemzetközi oldalak"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Lefordított oldalak (naprakész)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Lefordított sablonok (PO fájlok)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "PO Fordítási statisztikák"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Zavaros"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Lefordítatlan"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Összesen"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Összesen:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Lefordított web oldalak"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Fordítási Statisztikák a Page Count által"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Nyelv"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Fordítások"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Lefordított web oldalak (méret szerint)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Fordítási statisztikák az oldal mérete szerint"
