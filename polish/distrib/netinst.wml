#use wml::debian::template title="Instalacja Debiana przez Internet" BARETITLE=true
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Ta metoda instalacji Debiana wymaga działającego dostępu do Internetu <em>podczas</em> instalacji.
W porównaniu do innych metod pobiera się mniej danych, a proces można dostosować do własnych wymagań.
Obsługiwane są połączenia Ethernet lub bezprzewodowe.
Wewnętrzne karty ISDN niestety <em>nie</em> są obsługiwane.</p>

<p>Istnieją trzy sposoby instalacji przez sieć:</p>

<toc-display />

<div class="line">

<div class="item col50">
<toc-add-entry name="smallcd">Małe płyty CD lub pamięć USB</toc-add-entry>

<p>Poniżej znajdują się pliki obrazów. 
Wybierz architekturę swojego procesora:</p>

<stable-netinst-images />
</div>

<div class="clear"></div>

</div>

<p>Szczegóły są dostępne na stronie: <a href="../CD/netinst/">instalacja
sieciowa z małej płyty CD</a>.</p>

<div class="line">

<div class="item col50">
<toc-add-entry name="verysmall">Najmniejsze płytki, napędy USB itp.</toc-add-entry>

<p>Możesz pobrać niewielki plik obrazu, odpowiedni dla napędów USB lub
podobnych urządzeń, zapisać go na nośniku i rozpocząć instalację uruchamiając z
niego komputer.</p>

<p>Istnieje pewna różnorodność, jeśli chodzi o wspracie instalacji z
małych mediów w zależności od architektury.
</p>

<p>Szczegóły są dostępne w 
<a href="$(HOME)/releases/stable/installmanual">podręczniku instalacji
dla Twojej architektury</a>, zwłaszcza w rozdziale
<q>Obtaining System Installation Media</q>.</p>

<p>
Poniżej znajdują się linki 
do dostępnych plików obrazów (prosimy zajrzeć do pliku MANIFEST, gdzie
znajdują się informacje na ten temat):
</p>

<stable-verysmall-images />
</div>

<div class="item col50 lastcol">
<toc-add-entry name="netboot">Bootowanie przez sieć</toc-add-entry>

<p>Można skonfigurować serwer TFTP i DHCP (lub BOOTP albo RARP), tak aby
obsługiwał media instalacyjne w sieci lokalnej. Jeśli BIOS komputera obsługuje
tę możliwość, możesz wystartować na nim instalację Debiana z sieci (za pomocą
PXE i TFTP) i zainstalować w ten sposób resztę systemu.</p>

<p>Nie wszystkie maszyny posiadają możliwość bootowania z sieci. Ze względu
na dodatkową pracę konieczną do wykonania dla skonfigurowania instalacji,
metoda ta nie jest rekomendowana dla niedoświadczonych użytkowników.</p>

<p>Szczegóły są dostępne w 
<a href="$(HOME)/releases/stable/installmanual">podręczniku instalacji
dla Twojej architektury</a>, zwłaszcza w rozdziale
<q>Preparing Files for TFTP Net Booting</q>.</p>

<p>Poniżej znajdują się linki do plików obrazów (prosimy zajrzeć do pliku
MANIFEST).</p>

<stable-netboot-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Jeżeli jakikolwiek sprzęt w systemie <strong>wymaga załadowania niewolnego 
oprogramowania (firmware)</strong> wraz ze sterownikiem, można użyć jednego z 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archiwów z popularnymi pakietami oprogramowania</a> lub pobrać 
<strong>nieoficjalny</strong> obraz zawierający te <strong>niewolne</strong> oprogramowanie.
Instrukcje jak użyć archiwów i ogólne informacje o ładowaniu firmware podczas instalacji 
znajduje się w  <a href="../releases/stable/amd64/ch06s04">Podręczniku Instalacji</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">nieoficjalne
obrazy instalacyjne dla wersji <q>stabilnej</q> z zawartym firmwarem</a>
</p>
</div>