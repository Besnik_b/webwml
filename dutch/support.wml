#use wml::debian::template title="Ondersteuning"
#use wml::debian::translation-check translation="9c90f0ed4b82c5e7504045fff4274d38f9b9997b"
#use wml::debian::toc

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Debian en zijn ondersteuning worden door een vrijwilligersgemeenschap gedaan.

Indien deze ondersteuning vanuit de gemeenschap niet beantwoordt aan uw
noden, kunt u onze <a href="doc/">documentatie</a> lezen of een <a href="consultants/">consultant</a> inhuren.

<toc-display />

<toc-add-entry name="irc">Directe on-line hulp via IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> is
een manier om rechtstreeks met mensen van over de hele wereld te
chatten. Op het <a href="https://www.oftc.net/">OFTC IRC-netwerk</a> bestaan
kanalen die aan Debian gewijd zijn.</p>

<p>Om een verbinding te maken heeft u een IRC-programma nodig. Enkele
van de meest gebruikte programma's zijn
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> en
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>.
Al deze programma's zijn beschikbaar als Debian-pakketten. OFTC
biedt ook <a href="https://www.oftc.net/WebChat/">WebChat</a>
als mogelijkheid. Het is een web-interface die u toelaat
via een browser een verbinding te maken met IRC, waardoor het
niet nodig is om lokaal een programma te installeren.</p>

<p>Als u een IRC-programma heeft geïnstalleerd, moet u het opdragen
een verbinding te maken met de server.
In de meeste programma's kunt u dat doen
door het volgende commando in te tikken:</p>

<pre>
/server irc.debian.org
</pre>

<p>Bij sommige programma's (zoals irssi) zult u in de plaats daarvan
het volgende moeten typen:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Als u eenmaal bent verbonden, kunt het kanaal <code>#debian-nl</code>
binnengaan door het volgende te typen:

<pre>/join #debian-nl</pre>

<p>Of, voor ondersteuning in het Engels:</p>

<pre>
/join #debian
</pre>

<p>NB: Grafische programma's zoals HexChat hebben vaak een andere
interface waar u de server en het chatkanaal kunt invoeren.</p>

<p>Vanaf dan bevindt U zich in de vriendelijke gemeenschap van bewoners
van <code>#debian</code>. Al uw vragen over Debian kunt u hier
stellen. Een lijst van veel gestelde vragen (FAQ) op dit kanaal, kunt u
vinden op <url "https://wiki.debian.org/DebianIRC">.</p>

<p>Er is ook een aantal andere IRC-netwerken waar u over Debian kunt
chatten.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Mailinglijsten</toc-add-entry>

<p>Debian wordt via een gedistribueerd ontwikkelingsmodel
gemaakt door ontwikkelaars uit de hele wereld.
E-mail heeft daarom de voorkeur als communicatiemiddel om allerlei aan
Debian gerelateerde zaken te bediscussiëren. Veel van de
communicatie tussen ontwikkelaars van Debian en gebruikers verloopt via een
aantal mailinglijsten.</p>

<p>Er zijn een aantal publiek beschikbare mailinglijsten. Voor meer
informatie daarover, kunt u terecht op de pagina over
<a href="MailingLists/">Debian mailinglijsten</a>.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<p>
Voor gebruikersondersteuning in het Nederlands neemt u best contact op
met de <a
href="https://lists.debian.org/debian-user-dutch/">debian-user-dutch
mailinglijst</a>.
</p>

<p>
Voor gebruikersondersteuning in het Engels neemt u contact met de <a
href="https://lists.debian.org/debian-user/">debian-user
mailinglijst</a>.
</p>

<p>
Voor ondersteuning in andere talen kunt u gaan kijken in de <a
href="https://lists.debian.org/users.html">index van mailinglijsten voor
gebruikers</a>.
</p>

<p>Er zijn uiteraard vele andere mailinglijsten, gewijd aan een aspect van
het uitgestrekte Linux ecosysteem, die niet specifiek op Debian zijn gericht.
Gebruik uw favoriete zoekmachine om lijsten te vinden die aansluiten op uw
behoeften.</p>


<toc-add-entry name="usenet">Usenet nieuwsgroepen</toc-add-entry>

<p>Een groot aantal van onze <a href="#mail_lists">mailinglijsten</a>
kan in de vorm van nieuwsgroepen worden gelezen, in de
<kbd>linux.debian.*</kbd> hiërarchie.  Dit is ook mogelijk via
een web-interface zoals <a href="http://groups.google.com/d/homeredir">Google
Discussiegroepen</a>.</p>


<toc-add-entry name="web">Websites</toc-add-entry>

<h3 id="forums">Forums</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="http://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="http://forums.debian.net">Debian User Forums</a> is een webportaal
waarop u met Debian verband houdende zaken kunt bespreken en vragen over Debian
kunt stellen, die dan beantwoord kunnen worden door andere gebruikers.</p>


<toc-add-entry name="maintainers">Pakketontwikkelaars
contacteren</toc-add-entry>

<p>Er zijn twee manieren om pakketontwikkelaars te contacteren. Als u een
ontwikkelaar wilt mailen over een bug, kunt u simpelweg een bugrapport
versturen (zie de paragraaf over het bugvolgsysteem hieronder). De
ontwikkelaar krijgt een kopie van uw rapport.</p>

<p>Als u over een ander onderwerp met de pakketontwikkelaar in contact wilt
treden, kunt u de speciale e-mailaliassen gebruiken die voor elk pakket
gemaakt zijn. E-mailberichten die worden gestuurd naar
<tt>&lt;<EM>pakketnaam</EM>&gt;@packages.debian.org</tt> wordt
doorgestuurd naar de ontwikkelaar die verantwoordelijk is voor dat
pakket.</p>


<toc-add-entry name="bts" href="Bugs/">Het Bug Tracking
System (bugvolgsysteem)</toc-add-entry>

<p>De Debian distributie heeft een bugvolgsysteem dat
informatie verzamelt over problemen (bugs) die worden gerapporteerd door
gebruikers en ontwikkelaars. Elke bug krijgt een nummer en blijft actief
totdat het probleem is opgelost.</p>

<p>Om een probleem te rapporteren kunt u de bugpagina hieronder lezen.
We bevelen het gebruik aan van het Debian pakket <q>reportbug</q>
om automatisch een bugrapport in te dienen.</p>

<p>Informatie over het rapporteren
van bugs, het bekijken van actieve bugs en het bugvolgsysteem
in het algemeen is te vinden
op de <a href="Bugs/">webpagina's over het Bug Tracking System</a>.</p>


<toc-add-entry name="doc" href="doc/">Documentatie</toc-add-entry>

<p>Een belangrijk onderdeel van elk besturingssysteem is documentatie, de
technische handleidingen die de werking en het gebruik van programma's
beschrijven. Als onderdeel van haar inspanningen om een vrij besturingssysteem
van hoge kwaliteit te maken, doet het Debian-project er alles aan om al haar
gebruikers te voorzien van goede documentatie in een gemakkelijk toegankelijke
vorm.</p>

<p>Zie de <a href="doc/">documentatiepagina</a> voor een lijst van
Debian-handleidingen en andere documentatie, inclusief
de installatiehandleiding, de Debian FAQ en andere handleidingen
voor gebruikers en ontwikkelaars.</p>


<toc-add-entry name="consultants" href="consultants/">Consultants</toc-add-entry>

<p>Debian is vrije software en biedt gratis hulp aan via mailinglijsten.
Sommige mensen hebben daar echter geen tijd voor of hebben speciale
noden en willen iemand inhuren om hun Debian-systeem
te onderhouden of om er extra functionaliteit aan toe te voegen.
Op de <a href="consultants/">consultants pagina</a> kunt een
lijst van personen/bedrijven vinden.</p>


<toc-add-entry name="release" href="releases/stable/">Bekende problemen</toc-add-entry>

<p>Eventuele bekende beperkingen en ernstige problemen van de huidige stabiele
distributie worden beschreven op <a href="releases/stable/">de releasepagina's</a>.</p>

<p>Let vooral op de <a href="releases/stable/releasenotes">aantekeningen bij
de release</a> en de <a href="releases/stable/errata">errata</a>.</p>

