# translation of others.po to Dutch
# Templates files for webwml modules
# Copyright (C) 2003,2004 Software in the Public Interest, Inc.
#
# Frans Pop <elendil@planet.nl>, 2006, 2007.
# Jeroen Schot <schot@a-eskwadraat.nl, 2011.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2014-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: dutch/po/others.nl.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-02-08 15:47+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 3.30.1\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Hoek voor nieuwe leden"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Stap 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Stap 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Stap 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Stap 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Stap 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Stap 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Stap 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Checklist voor kandidaten"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Zie <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/"
"index.fr.html</a> (alleen in het Frans) voor meer informatie."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Meer informatie"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Zie <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (alleen in het Spaans) voor meer informatie."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telefoon"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Adres"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Producten"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "T-shirts"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "hoofddeksels"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "stickers"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "mokken"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "andere kleding"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "polohemden"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frisbees"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "muismatten"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "badges"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "basketdoelen"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "oorbellen"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "koffers"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "paraplu's"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "kussenslopen"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "sleutelhangers"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Zwitserse zakmessen"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "USB-sticks"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "draagriemen"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "andere"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Beschikbare talen:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Internationale levering:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "binnen Europa"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "Oorspronkelijk land:"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Schenkt geld aan Debian"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr "Geld wordt gebruikt om lokale vrijesoftware-evenementen te organiseren"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Met&nbsp;\"Debian\""

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Zonder&nbsp;\"Debian\""

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Encapsulated PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Draait op Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Draait op Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Draait op Debian]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (mini-button)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "zelfde als bovenstaande"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Hoe lang gebruikt u Debian al?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Bent u een ontwikkelaar van Debian?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "In welke domeinen van Debian bent u actief?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Waardoor raakte u geïnteresseerd om met Debian te werken?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Heeft u tips voor vrouwen die graag meer actief betrokken willen worden bij "
"Debian?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "Bent u actief in andere technologiegroepen van vrouwen? Welke?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Iets meer over uzelf..."

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Version"
#~ msgstr "Versie"

#~ msgid "Status"
#~ msgstr "Status"

#~ msgid "Package"
#~ msgstr "Pakket"

#~ msgid "ALL"
#~ msgstr "ALLE"

#~ msgid "Unknown"
#~ msgstr "Onbekend"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "BAD?"
#~ msgstr "SLECHT?"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "BAD"
#~ msgstr "SLECHT"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Old banner ads"
#~ msgstr "Oude reclamebalken"

#~ msgid "Download"
#~ msgstr "Downloaden"

#~ msgid "Unavailable"
#~ msgstr "Niet beschikbaar"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />Onbekend"

#~ msgid "No images"
#~ msgstr "Geen images"

#~ msgid "No kernel"
#~ msgstr "Geen kernel"

#~ msgid "Not yet"
#~ msgstr "Nog niet"

#~ msgid "Building"
#~ msgstr "Bouwt"

#~ msgid "Booting"
#~ msgstr "Start op"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (kapot)"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "Working"
#~ msgstr "Werkend"
