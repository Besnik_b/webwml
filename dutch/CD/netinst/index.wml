#use wml::debian::cdimage title="Netwerkinstallatie vanaf een minimale cd"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="e01f641181acd20b6afc24e482e4360c075e4712"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<p>Een <q>netwerkinstallatie</q>- of <q>netinst</q>-cd is één cd die u in staat
stelt om het volledige besturingssysteem te installeren. Deze ene cd bevat
enkel de minimale hoeveelheid aan software om het basissysteem te installeren
en de overige pakketten op te halen via het internet.</p>

<p><strong>Wat is beter voor mij &mdash; de minimale opstartbare cd of de
volledige cd’s?</strong>. Dat hangt ervan af, maar wij denken dat in de meeste
gevallen de minimale cd beter is. U hoeft alleen die pakketten die u tijdens de
installatie voor uw machine selecteert, te downloaden. Dit spaart zowel tijd als
bandbreedte. De volledige cd’s zijn geschikter als u meerdere machines wilt
installeren, of een machine zonder goedkope internetverbinding.</p>

<p><strong>Welke types van netwerkverbindingen worden ondersteund tijdens de
installatie?</strong>
De netwerkinstallatie gaat ervan uit dat u een verbinding met internet heeft.
Hierbij worden verschillende manieren ondersteund, zoals een analoge
PPP-inbelverbinding, ethernet en WLAN (met enkele beperkingen), maar geen ISDN
&mdash; sorry!</p>

<p>De volgende minimale opstartbare cd-images kunnen gedownload worden:</p>

<ul>

<li>Officiële "netinst" images voor de "stable" release &mdash;
<a href="#netinst-stable">zie hieronder</a></li>

<li>Images voor de <q>testing</q> release, zowel versies die dagelijks
gebouwd worden als momentopnames waarvan geweten is dat ze zeker werken.
Zie de <a href="$(DEVEL)/debian-installer/">Debian-Installer pagina</a>.</li>

</ul>

<h2 id="netinst-stable">Officiële netinstallatie-images voor de
<q>stable</q> release</h2>

<p>Tot 300&nbsp;MB groot. Dit image bevat het installatiesysteem en een klein
aantal pakketten dat de installatie van een (erg) basaal systeem toelaat.</p>

<div class="line">
<div class="item col50">
<p><strong>
netinst cd-image (normaal 150-300 MB, afhankelijk van de architectuur)
</strong></p>
  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>
netinst cd-image (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)
</strong></p>
  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Voor informatie over deze bestanden en hoe ze te gebruiken, zie de
<a href="../faq/">FAQ</a>.</p>

<p>Vergeet niet om ook de
<a href="$(HOME)/releases/stable/installmanual">gedetailleerde informatie over
het installatieproces</a> te bekijken.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<h2><a name="firmware">Niet-officiële netinst-images die niet-vrije firmware bevatten</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Als op uw systeem bepaalde hardware <strong>vereist dat niet-vrije firmware
geladen wordt</strong> samen met het stuurprogramma voor het apparaat, kunt u
een van de <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">tar-archieven met gangbare firmware-pakketten</a>
gebruiken of een <strong>niet-officieel</strong> image downloaden dat deze
<strong>niet-vrije</strong> firmware bevat. Instructies over het gebruik van
tar-archieven en algemene informatie over het laden van firmware tijdens een
installatie kunt u vinden in de
<a href="../../releases/stable/amd64/ch06s04">Installatiehandleiding</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">niet-officiële installatie-images voor de release
<q>stable</q> die firmware bevatten</a>
</p>
</div>

