#use wml::debian::template title="Debian GNU/Hurd &mdash; Configuratie" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="ad90efc904807b8db1f35dd7d05b950182b3c1fa"

<h1>Configuratie van Debian GNU/Hurd</h1>

<p>
Het doel van dit document is een aantal gemakkelijke en relatief
pijnloze instructies te verstrekken voor de configuratie van Debian GNU/Hurd
met een minimale inspanning.
</p>

<p>
Het is gebaseerd op <q>The Hurd Installation Guide</q> van Neal H. Walfield.
Hartelijk dank aan Neal voor zijn bijdrage.
</p>

<h2>Overzicht </h2>

<p>
GNU is qua aard vergelijkbaar met elk Unix-achtig systeem: na het inloggen
krijgt de gebruiker een shell en het bekende Unix VFS (virtual filesystem -
virtueel bestandssysteem) te zien. Hoewel GNU POSIX-compatibel probeert te
zijn, is het <q>geen Unix</q>. GNU/Hurd bouwt voort op veel van de
Unix-concepten en breidt ze uit om ofwel nieuwe functionaliteit toe te voegen
of om wat als gebreken in het oorspronkelijke ontwerp werd gezien, te
herstellen. Het meest opvallende verschil zijn zogenaamde translators
(vertalers), programma's in de gebruikersruimte die communiceren met het VFS.
Deze bestandssystemen leven niet in de kernel en dienen evenmin uitgevoerd te
worden als root-gebruiker; ze hebben alleen toegang nodig tot de externe opslag
en het <code>mount point</code> (aankoppelpunt). Een ander verschil is dat
processen, in plaats van één enkele op het moment van creatie vastgelegde
gebruikersidentiteit te hebben, identiteitskentekens hebben die los staan van
het proces, d.w.z. dat deze met de juiste toestemming van een autoriteit kunnen
worden toegevoegd of vernietigd.
</p>

<p>
Om uzelf comfortabel te voelen in GNU is het vereist dat u bekend bent met de
Unix-omgeving (en in het bijzonder met de GNU-gebruikersomgeving die men in
populaire varianten zoals GNU/Linux vindt). Ervaring hebben met de
Debian-hulpmiddelen zal ook van onschatbare waarde blijken voor de configuratie
en het onderhoud van een GNU/Hurd-computer.
</p>

<p>
Met deze gids trachten we de installatie van GNU/Hurd zo pijnloos mogelijk te
maken. Mochten er fouten in staan, dan zijn die zeker op het conto van de
auteur te schrijven. Wees zo vriendelijk hem deze te melden samen met eventuele
andere suggesties of kritiek; ze worden allemaal in dank aangenomen.
</p>

#<h2> 2. Real Estate or Finding A Home </h2>
#
#<p>
#If you do not have an available partition or an extra hard drive, this can be
#the longest step. In this case, you will need to repartition the hard drive.
#One solution is to use GNU's partition editor,
#<a href="https://packages.debian.org/parted">Parted</a>. It features not only
#basic partition editing but also partition resizing and moving functionality.
#The manual is quite complete and includes several tutorials. Note that also
#there is a <code>63 GNU HURD or SysV</code> partition type, it should not be
#used or else the installation CD will not find it. Just use <code>83
#Linux</code>
#</p>
#
#<p>
#The Hurd supports several extensions to the ext2fs filesystem format. Foremost
#among these are passive translators and a fourth set of permission bits for
#unknown users (users without an identity, not the other user). To use these
#extensions, the owner of the partition must be set to <code>hurd</code>.
#<code>mke2fs</code>, unless specifically overridden on the command line, will
#set the owner to whatever kernel it is running on. As the Hurd will diligently
#respect this setting, care must be taken to set this appropriately or the Hurd
#will fail in subtle ways. Be aware that even if a file system is owned by a
#particular kernel, others may still use it; they just may not be able to use
#certain extensions.
#</p>
#
#<p>
#To create a filesystem, use <code>mke2fs</code> and pass it <q><var>-b 4096
#-I 128 -o hurd</var></q> to designate the Hurd as the owner of the new file
#system. For instance, assuming the partition is <tt><q>/dev/hda1</q></tt>:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# mke2fs -b 4096 -I 128 -o hurd /dev/hda1
#</pre></td></tr></table>
#
#
#<h2> 3. The Boot Loader </h2>
#
#<p>
#The bootloader of the GNU system is Grub, the GRand Unified Boot loader,
#and it is its main purpose to boot the GNU system core (Mach and the Hurd).
#Any boot loader that supports the multiboot standard, though, may be used to
#load Mach and the Hurd. At the moment (that we're aware of), GNU Grub is the
#only bootloader that supports such standard.
#</p>
#
#<p>
#A word about Grub. Unlike traditional boot loaders on the x86, such as LILO,
#Grub is very powerful. It has a command line interface, bootp, dummy terminal
#support and a plethora of other features. In addition, it can boot almost any
#kernel. If you have ever booted an alpha or sparc, you will understand what
#Grub can do. Therefore, do not be scared: Grub is better. You will like it.
#You will not go back.
#</p>
#
#<p>
#It is probably better if you install
#<a href="https://packages.debian.org/grub2">Grub</a> before installing the
#Hurd, but you can always install it onto your hard drive at a later date.
#</p>
#
#
#<h2> 4. Cross Install </h2>
#
#<p>
#The next step is to get a base system. There are several alternatives, if you
#are running a Debian system you can use the package crosshurd. Otherwise you
#can get an updated base system tarball from any of the mirrors listed at
#<url "https://wiki.debian.org/DebianPorts/Mirrors" />.
#</p>
#
#<p>
#The tarball is set up to extract everything into the current directory.
#After the filesystem is mounted, the archive can be extracted.
#Assuming that the filesystem is on <tt><q>/dev/hda2</q></tt>, the mount point
#is <tt><q>/gnu</q></tt> and archive is in current user's home directory, the
#following is required:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# mount -t ext2 /dev/hda2 /gnu
# \# cd /gnu
# \# tar --same-owner -xvjpf ~/gnu.tar.bz2
#</pre></td></tr></table>
#
#
#<h2> 5. Booting GNU/Hurd </h2>
#
#<p>
#All is now in readiness to boot GNU/Hurd for the first time. Note that depending
#on your version of GRUB, small details with vary. Please make sure whether you
#have GRUB1 (aka legacy aka 0.95 and later up to 0.97), or GRUB2 (aka 1.95 and
#later). Examples below will provide both versions, make sure to pick the right
#one.
#</p>
#
#<p>
#Please also note that some problems have been reported when booting with only
#128MB memory, because of swap not being setup yet at this stage. Make sure to
#provide at least 256MB memory.
#</p>
#
#<p>
#If your system already has GRUB installed, just reboot it. Else, you need to use
#a Grub boot disk. On Debian, the grub-disk (grub1) or grub-rescue-pc (grub2)
#packages provides CD and Floppy disk images. Make sure that the Grub boot disk
#is in the drive, reboot. If all goes well, either a Grub menu or command line
#will be displayed. If the menu has a <tt><q>GNU/Hurd</q></tt> entry, you will
#probably be able to just reuse it to save a lot of typing burden. For now, if
#presented with a menu, press <kbd>c</kbd> to go to the command line.
#</p>
#
#<p>
#First, GNU Mach needs to be loaded. This requires knowing the filesystem
#and the path to GNU Mach. Grub uses a partition nomenclature that is a bit
#different from both Linux and the Hurd: both IDE and SCSI drives are named
#<tt><q>(hdN,M)</q></tt>. <code>N</code> is the drive number (zero based) as
#enumerated by the BIOS. That is, Grub makes no distinction between IDE and SCSI
#disks. <code>M</code> identifies the partition on the drive. It is zero based
#in grub1 but one based in grub2. If this sounds confusing, relax:  Grub is also
#helpful.
#</p>
#
#<p>
#To determine on which filesystem a particular file resides, Grub provides the
#<code>find</code> command. When this command is issued along with a filename,
#Grub searches on each filesystem for the specified file and prints where it was
#found. For example, to search for the kernel, <tt><q>/boot/gnumach.gz</q></tt>:
#</p>
#
#<table>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub1&#62; find /boot/gnumach.gz
#   (hd0,0)
#</pre></td></tr>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub2&#62; search -f /boot/gnumach.gz
#   (hd0,1)
#</pre></td></tr>
#</table>
#
#<p>
#Here, Grub1 indicates that <tt><q>/boot/gnumach.gz</q></tt> is on
#<tt><q>(hd0,0)</q></tt> and Grub2, on <tt><q>(hd0,1)</q></tt> (remember the difference
#in partition numbering between Grub1 and Grub2).  To save you a couple of
#typing, set Grub's root to that value:
#</p>
#
#<table>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub1&#62; root (hd0,0)
#</pre></td></tr>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub2&#62; set root=(hd0,1)
#</pre></td></tr>
#</table>
#
#<p>
#Before loading the kernel, at least one option, the root partition, must be
#specified on the command line. This will be used by the Hurd itself (i.e. not
#Grub). As such, it must be in terms that the Hurd can understand.
#</p>
#
#<p>
#GNU Mach enumerates disks starting at zero. IDE drives are prefixed with
#<code>hd</code>, while SCSI disks are prefixed with <code>sd</code>. Like
#Linux, drives are number by their position on the controller. For instance, the
#primary master is <code>hd0</code> and the secondary slave is <code>hd3</code>.
#Partitions use the BSD slice naming convention and append <code>sM</code> to the
#drive name to indicate a given partition. Note that <code>M</code> is a one, not
#zero, based index. The slice number is simple to calculate: if you have Grub2,
#just reuse the same index, if you have Grub1, just increment what was used for
#Grub1 by one.
#</p>
#
#<p>
#Since the Hurd has not yet been configured, it must be started in single user
#mode. Adding a <q><var>-s</var></q> to the kernel command line is all that is
#required.
#</p>
#
#<p>
#To sum it up, assuming that the first drive (i.e. <tt><q>(hd0)</q></tt>) is the
#master on the master controller, we would have:
#</p>
#
#<table>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub1&#62; kernel /boot/gnumach.gz root=device:hd0s1 -s
#   [Multiboot-elf, ...]
#</pre></td></tr>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub2&#62; multiboot /boot/gnumach.gz root=device:hd0s1 -s
#</pre></td></tr>
#</table>
#
#<p>
#Next, the root filesystem server and the <code>exec</code> server must be
#loaded. This is done using Grub's boot module capability. The ${var} are
#filled in by GNU Mach. The arguments are used by the Hurd to indicate what
#type of information is being provided. Since the ext2fs command line is very
#long, it can be broken up by escaping the newline character in the normal Unix
#fashion. Be sure that there is not space after the antislash at the end of each
#line. Also be sure to differentiate <kbd>{</kbd> and <kbd>}</kbd> from <kbd>
#(</kbd> and <kbd>)</kbd>. Mind the subttle differences between Grub1 and
#Grub2: Grub2 needs that the filename be repeated and quotes need
#to be used. Note that at this stage the <var>--readonly</var> option of
#<code>ext2fs.static</code> must not be passed.
#</p>
#
#<table>
#<tr><td>&nbsp;</td><td class=example><pre>
#  grub1&#62; module /hurd/ext2fs.static \
#   --multiboot-command-line=${kernel-command-line} \
#   --host-priv-port=${host-port} \
#   --device-master-port=${device-port} \
#   --exec-server-task=${exec-task} -T typed ${root} \
#   $(task-create) $(task-resume)
#    [Multiboot-module  0x1c4000, 0x2cfe6a bytes]
#  grub1&#62; module /lib/ld.so.1 /hurd/exec $(exec-task=task-create)
#    [Multiboot-module  0x494000, 0x27afe bytes]
#</pre></td></tr>
#<tr><td>&nbsp;</td><td class=example><pre>
#  grub2&#62; module /hurd/ext2fs.static ext2fs \
#   --multiboot-command-line='${kernel-command-line}' \
#   --host-priv-port='${host-port}' \
#   --device-master-port='${device-port}' \
#   --exec-server-task='${exec-task}' -T typed '${root}' \
#   '$(task-create)' '$(task-resume)'
#  grub2&#62; module /lib/ld.so.1 exec /hurd/exec '$(exec-task=task-create)'
#</pre></td></tr>
#</table>
#
#<p>
#Alternatively, you can throw these lines into a <tt><q>menu.lst</q></tt>
#(Grub1) or <tt><q>grub.cfg</q></tt> (Grub2) configuration file in the partition,
#and load it by using <tt><q>configfile /path/to/menu.lst</q></tt> (Grub1) or
#<tt><q>configfile /path/to/grub.cfg</q></tt> (Grub2) from the grub prompt. You
#can of course also simply install grub in some MBR and point it to there.
#</p>
#
#<p>
#GNU/Hurd can be now booted:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
#grub&#62; boot
#</pre></td></tr></table>
#
#<p>
#If GNU/Hurd fails to boot, it could be due to shared IRQs: GNU Mach does not
#play well with these. You can verify your situation by looking at, for
#instance, the <tt><q>/proc/interrupts</q></tt> file under GNU/Linux. Also, as GNU
#Mach does not support loadable kernel modules, many of the drivers are compiled
#into the default kernel. If there are old peripherals, this can be a problem: a
#device may incorrectly respond to a probe intended for a completely unrelated
#device and thereby cause a crash. Building a new kernel with only the required
#device drivers will usually solve this problem. GNU Mach is easily cross
#compiled. If you are running Debian, install the <tt><q>mig</q></tt> package,
#and your stock <tt><q>gcc</q></tt> should do.
#</p>
#
#<p>
#If this does not help, ask on the appropriate mailing list.
#</p>
#
#
#<h2> 6. Native Install </h2>
#
#<p>
#Once you are presented with a shell prompt, and any time that the Hurd is in
#single user mode, it is necessary to set the terminal type:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# export TERM=mach
#</pre></td></tr></table>
#
#<p>
#Be warned that <kbd>CONTROL-C</kbd> and family will not work in single user
#mode.
#</p>
#
#<p>
#We can now run the <code>native-install</code> script. This will configure the
#packages and set up several important translators:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# ./native-install
#</pre></td></tr></table>
#
#<p>
#Before the script terminates, it will indicate that you can now reboot and enter
#multi-user mode. Do so, this is the Hurd, welcome!
#</p>


<h2> Installatie </h2>

<p>
U kunt gewoon het Debian-installatiesysteem gebruiken; zie de
<a href="hurd-cd">voorbereide cd-images</a>.
Daarna zijn de volgende stappen nodig voor een juiste configuratie.
</p>

<p>
U kunt ook een vooraf geïnstalleerd image ophalen en dit in qemu uitvoeren:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
$ wget https://cdimage.debian.org/cdimage/ports/stable/hurd-i386/debian-hurd.img.tar.gz
$ tar xzf debian-hurd.img.tar.gz
$ kvm -m 1G -drive file=$(echo debian-hurd*.img),cache=writeback
</pre></td></tr></table>

<p>
Om toegang tot de computer via ssh mogelijk te maken kunt u de volgende regel toevoegen:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
-net nic -net user,hostfwd=tcp:127.0.0.1:2222-:22
</pre></td></tr></table>

<p>
en zo ssh-toegang verkrijgen via uw lokale TCP poort 2222.
</p>

<p>
U kunt het image ook converteren naar de voor virtualbox geschikte VDI-indeling:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
$ VBoxManage convertfromraw debian-hurd-*.img debian-hurd.vdi --format vdi
</pre></td></tr></table>

<h2> Configuratie </h2>

<h3> Het netwerk </h3>

<p>
De Debian-manier wordt ondersteund sinds sysvinit 2.88dsf-48 en hurd 1:0.5.git20140320-1: <tt>/etc/network/interfaces</tt> wordt gebruikt zoals in
Linux. Het enige verschil is dat netwerkkaarten in <tt>/dev</tt> verschijnen en
interfaces daarom gespecificeerd moeten worden als <tt>/dev/eth0</tt> enz.
</p>

#<p>
#First, make sure that your network card is recognized by GNU Mach:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# devprobe eth0
# eth0
#</pre></td></tr></table>
#
#<p>
#If <code>devprobe eth0</code> does not return <code>eth0</code>, the kernel
#didn't detect your network board and you need to try another board. For
#instance, qemu's e1000 board is not supported, the rtl8139 one should work:
#<code>-net nic,model=rtl8139 -net user</code>
#</p>

#<p>
#Starting from version 20120520-1, the hurd package includes DDE drivers which
#are used to support more recent devices (drivers are taken from Linux
#2.6.32). Translators for that are already set up for you, and all you
#need to do is to replace <code>eth0</code> (the mach driver name) with
#<code>/dev/eth0</code> (the DDE driver path) in the remainder of this document.
#</p>
#
#<p>
#It is possible to try to use the DDE driver even if GNU Mach has a driver:
#passing <code>nonetdev</code> on the gnumach command line will disable the GNU
#Mach driver, and the DDE driver will start working.
#</p>

<p>
Als het netwerk niet lijkt te werken, gebruik dan het volgende om
foutopsporingsinformatie van het DDE-stuurprogramma te verkrijgen::
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans -fga /dev/netdde /hurd/netdde
</pre></td></tr></table>

<p>
en vernietig daarna alle devnode- en pfinet-processen om hen te laten herstarten
met het nieuwere netdde. Werkt het nog steeds niet, post dan in een bugrapport
de volledige uitvoer van het bovenstaande netdde settrans-commando, evenals de
uitvoer van <code>lspci</code> en <code>lspci -n</code> .
</p>

<p>
Om het netwerk te configureren zonder via <tt>/etc/network/interfaces</tt> te
gaan, moet de pfinet translator geconfigureerd worden.
Dit kan gebeuren met <code>dhclient</code> uit het pakket
<code>isc-dhcp-client</code>. Dit kan ook handmatig gebeuren met
<code>inetutils-ifconfig</code> uit het pakket <code>inetutils-tools</code>,
en <code>ping</code> is in het pakket <code>inetutils-ping</code> te vinden.
Ten slotte kan dit handmatig gebeuren (en voorgoed worden vastgelegd) met
behulp van het <code>settrans</code>-commando om een translator aan een bepaalde
bestandssysteemnode te koppelen. Wanneer programma's de node
benaderen door bijvoorbeeld een RPC te verzenden, zal het besturingssysteem de
server transparant starten om het verzoek af te handelen.
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans -fgap /servers/socket/2 /hurd/pfinet -i /dev/eth0 \
   -a a.b.c.d -g e.f.g.h -m i.j.k.l
</pre></td></tr></table>

<p>
Hier worden verschillende opties meegegeven aan <code>settrans</code>. De
eerste twee, <q><var>fg</var></q>, dwingen eventuele bestaande translators om
te verdwijnen. De volgende twee, <q><var>ap</var></q>, maken zowel actieve als
passieve translators. Door de translator actief te maken, zullen we eventuele
foutmeldingen direct op <tt><q>stderr</q></tt> zien. Het laatste slaat de
translator en de argumenten op in de node, zodat deze later transparant opnieuw
kan worden opgestart (d.w.z. de instelling wordt persistent gemaakt, ook voor
de volgende malen dat opgestart wordt). De opties worden gevolgd door de node
waaraan de translator gekoppeld moet worden, dan het uit te voeren programma
(d.w.z. de translator) en eventuele argumenten die eraan meegegeven moeten
worden. De <q><var>-i</var></q>-optie is de interface waarop <code>pfinet</code>
zal luisteren, <q><var>-a</var></q> is het IP-adres, <q><var>-g</var></q> is de
gateway en <q><var>-m</var></q> is het netwerk-mask.
</p>

<p>
Zorg ervoor dat u naamservers toevoegt aan uw
<tt><q>/etc/resolv.conf</q></tt>-bestand:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
  nameserver 192.168.1.1
</pre></td></tr></table>

<p>
Om de configuratie te testen voert u het commando
<code>ping -c2 gateway</code> uit. De optie <q><var>-c</var></q> is belangrijk
om het aantal malen dat ping uitgevoerd wordt te beperken; denk eraan,
<kbd>CONTROL-C</kbd> werkt niet in de modus voor één enkele gebruiker.
</p>

<p>
Hulp over <code>settrans</code> kan verkregen worden door het de optie
<q><var>--help</var></q> mee te geven. Hulp over een specifieke translator kan
verkregen worden door deze met hetzelfde argument aan te roepen aan de
commandoregel, bijv.:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# /hurd/pfinet --help
</pre></td></tr></table>

<p>
Aangezien er veel uitvoer kan zijn, kunt u die door een pagineringsprogramma
zoals <code>less</code> sluizen.
</p>

<p>
Om ook ondersteuning voor IPv6 te configureren, moet dezelfde configuratie
opgeslagen worden op zowel <tt>/servers/socket/2</tt> als op
<tt>/servers/socket/26</tt>, waarbij deze naar elkaar verwijzen, zodat er
slechts één daadwerkelijk gestart wordt, gebonden aan beide nodes:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans -fgap /servers/socket/2 /hurd/pfinet -6 /servers/socket/26 -i /dev/eth0 \
   -a a.b.c.d -g e.f.g.h -m i.j.k.l
 \# settrans -p /servers/socket/26 /hurd/pfinet -4 /servers/socket/2 -i /dev/eth0 \
   -a a.b.c.d -g e.f.g.h -m i.j.k.l
</pre></td></tr></table>

<p>
De pfinet-server maakt standaard IPv6-autoconfiguratie mogelijk. De huidige
toestand kan verkregen worden met <tt>fsysopts /servers/socket/26</tt>.
Adressen kunnen ook handmatig ingesteld worden, bijvoorbeeld door
<tt>-A 2001:123:123::42/64 -G 2001:123:123::1</tt> te gebruiken.
</p>

<p>
De configuratie van pfinet kan ook live worden gewijzigd (zonder dat deze op
schijf opgeslagen wordt) door <tt>fsysopts</tt> te gebruiken:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# fsysopts /servers/socket/2
 /hurd/pfinet --interface=/dev/eth0 --address=10.3.0.1 --netmask=255.255.0.0 --gateway=10.3.0.128
 \# fsysopts /server/socket/2 -a 10.3.0.2 -m 255.255.0.0 -g 10.3.0.128
</pre></td></tr></table>

<p>
Er kan een firewall opgezet worden door de <tt>eth-filter</tt>-translator
ertussen te plaatsen. Bijvoorbeeld, dit voorkomt toegang tot poort 22:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans -c /dev/eth0f /hurd/eth-filter -i /dev/eth0 -r "not port 22"
</pre></td></tr></table>

<p>
Dan kan het gefilterde device <tt>/dev/eth0f</tt> opgegeven worden aan
<tt>pfinet</tt> of <tt>dhclient</tt>, in plaats van /dev/eth0.
</p>


<h3> Toetsenbordindeling </h3>

<p>
De toetsenbordindeling kan geconfigureerd worden met het standaardpakket
<code>keyboard-configuration</code>. Zorg ervoor dat het geïnstalleerd is en
voer dan het commando <code>dpkg-reconfigure keyboard-configuration</code> uit.
Enkel de indeling wordt ondersteund, varianten (nog) niet. Dit heeft geen
onmiddellijk effect, aangezien de console opnieuw moet worden opgestart opdat
er rekening gehouden zou worden met de parameter. Opnieuw opstarten bijvoorbeeld
zou daarvoor goed moeten zijn.
</p>

<h3> Andere bestandssystemen </h3>

<p>
Bewerk nadien <tt><q>/etc/fstab</q></tt> om eventueel extra bestandssystemen en
wisselgeheugen toe te voegen. Het is <em>erg belangrijk</em> dat er
wisselgeheugen gebruikt wordt; de Hurd zal dan een orde van grootte stabieler
zijn. Merk op dat de Hurd op een transparante manier een swappartitie kan delen
met Linux, maar dat het ook makkelijk kan pagineren naar elk apparaat, met
inbegrip van een rauwe partitie, zoals uw home-partitie. Standaard worden bij
een basisdistributie enkel de editors <code>nano</code> en <code>vi</code>
geïnstalleerd.
</p>

<p>
Hier is een voorbeeld van een <tt><q>/etc/fstab</q></tt>-bestand:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
\# &#60;file system&#62; &#60;mount point&#62;   &#60;type&#62;  &#60;options&#62;  &#60;dump&#62;  &#60;pass&#62;
/dev/hd0s1      /               ext2    rw         0       1
/dev/hd0s2      /home           ext2    rw         0       2
/dev/hd0s3      none            swap    sw         0       0
</pre></td></tr></table>

<p>
Indien een regel ontbreekt voor een bepaald <code>/dev</code>-apparaat, denk er
dan aan om het te creëren met het commando <code>MAKEDEV</code>:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# cd /dev
 \# ./MAKEDEV hd0s1 hd0s2 hd0s3
</pre></td></tr></table>

<p>
U kunt ook handmatig een bestandssysteem aankoppelen door het aanroepen van
<code>settrans</code>:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans /mnt /hurd/ext2fs /dev/hd0s5
</pre></td></tr></table>

<p>
Het idee achter dit commando is dat u op de node <code>/mnt</code> de translator
<code>/hurd/ext2fs /dev/hd0s5</code> plaatst. <code>/hurd/ext2fs</code> zal
uitgevoerd worden en zal beginnen met lezen/schrijven van/naar
<code>/dev/hd0s5</code> en de inhoud ervan weergeven op <code>/mnt</code>. Meer
informatie is te vinden in de
<a href="hurd-doc-translator">Translator-documentatie</a>.
</p>

<p>
Om een nfs-bestandssysteem aan te koppelen wordt de translator
<code>/hurd/nfs</code> gebruikt. Wanneer deze translator niet uitgevoerd wordt
door de root-gebruiker, zal deze een verbinding maken met de server via een
poort boven 1023. Standaard zal GNU/Linux dit verwerpen. Om GNU/Linux op te
dragen verbindingen te accepteren die afkomstig zijn van een niet-gereserveerde
poort, moet u de optie <q><var>insecure</var></q> toevoegen aan de
export-regel. Hier volgt een voorbeeld van een
<tt><q>/etc/exports</q></tt>-bestand waarbij ervan uitgegaan wordt dat het
ip-adres van de cliëntcomputer <tt><q>192.168.1.2</q></tt> is:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
  /home  192.168.1.2(rw,insecure)
</pre></td></tr></table>

<p>
Om dit aan te koppelen vanaf een GNU-computer, ervan uitgaand dat het ip-adres
van de nfs-server <tt><q>192.168.1.1</q></tt> is:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
\# settrans -cga /aankoppel/punt /hurd/nfs 192.168.1.1:/home
</pre></td></tr></table>


<h2> Plezier beleven aan Debian GNU/Hurd </h2>

<p>
Nu, wat voor leuke dingen kunnen we doen met de Hurd?
</p>

<h3> Schijf-images aankoppelen </h3>

<p>
Toegang krijgen tot de inhoud van een cd-image is een beetje omslachtig met
standaard Unix-systemen als u geen root bent. Op GNU/Hurd komt het hierop
neer:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
settrans ~/mnt /hurd/iso9660fs cd_image.iso
</pre></td></tr></table>

<p>
En het is volledig veilig: de <code>iso9660fs</code>-translator draait onder
uw identiteit, niet onder die van de root-gebruiker. U kunt zelfs uw eigen
translator coderen voor elk soort bestandssysteem. Ja, dit is zoals FUSE.
Zonder al het gedoe.
</p>

<h3> Transparante FTP </h3>

<p>
Met het volgende zet u een transparante <code>ftp</code>-map op:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
settrans -c /ftp: /hurd/hostmux /hurd/ftpfs /
</pre></td></tr></table>

<p>
Ga nu met het commando <code>cd</code> naar bijvoorbeeld
<code>/ftp://ftp.gnu.org/</code> en voer daar het comando <code>ls</code> uit.
Jawel, u kunt van thuis uit gewoon het commando <code>tar xf ftp://ftp.gnu.org/pub/gnu/gcc/gcc-4.6.0/gcc-4.6.0.tar.bz2</code> uitvoeren!
</p>

<h3> Sub-Hurd </h3>

<p>
Een <a href="https://www.gnu.org/software/hurd/hurd/subhurd.html">sub-Hurd</a>
is een compleet subsysteem. Op het eerste gezicht heel erg zoals
virtualisatiecontainers, behalve dat je helemaal geen root-gebruiker hoeft te
zijn om er een uit te voeren.
</p>

<h3> gdb ext2fs, pfinet, ... </h3>

<p>
Jawel, u kunt gdb bijvoorbeeld uitvoeren op de ext2fs-implementatie, de
<code>pfinet</code> TCP/IP-stapel, enz.
</p>

<h3> En veel meer andere zaken! </h3>

<p>
Enkele van de lopende werkzaamheden zijn <code>mboxfs</code>, <code>tarfs</code>, <code>xmlfs</code>, <code>gopherfs</code>, ...
</p>

#<h3> 7.3 Rebooting </h3>
#
#<p>
#Finally, reboot into multiuser mode, i.e. in the same way single user mode was
#brought up minus the <q><var>-s</var></q> option when loading the kernel. For
#details, see section 5. Booting GNU/Hurd.
#</p>
#
#<p>
#Happy Hacking!
#</p>

<h2>Slotwoorden </h2>

<p>
Wat volgt zijn gewoon enkele vluggertjes voor tijdens de installatie. Zorg
ervoor dat u ook de documentatie leest voor het geïnstalleerde systeem:
de <a href=hurd-doc>Debian GNU/Hurd documentatie</a>,
maar ook de <a href=http://hurd.gnu.org/>bovenstroomse website</a>.
</p>


#<h3> 8.1 The Grub Menu </h3>
#
#<p>
#Having to always load the kernel by hand can be very tedious. Edit the
#<tt><q>/boot/grub/menu.lst</q></tt> for Grub1 or
#<tt><q>/boot/grub/grub.cfg</q></tt> for Grub2 and tailor it appropriately;
#booting will become much quicker and easier.
#</p>
#
#
#<h3> 8.2 Adding Devices </h3>
#
#<p>
#By default, only a few devices are created in the <tt><q>/dev</q></tt> directory.
##Use the <code>MAKEDEV</code> script to create any needed device nodes.
#</p>

<h3><a name=morepackages> Extra pakketten installeren </a></h3>

<p>
U heeft verschillende mogelijkheden om pakketten toe te voegen. Pakketten
downloaden en <code>dpkg -i</code> gebruiken werkt, maar is erg onhandig. De
makkelijkste methode is <code>apt-get</code> gebruiken.
</p>

<p>
Indien u de uitgave van 2021 van Debian GNU/Hurd gebruikt heeft, is de
veiligste manier om de momentopname (het snapshot) van deze uitgave te
gebruiken door het bestand
<code>/etc/apt/apt.conf.d/99ignore-valid-until</code> aan te maken met daarin
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
Acquire::Check-Valid-Until "false";
</pre></td></tr></table>

<p>
En dan kan de momentopname gebruikt worden als bron voor apt: bewerk
<tt><q>/etc/apt/sources.list</q></tt>, voeg de volgende vermelding toe van
een niet-vrijgegeven bron.
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
deb http://snapshot.debian.org/archive/debian-ports/20210812T100000Z/ sid main
deb-src http://snapshot.debian.org/archive/debian/20210812T100000Z/ sid main
deb [trusted=yes] https://snapshot.debian.org/archive/debian-ports/20210812T100000Z/ unreleased main
</pre></td></tr></table>

<p>
Daarna updaten, het pakket <code>debian-ports-archive-keyring</code>
installeren en opnieuw updaten. U heeft nu
de volledige uitgave van Debian GNU/Hurd 2021 ter beschikking.
</p>


<p>
Indien u een momentopname gebruikt heeft die recenter is dan de uitgave van
2021, kunt u de volgende pakketbronnen toevoegen om de meest recente pakketten
te verkrijgen:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
deb http://deb.debian.org/debian-ports unstable main
deb-src http://deb.debian.org/debian unstable main
deb http://deb.debian.org/debian-ports unreleased main
</pre></td></tr></table>

<p>
Daarna updaten, het pakket <code>debian-ports-archive-keyring</code> installeren
en opnieuw updaten.
</p>

<p>
Indien <code>dpkg</code> klaagt over ontbrekende programma's wanneer u
<code>apt-get</code> voor het eerst uitvoert, moet u root-gebruiker worden in
een login-shell (d.w.z. <code>su -</code>, niet gewoon <code>su</code>).
</p>

<p>
Indien GNU Mach uw netwerkkaart niet herkent of indien u gebruik maakt van een
modem, zal de enige manier om op te waarderen zijn: de pakketten downloaden en
deze dan transfereren naar het GNU-systeem. De makkelijkste manier om dit te
doen is apt off-line gebruiken. Raadpleeg
<tt><q>/usr/share/doc/apt-doc/offline.text.gz</q></tt> voor gedetailleerde
instructies.
</p>


<h3>De Hurd-console</h3>

<p>
Naast de Mach-console die u tijdens de installatie tegenkwam, heeft de GNU/Hurd
een krachtige console voor de gebruikersruimte die virtuele terminals biedt.
Indien u de installatie uitvoerde in pseudo-grafische modus, zou deze console
automatisch moeten starten bij het opstarten, anders kan u ze handmatig starten
met het volgende commando:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# console -d vga -d pc_mouse --repeat=mouse -d pc_kbd --repeat=kbd -d generic_speaker -c /dev/vcs
</pre></td></tr></table>

<p>
Als bevestigd is dat ze werkt, kan ze bij het opstarten geactiveerd worden
vanuit <tt>/etc/default/hurd-console</tt>: verander <tt>ENABLE="false"</tt> naar
<tt>ENABLE="true"</tt>.
</p>

<p>
Binnen de Hurd-console kunt u schakelen tussen virtuele terminals via
<kbd>ALT+F1</kbd>, <kbd>ALT+F2</kbd> enzovoort. <kbd>ALT+CTRL+BACKSPACE</kbd>
koppelt de Hurd-console af en brengt u terug naar de Mach-console, vanwaar u ze
terug kunt aankoppelen met het bovenstaande commando.
</p>


<h3>X.Org </h3>

<p>
X.Org werd geschikt gemaakt voor de Hurd en alle videokaarten die het
ondersteunt en die geen kernelmodule of drm nodig hebben, zouden moeten werken.
</p>

<p>
De Hurd-console moet reeds actief zijn en u moet repeaters hebben ingesteld
zoals aangegeven in de vorige sectie. Controleer bijvoorbeeld of
<code>echo $TERM</code> <code>hurd</code> als uitvoer geeft en ga na of
<code>/dev/cons/kbd</code> en <code>/dev/cons/mouse</code> bestaan.
</p>

<p>
U moet <tt>dpkg-reconfigure x11-common xserver-xorg-legacy</tt> uitvoeren om
elke gebruiker in staat te stellen Xorg te starten, omdat de X-wrapper niets
weet over de Hurd- en Mach-consoles.
</p>

<p>
U moet ook <tt>/etc/X11/xorg.conf</tt> creëren om de sneltoets
control-alt-backspace mogelijk te maken:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
Section "InputDevice"
   Identifier "Generic Keyboard"
   Driver "kbd"
   Option "XkbOptions" "terminate:ctrl_alt_bksp"
EndSection
</pre></td></tr></table>

<p>
Het kan gebeuren dat Xorg om de een of andere reden een resolutie van 16/9
kiest, maar een desktopgrootte van 4/3. Dit is de schuld van Xorg, niet van
de Hurd :) Om dit probleem te vermijden moet u het volgende toevoegen aan
<tt>/etc/X11/xorg.conf</tt> :
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
Section "Screen"
   Identifier "myScreen"
   SubSection "Display"
      Virtual 1024 768
   EndSubSection
EndSection
</pre></td></tr></table>

<p>
U zult verschillende X-pakketten nodig hebben. <code>xorg</code>,
<code>rxvt</code> en een windowmanager: <code>twm</code>, <code>icewm</code>,
<code>openbox</code>, ... zijn goed om te beginnen. Indien u wilt dat X start
bij het opstarten, zult u een programma voor beeldschermbeheer moeten
installeren. <code>lightdm</code> en <code>gdm</code> werken nog niet, maar
<code>xdm</code> zou gewoon goed moeten werken.
</p>

<p>
Voer tenslotte het commando <code>startx /usr/bin/uw_beeldschermbeheerder</code>
uit.
</p>

<p>
Als dat niet werkt, zoals vermeld in de foutmelding, ga dan kijken in
<tt>/var/log/Xorg.0.log</tt> (of post het op de lijst zodat iemand er kan
naar kijken).
</p>

<h3>Uw systeem opwaarderen</h3>

<p>
Als u een momentopname van een Debian release gebruikt, heeft u geen upgrade
beschikbaar, aangezien de vrijgegeven distributie bevroren wordt op de
releasedatum. <b>Dit betekent dat u geen beveiligingsupdates krijgt!</b>
Misschien wilt u liever de distributie unstable (de onstabiele distributie)
activeren, zoals beschreven wordt in de sectie <a href=#morepackages>Meer
pakketten installeren</a>.
</p>

<p>
Zodra u de distributie unstable heeft ingeschakeld, moet u er rekening mee
houden dat, aangezien deze onstabiel is, deze kan beïnvloed worden door
overgangsproblemen bij de opwaardering van bibliotheken. Wees dus niet verbaasd
dat het soms niet mogelijk zal zijn om sommige pakketten op te waarderen. Over
het algemeen kunt u de door Debian aanbevolen opwaarderingsprocedure gebruiken:
gebruik eerst
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
\# apt upgrade --without-new-pkgs
</pre></td></tr></table>

<p>om op te waarderen wat opgewaardeerd kan worden zonder de lijst met pakketten te wijzigen, en gebruik daarna</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
\# apt full-upgrade
</pre></td></tr></table>

<p>om de rest op te waarderen.<p>

<p>Opmerking: als u uw systeem zeer zelden opwaardeert, kunt u op
opwaarderingsproblemen stuiten. Zorg ervoor dat u eerst opwaardeert naar de
recentste momentopname van een Debian release (Hurd 2021, zie de sectie
<a href=#morepackages>Meer pakketten installeren</a>), voordat u opwaardeert
vanaf de distributie unstable.</p>

<h3>Slotwoorden</h3>

<p>
Om uw systeem af te sluiten gebruikt u gewoon <code>halt</code>,
<code>poweroff</code> of <code>reboot</code>. Indien deze soms vastlopen omdat
een bepaalde achtergronddienst niet correct afgesloten wordt, kunt u in de
plaats daarvan gebruik maken van <code>halt-hurd</code>,
<code>poweroff-hurd</code>, <code>reboot-hurd</code>, welke de
achtergronddiensten niet echt afsluiten, maar gegevens wel correct naar schijf
synchroniseren.
</p>
