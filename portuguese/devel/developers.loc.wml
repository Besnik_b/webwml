#use wml::debian::template title="Localização dos(as) desenvolvedores(as)"
#use wml::debian::translation-check translation="21849af3149d448e7ee39af170c93bee402c4d3a"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Onde estão localizados os(as)
desenvolvedores(as) Debian (DD)? Se um(a) DD especificou as coordenadas de sua
casa no banco de dados de desenvolvedores(as), ela ficará visível em nosso
mapa-múndi.</p>
</aside>

<p>
O mapa abaixo foi gerado de uma
<a href="developers.coords">lista de coordenadas de desenvolvedores(as)</a>
anonimizada usando o programa
<a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.
</p>

<p><img src="developers.map.jpeg" alt="World Map">

<h2>Como adicionar suas coordenadas</h2>

<p>
Se você quiser adicionar suas coordenadas na sua entrada do banco de
dados, faça o login no
<a href="https://db.debian.org">banco de dados dos(as) desenvolvedores(as) Debian</a>
e modifique sua entrada. Se não sabe as coordenadas da cidade onde mora,
você pode usar o <a href="https://osm.org">OpenStreetMap</a> para procurá-las.
Pesquise sua cidade e selecione as setas de direção ao lado do campo de
pesquisa. Arraste o marcador verde para o mapa do OSM e as coordenadas
aparecerão no campo <em>From</em>.
</p>

<p>O formato das coordenadas é um dos seguintes:</p>

<dl>
  <dt>Graus decimais</dt>
    <dd>O formato é <code>+-DDD.DDDDDDDDDDDDDDD</code>. Programas como o Xearth e muitos
    outros sites de posicionamento usam este formato. A precisão é limitada a 4
    ou 5 decimais.</dd>
  <dt>Graus Minutos (DGM)</dt>
    <dd>O formato é <code>+-DDDMM.MMMMMMMMMMMMM</code>. Não é de tipo aritmético, mas uma
    representação empacotada de duas unidades separadas: graus e minutos. Essa
    saída é comum para alguns tipos de unidades GPS de mão e mensagens GPS no
    formato NMEA.</dd>
  <dt>Graus Minutos Segundos (DGMS)</dt>
    <dd>O formato é <code>+-DDDMMSS.SSSSSSSSSSS</code>. Como o DGM, ele não
    é um tipo aritmético, mas uma representação empacotada de três unidades
    separadas: graus, minutos e segundos. Essa saída é tipicamente derivada de
    sites web que dão três valores para cada posição. Por exemplo
    <code>34:50:12.24523 Norte</code> pode ser a posição dada, em DGMS isso
    seria <code>+0345012.24523</code>.</dd>
</dl>

<p>
<strong>Por favor, note:</strong> <code>+</code> é Norte para latitude,
code>+</code> é Leste para longitude. É importante especificar zeros iniciais o
suficiente para tornar claro o formato que está sendo usado, se sua posição é
menos dois graus de um ponto zero.
