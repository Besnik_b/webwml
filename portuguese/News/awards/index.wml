#use wml::debian::template title="Prêmios"
#use wml::debian::translation-check translation="ccec77bd0e2877743e2f0c75235e272549af6ef8" mindelta="1" maxdelta="1" maintainer="Thiago Pezzo"

# TRANSLATORS: We moved the original english file from /misc/awards to the /News folder, so we lost the git history.
# This translation-check header is set to an arbitrary commit just to allow this file to be built.
# Please compare manually with the english version to update your translation (which is older than the commit referenced).


<p>Tanto nossa distribuição quanto nosso site ganharam vários prêmios de
diferentes organizações. Abaixo está uma lista dos prêmios dos quais sabemos.
Se você sabe de algum outro (ou se você é uma organização que concedeu um
prêmio a nosso site ou distribuição) então envie um email para
<a href="mailto:webmaster@debian.org">webmaster@debian.org</a> e nós colocaremos
aqui.</p>


<h2>Prêmios para a distribuição</h2>

<table width="100%">
 <colgroup span="2">
 <col align="left" width="50%">
 <col align="left" width="50%">
 </colgroup>
 <tr>
  <td>
    <p><b>Fevereiro 2018</b></p>
    <p><q>Melhor distribuição Linux</q>, Linux Journal Readers' Choice Awards
    2018 (Prêmio escolha do leitor 2018 do Linux Journal).</p>
  </td>
  <td>
    <a href="https://www.linuxjournal.com/content/best-linux-distribution">
     <img src="ljaward2018.png" alt="Melhor distribuição Linux" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Fevereiro 2012</b></p>
    <p><q>Distribuição do ano para servidores</q>, LinuxQuestions.org Members
    Choice Awards 2011 (Prêmio escolha dos(as) membros(as) 2011 do LinuxQuestions.org).</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/linux-news-59/2011-linuxquestions-org-members-choice-award-winners-928502/">
     <img src="LinuxQuestions.png" alt="Distribuição do Ano para Servidores" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Agosto 2011</b></p>
    <p><q>Melhor distribuição Linux de 2011</q> pelo
    <a href="http://www.tuxradar.com/content/best-distro-2011">TuxRadar</a></p>
  </td>
  <td>
    <a href="http://www.tuxradar.com/content/best-distro-2011">
     <img src="tuxradar-best-distro-2011.jpg" alt="Melhor distribuição Linux de
    2011 pelo TuxRadar"></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Março 2011</b></p>
    <p><q>Melhor distribuição de Código Aberto para servidores</q> e <q>notável
    contribuição para os softwares de Código Aberto/Linux/Livre</q>
       no <a href="$(HOME)/News/2011/20110304">Linux New Media Awards 2011</a></p>
  </td>
  <td>
     <a href="http://www.linuxnewmedia.com/">
      <img src="lnm_award_2011.png" alt="Linux New Media Award 2011"></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Fevereiro 2011</b></p>
    <p><q>Distribuição do ano para servidores</q>, LinuxQuestions.org Members
    Choice Awards 2010 (Prêmio escolha dos(as) membros(as) 2010 do LinuxQuestions.org</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/linux-news-59/2010-linuxquestions-org-members-choice-award-winners-861440/">
     <img src="LinuxQuestions.png" alt="Distribuição do Ano para Servidores" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Fevereiro 2010</b></p>
    <p><q>Distribuição do ano para servidores</q>, LinuxQuestions.org Members
    Choice Awards 2009 (Prêmio escolha dos(as) membros(as) 2009 do LinuxQuestions.org</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/2009-linuxquestions-org-members-choice-awards-91/server-distribution-of-the-year-780628/">
     <img src="LinuxQuestions.png" alt="Distribuição do Ano para Servidores" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Fevereiro 2009</b></p>
    <p><q>Distribuição do ano para servidores</q>, LinuxQuestions.org Members
    Choice Awards 2008 (Prêmio escolha dos(as) membros(as) 2008 do LinuxQuestions.org</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/2008-linuxquestions.org-members-choice-awards-83/server-distribution-of-the-year-695611/">
     <img src="server-distribution-debian.png"
      alt="Distribuição do Ano para Servidores" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Fevereiro 2008</b></p>
    <p><q>Distribuição do ano para servidores</q>, LinuxQuestions.org Members
    Choice Awards 2007 (Prêmio escolha dos(as) membros(as) 2007 do LinuxQuestions.org</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/2007-linuxquestions-org-members-choice-awards-79/server-distribution-of-the-year-610200/">
     <img src="LinuxQuestions.png" alt="Distribuição do Ano para Servidores" /></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Abril 2006</b></p>
   <p><q>Projeto recomendado</q> pelo Linux+</p>
  </td>
  <td>
    <a href="http://lpmagazine.org/">
      <img src="rec_pro_L+.png" alt="Projeto Recomendado pelo Linux+"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Novembro de 2003</b></p>
   <p>Melhor distribuição no Linux New Media Award 2003.</p>
   <p>Apareceu na <a
    href="http://www.linux-magazin.de/Artikel/ausgabe/2003/12/award/award.html">\
    edição de dezembro de 2003 (alemã)</a> da Linuz-Magazin.</p>
  </td>
  <td>
   <a href="http://www.linuxnewmedia.de/"><img src="lnm_award_logo_2003.jpg"
        alt="Linux New media Award"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Outubro de 2003</b></p>
   <p>Melhor distribuição empresarial na escolha do leitor Linux Enterprise
    2003.
    (<a href="http://www.linuxenterprise.de/itr/service/show.php3?id=104&amp;nodeid=35">Resultados</a>)
   </p>
  </td>
  <td>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Outubro de 2003</b></p>
   <p>Melhor distribuição no "Linux Journal 2003 readers' Choice Awards"
   (Prêmio Escolha do Leitor 2003 do Linux Journal).</p>
  </td>
  <td>
   <a href="http://pr.linuxjournal.com/article.php?sid=785"><img
       src="readerschoice2003small.png" width="128" height="122"
       alt="Linux Journal Readers' Choice 2003">
   </a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Janeiro de 2003</b></p>
   <p>Melhor escolha de orçamento para Linux como sistema operacional em
    Mikrodatorn 1/2003</p>
  </td>
  <td>
   <a href="http://mikrodatorn.idg.se/"><img src="budgetval.png"
    alt="Bästa budgetval 2003" width="100" height="98"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Novembro de 2002</b></p>
   <p>Primeiro lugar na categoria distribuições do Linux New Media Award
   2002.</p>
   <p>Aparece na
    <a href="http://www.linux-magazin.de/Artikel/ausgabe/2002/12/award/award.html">\
    edição de dezembro de 2002 (alemã)</a> da Linux Magazin. Uma versão em inglês
    está disponível como
    <a href="https://web.archive.org/web/20081203203144/http://www.linux-magazine.com/issue/25/2002_Linux_Awards.pdf">\
    arquivo pdf</a>.</p>
  </td>
  <td>
   <a href="http://www.linuxnewmedia.de/"><img src="lnm_award_logo.jpg"
    alt="Linux New Media Award"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Novembro de 2000</b></p>
   <p>A série VA Linux 2200 com o Debian ganhou o prêmio de melhor servidor
   web na premiação da escolha dos editores do Linux Journal 2000.</p>
  </td>
  <td>
   &nbsp;
  </td>
 </tr>
 <tr>
  <td>
   <p>Prêmio escolha dos leitores na categoria infraestrutura na conferência
   e exposição Web Tools 2000.</p>
  </td>
  <td>
   &nbsp;
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Setembro de 2000</b></p>
   <p>Segundo lugar na categoria distribuição de servidor na premiação escolha dos
   editores da Linux Magazine.</p>
  </td>
  <td>
   <a href="http://www.linux-mag.com/"><img src="EC_2000_lg.jpg"
    alt="Editor's Choice Award 2000"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p>Vencedor dos favoritos da mostra para distribuição/cliente da
   LinuxWorld Conference &amp; Expo.</p>
   <p>Finalista dos favoritos da mostra para servidor de e-mails,
   servidor web e distribuição/servidor da LinuxWorld Conference
   &amp; Expo Show. </p>
  </td>
  <td>
   <img src="lwshowfav-200.gif"
   alt="LinuxWorld Conference &amp; Expo Show Favorites 1999">
  </td>
 </tr>
 <tr>
  <td>
   <p>Prêmio de altamente recomendável na escolha dos editores da APCMAG.</p>
  </td>
  <td>
   &nbsp;
  </td>
 </tr>
</table>
