<define-tag pagetitle>Debian Edu / Skolelinux Bullseye — uma solução Linux
completa para sua escola</define-tag>
<define-tag release_date>2021-08-15</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="e7ce9859ec2794a8c0d0978182d2d513ed96cfa0" maintainer="felipeviggiano"

<p>
Você é o(a) administrador(a) de um laboratório de informática ou de uma rede
inteira de uma escola?
Você gostaria de instalar servidores, estações de trabalho e notebooks para
trabalharem juntos?
Você quer a estabilidade do Debian com serviços de rede já pré-configurados?
Você gostaria de ter uma ferramenta baseada na web para gerenciar sistemas e
centenas ou mais contas de usuários(as)?
Você já se perguntou se e como computadores antigos podem ser utilizados?
</p>

<p>
O Debian Edu é para você. Os(As) próprios(as) professores(as) ou profissionais
de suporte técnico podem implementar um ambiente de estudo multiusuário(a) e
multimáquinas em poucos dias. O Debian Edu vem com centenas de aplicativos
pré-instalados e você pode sempre adicionar mais pacotes do Debian.
</p>

<p>
O time de desenvolvimento do Debian Edu está feliz em anunciar o Debian Edu 11
<q>Bullseye</q>, a versão do Debian Edu / Skolelinux baseada na versão Debian 11
<q>Bullseye</q>.
Por favor considere testá-lo e nos enviar avaliações
(&lt;debian-edu@lists.debian.org&gt;) para nos ajudar a melhorá-lo ainda mais.
</p>

<h2>Sobre o Debian Edu e o Skolelinux</h2>

<p>
<a href="https://blends.debian.org/edu"> O Debian Edu, também conhecido como
Skolelinux</a>, é uma distribuição Linux baseada no Debian que fornece um ambiente
de rede escolar pronto e completamente configurado. Imediatamente após
a instalação, um servidor escolar executando todos os serviços necessários
em uma rede escolar é configurado, aguardando somente a adição de usuários(as) e
máquinas via GOsa², uma confortável interface web.
Um ambiente de inicialização via rede é preparado, para que, após a instalação
inicial do servidor principal a partir de um CD / DVD / BD ou pendrive,
todas as outras máquinas possam ser instaladas via rede.
Computadores antigos (mesmo com mais de dez anos) podem ser utilizados como
terminais burros (thin clients) LTSP ou como estações de trabalho sem disco,
inicializadas via rede sem qualquer instalação ou configuração.
O servidor escolar do Debian Edu fornece um banco de dados LDAP, serviço de
autenticação Kerberos, diretórios home centralizados, um servidor DHCP, um
proxy web e muitos outros serviços. O ambiente de área de trabalho contém mais
de 70 pacotes de softwares educacionais e outros mais estão disponíveis no
repositório Debian.
As escolas podem escolher entre os ambientes de área de trabalho Xfce, GNOME,
LXDE, MATE, KDE Plasma, Cinnamon e LXQt.
</p>

<h2>Novos recursos do Debian Edu 11 <q>Bullseye</q></h2>

<p>Estes são alguns itens das notas de lançamento do Debian Edu 11
<q>Bullseye</q>, baseado na versão Debian 11 <q>Bullseye</q>. A lista completa,
incluindo informações mais detalhadas, é parte do respectivo
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Features#New_features_in_Debian_Edu_Bullseye">
capítulo do manual do Debian Edu</a>.
</p>

<ul>
<li>
Novo <a href="https://ltsp.org">LTSP</a> para suporte a estações de trabalho
sem disco. Terminais burros (thin clients) ainda são suportados, agora
utilizando a tecnologia <a href="https://wiki.x2go.org">X2Go</a>.
</li>
<li>
É fornecida a inicialização a partir da rede utilizando iPXE em vez de
PXELINUX para ser compatível com LTSP.
</li>
<li>
É utilizado o modo gráfico do instalador do Debian nas instalações iPXE.
</li>
<li>
O Samba agora é configurado como um <q>servidor independente</q> com suporte
para SMB2/SMB3.
</li>
<li>
O DuckDuckGo é utilizado como o provedor de pesquisa padrão, tanto para o
Firefox ESR quanto para o Chromium.
</li>
<li>
Foi adicionada uma nova ferramenta pra configurar o freeRADIUS com suporte
para os métodos EAP-TTLS/PAP e PEAP-MSCHAPV2.
</li>
<li>
Disponibilizada uma ferramenta melhorada para configurar um novo sistema com um
perfil <q>mínimo</q> como um gateway dedicado.
</li>
</ul>

<h2>Opções de download, passos para instalação e manual</h2>

<p>
As imagens oficiais do Debian Network-Installer CD (instalador via rede do
Debian) estão disponíveis tanto para PCs de 32 e 64 bits. A imagem para 32 bits
será necessária somente em casos raros (para computadores mais antigos acima 15
anos). O download dessas imagens pode ser feito a partir dos seguintes locais:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Alternativamente, imagens oficiais BD (tamanho maior do que 5 GB) também
estão disponíveis. É possível montar toda uma rede Debian Edu completa sem uma
conexão com a internet (incluindo todos os ambientes de área de trabalho e
traduções para todos os idiomas suportados pelo Debian).
O download dessas imagens pode ser feito nos seguintes locais:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
As imagens podem ser verificadas utilizando checksums assinados fornecidas no
diretório de download.
<br />
Uma vez feito o download de uma imagem, você pode conferir que
<ul>
<li>
o seu checksum bate com esperado que está no arquivo de checksum; e que
</li>
<li>
o aquivo de checksum não foi adulterado.
</li>
</ul>
Para mais informações sobre como executar esses passos, leia o
<a href="https://www.debian.org/CD/verify">guia de verificação</a>.
</p>

<p>
O Debian Edu 11 <q>Bullseye</q> é inteiramente baseado no Debian 11
<q>Bullseye</q>; portanto, os códigos-fonte de todos os pacotes estão
disponíveis no repositório do Debian.
</p>

<p>
Por favor visite a
<a href="https://wiki.debian.org/DebianEdu/Status/Bullseye">página de status do
Debian Edu Bullseye</a> para informações sempre atualizadas sobre o Debian
Edu 11 <q>Bullseye</q>, incluindo instruções sobre como utilizar
o <code>rsync</code> para o download das imagens ISO.
</p>

<p>
Caso pretenda atualizar a partir do Debian Edu 10 <q>Buster</q>, por favor leia
o respectivo
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Upgrades">
capítulo do manual do Debian Edu</a>.
</p>

<p>
Para informações sobre a instalação, por favor leia o respectivo
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Installation#Installing_Debian_Edu">
capítulo do manual do Debian Edu</a>.
</p>

<p>
Após a instalação, é necessário executar esses
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/GettingStarted">
passos iniciais</a>.
</p>

<p>
Por favor veja as
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/">páginas wiki
do Debian Edu</a> para última versão em inglês do manual do Debian Edu <q>Bullseye</q>.
O manual já se encontra totalmente traduzido para os idiomas alemão, francês,
italiano, dinamarquês, holandês, bokmål norueguês, japonês, chinês simplificado
e português (de Portugal). Existem versões parcialmente traduzidas para
espanhol, romeno e polonês. Uma visão global das
<a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
últimas versões publicadas do manual</a> está disponível.
</p>

<p>
Mais informações sobre o próprio Debian 11 <q>Bullseye</q> pode ser obtida
nas notas de lançamento e no manual de instalação; veja
<a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional completamente livre Debian.
</p>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a> envie um e-mail (em inglês) para
&lt;press@debian.org&gt;.</p>
