#use wml::debian::template title="如何参与 Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="21849af3149d448e7ee39af170c93bee402c4d3a" maintainer="Boyuan Yang"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 我们一直在寻找新贡献者和\
志愿者。我们强烈鼓励<a href="$(HOME)/intro/diversity">每个人的参与</a>。\
只需要：对自由软件感兴趣且有一定的空闲时间。</p>
</aside>

<ul class="toc">
<li><a href="#reading">阅读</a></li>
<li><a href="#contributing">贡献</a></li>
<li><a href="#joining">加入</a></li>
</ul>


<h2><a id="reading">阅读</a></h2>

<p>
如果您还没开始，那您应当先阅读 <a href="$(HOME)">Debian 主页</a>所链接的网页。\
这能帮您更好地理解我们是谁与我们正尝试实现什么。作为一名潜在的 Debian 贡献者，请\
特别注意这两个页面：
</p>

<ul>
  <li><a href="$(HOME)/social_contract#guidelines">Debian 自由软件指导方针</a></li>
  <li><a href="$(HOME)/social_contract">社群契约</a></li>
</ul>

<p>
我们很大一部分沟通交流都是在<a href="$(HOME)/MailingLists/">邮件列表</a>中进行的。\
如果您想要对 Debian 项目内部的工作有一份直观的体验的话，您应当至少订阅 <a
href="https://lists.debian.org/debian-devel-announce/">debian-devel-announce</a> \
和 <a href="https://lists.debian.org/debian-news/">debian-news</a> 邮件列表。\
这些邮件列表均为低容量的列表（并不会有大量邮件产生），但记录了社区内正在发生的事件。\
<a href="https://www.debian.org/News/weekly/">Debian 计划快讯</a>（也在\
debian-news 列表上发布），总结了近期与 Debian 相关的邮件列表和博客上出现的讨论内容\
并提供了指向原始内容的链接。
</p>

<p>
作为一名潜在的开发人员，您也应当考虑订阅 <a
href="https://lists.debian.org/debian-mentors/">debian-mentors</a> 列表。\
在这里您可以询问打包和基础设施项目以及其它与开发人员相关的问题。请注意，这个列表是为新
贡献者而不是普通用户准备的。其它值得关注的列表还有 <a
href="https://lists.debian.org/debian-devel/">debian-devel</a>\
（技术性的开发话题）、<a
href="https://lists.debian.org/debian-project/">debian-project</a>\
（讨论项目中的非技术性问题）、<a
href="https://lists.debian.org/debian-release/">debian-release</a>\
（Debian 发布版本的协商）以及 <a
href="https://lists.debian.org/debian-qa/">debian-qa</a>（质量保证）。
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> <a href="$(HOME)/MailingLists/subscribe">邮件列表订阅</a></button></p>

<p>
<strong>小提示：</strong>如果您想减少收到的邮件数量的话（特别是那些高容量的列表），\
我们提供了电子邮件摘要（“-digest”的列表），它能提供电子邮件的摘要而不是原来的独立的\
信息。您还可以访问<a href="https://lists.debian.org/">邮件列表存档</a>页面来使用\
网页浏览器阅读在我们的列表中的消息。
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 您不需要成为正式的 Debian \
开发者（DD）便可以做出贡献。相反，一名现有的 Debian 开发者可以以<a
href="newmaint#Sponsor">赞助者</a>的身份帮助将您的工作成果整合到整个 Debian 项目\
中去。</p>
</aside>

<h2><a id="contributing">贡献</a></h2>

<p>
您对维护软件包感兴趣吗？那么请看看我们的<a
href="$(DEVEL)/wnpp/">需要投入精力的和未来的软件包</a>列表。在这里您会找到需要（新）\
维护者的软件包。接管被遗弃的软件包是一个很好的作为 Debian 维护者入门的方法。这样做不仅\
对我们的分发有所帮助，也是一次从先前的维护者身上进行学习的机会。
</p>

<p>
这里还有一些其它的关于您能如何向 Debian 做出贡献的主意：
</p>

<ul>
  <li>帮助我们编写<a href="$(HOME)/doc">文档</a>。</li>
  <li>帮助我们维护 <a href="$(HOME)/devel/website/">Debian 网站</a>，产出\
  内容，编辑或翻译现有的文本。 </li>
  <li>加入我们的<a href="$(HOME)/international/">翻译团队</a>。</li>
  <li>改进 Debian 的质量并加入 <a
  href="https://qa.debian.org/">Debian 质量保证（QA）团队</a>。</li>
</ul>

<p>
当然，您还有很多其它事情可以做，我们一直在寻找提供法律支持的人员，以及您也可以加入我们\
的 <a href="https://wiki.debian.org/Teams/Publicity">Debian 宣传团队<a/>。\
成为某个 Debian 团队的一员是在开始<a href="newmaint">新成员流程</a>之前获得经验\
的一种很好的方法。如果您正在寻找软件包赞助者，这也是一个很好的起点。因此，快找到一个团队\
并加入吧！
</p>

<p style="text-align:center"><button type="button"><span class="fa fa-users fa-2x"></span> <a href="https://wiki.debian.org/Teams">Debian 团队的列表</a></button></p>


<h2><a id="joining">加入</a></h2>

<p>
所以，您已经为 Debian 项目贡献了一段时间，并且想以更正式的角色加入 Debian？基本上\
有两种选择：
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian 维护者（DM）角色\
是在 2007 年引入的。在这之前，Debian 开发人员（DD）是唯一的正式角色。目前有两个独立\
的流程来申请其中之一的角色。</p>
</aside>

<ul>
  <li><strong>Debian 维护者（DM）：</strong> 这是第一步——作为 DM 您可以将自己\
  的软件包上传到 Debian 软件档案库（有一些限制）。与被赞助的维护者不同，Debian 维护者\
  可以在没有赞助者的情况下维护软件包。<br>更多信息：<a
  href="https://wiki.debian.org/DebianMaintainer">Debian 维护者 Wiki</a></li>
  <li><strong>Debian 开发人员（DD）：</strong> 这是 Debian 中的传统正式成员。DD \
  能参与 Debian 选举。负责上传的 DD 能将任何软件包上传到软件档案库。在成为负责上传\
  的 DD 之前，您应有至少六个月的打包维护记录（例如：作为 DM 上传软件包，在团队内部工作\
  或维护由赞助者上传的软件包）。非负责上传的 DD 具有与 Debian 维护者相同的上传软件包\
  权限。在申请成为非负责上传的 DD 之前，您应该在项目中具有可见且重要的工作记录。<br>更多\
  信息：<a href="newmaint">新成员专区</a></li>
</ul>

<p>
无论选择申请哪种角色，都应该熟悉 Debian 的程序，因此建议在申请前阅读 <a
href="$(DOC)/debian-policy/">Debian 政策</a>和<a
href="$(DOC)/developers-reference/">开发人员参考</a>。
</p>
