#use wml::debian::translation-check translation="20c4c7542d0df64b1bb0b78c04f3ed88d52fbc75" maintainer="Besnik Bleta"
<define-tag pagetitle>Debian Installer Buster RC 3 release</define-tag>
<define-tag release_date>2019-07-03</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the third release candidate of the installer for Debian 10
<q>Buster</q>.
</p>

<p>
This release candidate of the installer is meant to validate some very
last changes, making sure Recommends of the Linux kernel packages are
properly installed. That's also a way to double check the setup for
generating installation images is ready to prepare official Buster
images in a few days.
</p>

<p>
Installation reports for this specific D-I Buster RC 3 release are
welcome as always, but users may want to join the fun of
<a href="https://lists.debian.org/debian-cd/2019/06/msg00024.html">testing the official installation images on Saturday!</a>
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>base-installer:
    <ul>
      <li>Enable installation of Recommends while installing the
        kernel (<a href="https://bugs.debian.org/929667">#929667</a>).</li>
    </ul>
  </li>
  <li>debian-installer-utils:
    <ul>
      <li>Always set APT option if --{with,no}-recommends options are
        used (<a href="https://bugs.debian.org/931287">#931287</a>).</li>
    </ul>
  </li>
  <li>debian-cd:
    <ul>
      <li>Include Buster release notes on DVD, 16G USB and BD images.</li>
    </ul>
  </li>
  <li>grub2:
    <ul>
      <li>Add shim-signed to Recommends for
        grub-efi-{arm64,i386}-signed packages (<a href="https://bugs.debian.org/931038">#931038</a>).</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>76 languages are supported in this release.</li>
  <li>Full translation for 39 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
