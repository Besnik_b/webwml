#use wml::debian::template title="Debian-Accessibility - Software"
#use wml::debian::translation-check translation="82f3d1721149ec88f5974028ed2c2d8d454b4071" maintainer="Besnik Bleta"
{#style#:<link rel="stylesheet" href="style.css" type="text/css" />:#style#}

<define-tag a11y-pkg endtag=required>
<preserve name tag url/>
<set-var %attributes>
<h3><if "<get-var url>"
        <a href="<get-var url>" name="<get-var tag>"><get-var name></a>
      <a href="https://packages.debian.org/<get-var tag>" name="<get-var tag>"><get-var name></a>></h3>
  %body
<restore name tag url/>
</define-tag>

<h2><a id="speech-synthesis" name="speech-synthesis">Speech Synthesis and related APIs</a></h2>

<p>
  A thorough list is available on the
  <a href="https://blends.debian.org/accessibility/tasks/speechsynthesis">speechsynthesis task page</a>
</p>

<a11y-pkg name="EFlite" tag=eflite url="http://eflite.sourceforge.net/">
<p>
  A speech server for <a href="#emacspeak">Emacspeak</a> and
  <a href="#yasr">yasr</a> (or other screen readers) that allows them to
  interface with <a href="#flite">Festival Lite</a>, a free text-to-speech
  engine developed at the CMU Speech Center as an off-shoot of
  <a href="#festival">Festival</a>.
</p>
<p>
  Due to limitations inherited from its backend, EFlite does only provide
  support for the English language at the moment.
</p>
</a11y-pkg>
<a11y-pkg name="eSpeak" tag=espeak>
<p>
eSpeak/eSpeak-NG is a software speech synthesizer for English, and some other
languages.
</p>
<p>
eSpeak produces good quality English speech. It uses a different synthesis
method from other open source text to speech (TTS) engines (no concatenative
speech synthesis, therefore it also has a very small footprint), and sounds
quite different. It's perhaps not as natural or <q>smooth</q>, but some find the
articulation clearer and easier to listen to for long periods.
</p>
<p>
It can run as a command line program to speak text from a file or from stdin.
It also works well as a <q>Talker</q> with the KDE text to speech system (KTTS),
as an alternative to <a href="#festival">Festival</a> for example. As such, it
can speak text which has been selected into the clipboard, or directly from the
Konqueror browser or the Kate editor.
</p>
  <ul>
    <li>Includes different Voices, whose characteristics can be altered.</li>
    <li>Can produce speech output as a WAV file.</li>
    <li>Can translate text to phoneme codes, so it could be adapted as a front end
    for another speech synthesis engine.</li>
    <li>Potential for other languages. Rudimentary (and probably humourous)
    attempts at German and Esperanto are included.</li>
    <li>Compact size. The program and its data total about 350 kbytes.</li>
    <li>Written in C++.</li>
  </ul>
<p>
eSpeak can also be used with <a href="#speech-dispatcher">Speech Dispatcher</a>.
</p>
</a11y-pkg>
<a11y-pkg name="Festival Lite" tag=flite>
<p>
  A small fast run-time speech synthesis engine.  It is the latest
  addition to the suite of free software synthesis tools including
  University of Edinburgh's Festival Speech Synthesis System and
  Carnegie Mellon University's FestVox project, tools, scripts and 
  documentation for building synthetic voices.  However, flite itself
  does not require either of these systems to run.
</p>
<p>
  It currently only supports the English language.
</p>
</a11y-pkg>
<a11y-pkg name="Festival" tag="festival"
          url="http://www.cstr.ed.ac.uk/projects/festival/">
<p>
  A general multi-lingual speech synthesis system developed
  at the <a href="http://www.cstr.ed.ac.uk/">CSTR</a> [<i>C</i>entre for
  <i>S</i>peech <i>T</i>echnology <i>R</i>esearch] of
  <a href="http://www.ed.ac.uk/text.html">University of Edinburgh</a>.
</p>
<p>
  Festival offers a full text to speech system with various APIs, as well an
  environment for development and research of speech synthesis techniques.
  It is written in C++ with a Scheme-based command interpreter for general
  control.
</p>
<p>
  Besides research into speech synthesis, festival is useful as a stand-alone
  speech synthesis program. It is capable of producing clearly understandable
  speech from text.
</p>
</a11y-pkg>
<a11y-pkg name="Speech Dispatcher" tag="speech-dispatcher"
          url="http://www.freebsoft.org/speechd">
<p>
  Provides a device independent layer for speech synthesis.
  It supports various software and hardware speech synthesizers as
  backends and provides a generic layer for synthesizing speech and
  playing back PCM data via those different backends to applications.
</p>
<p>
  Various high level concepts like enqueueing vs. interrupting speech
  and application specific user configurations are implemented in a device
  independent way, therefore freeing the application programmer from having
  to yet again reinvent the wheel.
</p>
</a11y-pkg>


<h2><a name="i18nspeech">Internationalised Speech Synthesis</a></h2>
<p>
All the currently available free solutions for software based speech
synthesis seem to share one common deficiency: They are mostly limited to
English, providing only very marginal support for other languages, or in
most cases none at all.
Among all the free software speech synthesizers for Linux, only CMU
Festival supports more than one natural language. CMU Festival can
synthesize English, Spanish and Welsh. German is not
supported. French is not supported. Russian is not supported. When
internationalization and localization are the trends in software and
web services, is it reasonable to require blind people interested in
Linux to learn English just to understand their computer's output and to
conduct all their correspondence in a foreign tongue?
</p>
<p>
Unfortunately, speech synthesis is not really Jane Hacker's favourite
homebrew project.  Creating an intelligible software speech
synthesizer involves time-consuming tasks.
Concatenative speech synthesis requires the careful creation of a
phoneme database containing all the possible combinations of sounds
for the target language.
Rules that determine the transformation of the text representation
into individual phonemes also need to be developed and fine-tuned,
usually requiring the division of the stream of characters into
logical groups such as sentences, phrases and words. Such lexical
analysis requires a language-specific lexicon seldom released under a
free license.
</p>
<p>
One of the most promising speech synthesis systems is Mbrola, with
phoneme databases for over several dozen different languages. The synthesis
itself is free software. Unfortunately the phoneme databases are for
non-military and non-commercial use only. We are lacking free phoneme
databases in order to be used in the Debian Operating System.
</p>
<p>
Without a broadly multi-lingual software speech synthesizer, Linux
cannot be accepted by assistive technology providers and people with
visual disabilities. What can we do to improve this?
</p>
<p>
There are basically two approaches possible:
</p>
<ol>
<li>Organize a group of people willing to help in this regard, and
try to actively improve the situation.  This might get a bit complicated,
since a lot of specific knowledge about speech synthesis will be required,
which isn't that easy if done via an autodidactic approach.  However, this
should not discourage you.  If you think you can motivate a group of
people large enough to achieve some improvements, it would be worthwhile
to do.</li>
<li>Obtain funding and hire some institute which already has the
know how to create the necessary phoneme databases, lexica and transformation
rules.  This approach has the advantage that it has a better probability
of generating quality results, and it should also achieve some improvements
much earlier than the first approach.  Of course, the license under which all
resulting work would be released should be agreed on in advance, and it should
pass the DFSG requirements. The ideal solution would of course
be to convince some university to undergo such a project on their own
dime, and contribute the results to the Free Software community.</li>
</ol>
<p>
Last but not least, it seems most of the commercially successful
speech synthesis products nowadays do no longer use concatenative speech
synthesis, mainly because the sound databases
consume a lot of diskspace.  This is not really desireable
for small embedded products, like for instance speech
on a mobile phone.  Recently released Free software like <a href="#espeak">eSpeak</a>
seem to try this approach, which might be very worthwhile
to look at.
</p>


<h2><a id="emacs" name="emacs">Screen review extensions for Emacs</a></h2>
<a11y-pkg name="Emacspeak" tag="emacspeak"
          url="http://emacspeak.sourceforge.net/">
<p>
  A speech output system that will allow someone who cannot see
  to work directly on a UNIX system.  Once you start Emacs with
  Emacspeak loaded, you get spoken feedback for everything you do.  Your
  mileage will vary depending on how well you can use Emacs.  There is nothing
  that you cannot do inside Emacs :-).  This package includes speech servers
  written in tcl to support the DECtalk Express and DECtalk MultiVoice
  speech synthesizers.  For other synthesizers, look for separate
  speech server packages such as Emacspeak-ss or <a href="#eflite">eflite</a>.
</p>
</a11y-pkg>
<a11y-pkg name="speechd-el" tag="speechd-el"
          url="http://www.freebsoft.org/speechd-el">
<p>
  Emacs client to speech synthesizers, Braille displays
  and other alternative output interfaces.  It provides full speech and
  Braille output environment for Emacs.  It is aimed primarily at
  visually impaired users who need non-visual communication with Emacs,
  but it can be used by anybody who needs sophisticated speech or other
  kind of alternative output from Emacs.
</p>
</a11y-pkg>


<h2><a id="console" name="console">Console (text-mode) screen readers</a></h2>

<p>
  A thorough list is available on the
  <a href="https://blends.debian.org/accessibility/tasks/console">console screen readers task page</a>
</p>

<a11y-pkg name="BRLTTY" tag="brltty" url="https://brltty.app/">
<p>
  A daemon which provides access to the Linux console for a blind
  person using a soft braille display.
  It drives the braille terminal and provides complete screen review
  functionality.
</p>
<p>
  The Braille devices supported by BRLTTY are listed on the
  <a href="https://brltty.app/doc/KeyBindings/#braille-device-bindings">
  BRLTTY device documentation</a>
</p>
<p>
  BRLTTY also provides a client/server based infrastructure for applications
  wishing to utilize a Braille display.  The daemon process listens for
  incoming TCP/IP connections on a certain port.  A shared object library
  for clients is provided in the package
  <a href="https://packages.debian.org/libbrlapi">libbrlapi</a>.  A static
  library, header files and documentation is provided in package
  <a href="https://packages.debian.org/libbrlapi-dev">libbrlapi-dev</a>.  This
  functionality is for instance used by <a href="#gnome-orca">Orca</a>
  to provide support for display types which are not yet support by Gnopernicus
  directly.
</p>
</a11y-pkg>
<a11y-pkg name="Yasr" tag="yasr" url="http://yasr.sourceforge.net/">
<p>
  A general-purpose console screen reader for GNU/Linux and
  other UNIX-like operating systems.  The name <q>yasr</q> is an acronym that
  can stand for either <q>Yet Another Screen Reader</q> or <q>Your All-purpose
  Screen Reader</q>.
</p>
<p>
  Currently, yasr attempts to support the Speak-out, DEC-talk, BNS, Apollo,
  and DoubleTalk hardware synthesizers.  It is also able to communicate with
  Emacspeak speech servers and can thus be used with synthesizers not directly
  supported, such as <a href="#flite">Festival Lite</a> (via
  <a href="#eflite">eflite</a>) or FreeTTS.
</p>
<p>
  Yasr works by opening a pseudo-terminal and running a shell, intercepting
  all input and output.  It looks at the escape sequences being sent and
  maintains a virtual <q>window</q> containing what it believes to be on the
  screen.  It thus does not use any features specific to Linux and can be
  ported to other UNIX-like operating systems without too much trouble.
</p>
</a11y-pkg>


<h2><a id="gui" name="gui">Graphical User Interfaces</a></h2>
<p>
Accessibility of graphical user interfaces on UNIX platforms has only recently
received a significant upswing with the various development efforts around the
<a href="http://www.gnome.org/">GNOME Desktop</a>, especially the
<a href="https://wiki.gnome.org/Accessibility">GNOME Accessibility Project</a>.
</p>


<h2><a id="gnome" name="gnome">GNOME Accessibility Software</a></h2>

<p>
  A thorough list is available on the
  <a href="https://blends.debian.org/accessibility/tasks/gnome">Gnome accessibility task page</a>
</p>

<a11y-pkg name="Assistive Technology Service Provider Interface" tag="at-spi">
<p>
  This package contains the core components of GNOME Accessibility.
  It allows Assistive technology providers like screen readers to
  query all applications running on the desktop for accessibility
  related information as well as provides bridging mechanisms to support
  other toolkits than GTK.
</p>
<p>
  Bindings to the Python language are provided in package
  <a href="https://packages.debian.org/python-at-spi">python-at-spi</a>.
</p>
</a11y-pkg>
<a11y-pkg name="The ATK accessibility toolkit" tag="atk">
<p>
  ATK is a toolkit providing accessibility interfaces for applications or
  other toolkits. By implementing these interfaces, those other toolkits or
  applications can be used with tools such as screen readers, magnifiers, and
  other alternative input devices.
</p>
<p>
  The runtime part of ATK, needed to run applications built with it is available
  in package <a href="https://packages.debian.org/libatk1.0-0">libatk1.0-0</a>.
  Development files for ATK, needed for compilation of programs or toolkits
  which use it are provided by package <a href="https://packages.debian.org/libatk1.0-dev">libatk1.0-dev</a>.
  Ruby language bindings are provided by package
  <a href="https://packages.debian.org/ruby-atk">ruby-atk</a>.
</p>
</a11y-pkg>
<a11y-pkg name="gnome-accessibility-themes" tag="gnome-accessibility-themes">
<p>
  The gnome-accessibility-themes package contains some high accessibility themes
  for the GNOME desktop environment, designed for the visually impaired.
</p>
<p>
  A total of 7 themes are provided, providing combinations of high, low
  or inversed contrast, as well as enlarged text and icons.
</p>
</a11y-pkg>
<a11y-pkg name="gnome-orca" tag="gnome-orca"
          url="http://live.gnome.org/Orca">
<p>
  Orca is a flexible and extensible screen reader
  that provides access to the graphical desktop via user-customizable
  combinations of speech, braille, and/or magnification.  Under
  development by the Sun Microsystems, Inc., Accessibility Program
  Office since 2004, Orca has been created with early input from and
  continued engagement with its end users.
</p>
<p>
  Orca can use <a href="#speech-dispatcher">Speech Dispatcher</a> for delivering speech
  output to the user.  <a href="#brltty">BRLTTY</a> is used for braille display
  support (and for seamless console and GUI braille review integration).
</p>
</a11y-pkg>


<h2><a id="kde" name="kde">KDE Accessibility Software</a></h2>

<p>
  A thorough list is available on the
  <a href="https://blends.debian.org/accessibility/tasks/kde">KDE accessibility task page</a>
</p>

<a11y-pkg name="kmag" tag="kmag">
<p>
  Magnify a part of the screen just as you would use a lens to magnify a
  newspaper fine-print or a photograph.  This application is useful for
  a variety of people: from researchers to artists to web-designers
  to people with low vision.
</p>
</a11y-pkg>


<h2><a id="input" name="input">Non-standard input methods</a></h2>

<p>
  A thorough list is available on the
  <a href="https://blends.debian.org/accessibility/tasks/input">Input methods task page</a>
</p>

<a11y-pkg name="Dasher" tag="dasher" url="http://www.inference.phy.cam.ac.uk/dasher/">
<p>
  Dasher is an information-efficient text-entry interface, driven by natural
  continuous pointing gestures. Dasher is a competitive text-entry system
  wherever a full-size keyboard cannot be used - for example,
</p>
  <ul>
   <li>on a palmtop computer</li>
   <li>on a wearable computer</li>
   <li>when operating a computer one-handed, by joystick, touchscreen,
       trackball, or mouse</li>
   <li>when operating a computer with zero hands (i.e., by head-mouse or by
       eyetracker).</li>
  </ul>
<p>
  The eyetracking version of Dasher allows an experienced user to write text
  as fast as normal handwriting - 25 words per minute; using a mouse,
  experienced users can write at 39 words per minute.
</p>
<p>
  Dasher uses a more advanced prediction algorithm than the T9(tm) system
  often used in mobile phones, making it sensitive to surrounding context.
</p>
</a11y-pkg>
<a11y-pkg name="Caribou" tag="caribou" url="https://wiki.gnome.org/Projects/Caribou">
<p>
  Caribou is an input assistive technology intended for switch and pointer
users. It provides a configurable on-screen keyboard with scanning mode.
</p>
</a11y-pkg>
