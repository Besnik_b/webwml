#use wml::debian::template title="Debian &ldquo;bullseye&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="c8265f664931f5ce551446ba751d63d63b70ce86" maintainer="Besnik Bleta"

<if-stable-release release="bullseye">

<p>Debian <current_release_bullseye> was
released on <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 was initially released on <:=spokendate('XXXXXXXX'):>."
/>
The release included many major
changes, described in
our <a href="$(HOME)/News/XXXX/XXXXXXXX">press release</a> and
the <a href="releasenotes">Release Notes</a>.</p>

#<p><strong>Debian 11 has been superseded by
#<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>However, bullseye benefits from Long Term Support (LTS) until
#the end of xxxxx 20xx. The LTS is limited to i386, amd64, armel, armhf and arm64.
#All other architectures are no longer supported in bullseye.
#For more information, please refer to the <a
#href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
#</strong></p>

<p>To obtain and install Debian, see
the <a href="debian-installer/">installation information</a> page and the
<a href="installmanual">Installation Guide</a>. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Computer architectures supported at initial release of bullseye:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
<a href="reportingbugs">report other issues</a> to us.</p>

<p>Last but not least, we have a list of <a href="credits">people who take
credit</a> for making this release happen.</p>
</if-stable-release>

<if-stable-release release="buster">

<p>The code name for the next major Debian release after <a
href="../buster/">buster</a> is <q>bullseye</q>.</p>

<p>This release started as a copy of buster, and is currently in a state
called <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">testing</a></q>.
This means that things should not break as badly as in unstable or
experimental distributions, because packages are allowed to enter this
distribution only after a certain period of time has passed, and when they
don't have any release-critical bugs filed against them.</p>

<p>Please note that security updates for <q>testing</q> distribution are
<strong>not</strong> yet managed by the security team. Hence, <q>testing</q> does
<strong>not</strong> get security updates in a timely manner.
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
You are encouraged to switch your
sources.list entries from testing to buster for the time being if you
need security support. See also the entry in the
<a href="$(HOME)/security/faq#testing">Security Team's FAQ</a> for
the <q>testing</q> distribution.</p>

<p>There may be a <a href="releasenotes">draft of the release notes available</a>.
Please also <a href="https://bugs.debian.org/release-notes">check the
proposed additions to the release notes</a>.</p>

<p>For installation images and documentation about how to install <q>testing</q>,
see <a href="$(HOME)/devel/debian-installer/">the Debian-Installer page</a>.</p>

<p>To find out more about how the <q>testing</q> distribution works, check
<a href="$(HOME)/devel/testing">the developers' information about it</a>.</p>

<p>People often ask if there is a single release <q>progress meter</q>.
Unfortunately there isn't one, but we can refer you to several places
that describe things needed to be dealt with for the release to happen:</p>

<ul>
  <li><a href="https://release.debian.org/">Generic release status page</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Release-critical bugs</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Base system bugs</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bugs in standard and task packages</a></li>
</ul>

<p>In addition, general status reports are posted by the release manager
to the <a href="https://lists.debian.org/debian-devel-announce/">\
debian-devel-announce mailing list</a>.</p>

</if-stable-release>
