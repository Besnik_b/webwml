#use wml::debian::translation-check translation="62a641e1944c721e80cb7778920f8b8acd1602ef" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Three security issues were discovered in OpenSSL: A timing attack against
ECDSA, a padding oracle in PKCS7_dataDecode() and CMS_decrypt_set1_pkey()
and it was discovered that a feature of the random number generator (RNG)
intended to protect against shared RNG state between parent and child
processes in the event of a fork() syscall was not used by default.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 1.1.0l-1~deb9u1.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.1.1d-0+deb10u1.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4539.data"
