#use wml::debian::translation-check translation="1d182b3abd199000857dae0786cf70a840908dd1" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Joe Vennix discovered that sudo, a program designed to provide limited
super user privileges to specific users, when configured to allow a user
to run commands as an arbitrary user via the ALL keyword in a Runas
specification, allows to run commands as root by specifying the user ID
-1 or 4294967295. This could allow a user with sufficient sudo
privileges to run commands as root even if the Runas specification
explicitly disallows root access.</p>

<p>Details can be found in the upstream advisory at
<a href="https://www.sudo.ws/alerts/minus_1_uid.html">https://www.sudo.ws/alerts/minus_1_uid.html</a> .</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1.8.19p1-2.1+deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.8.27-1+deb10u1.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>For the detailed security status of sudo please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/sudo">https://security-tracker.debian.org/tracker/sudo</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4543.data"
