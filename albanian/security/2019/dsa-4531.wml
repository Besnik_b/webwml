#use wml::debian::translation-check translation="5ff7a077f0e13d9fe9cae52a517d81e9a15c05d7" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14821">CVE-2019-14821</a>

    <p>Matt Delco reported a race condition in KVM's coalesced MMIO
    facility, which could lead to out-of-bounds access in the kernel.
    A local attacker permitted to access /dev/kvm could use this to
    cause a denial of service (memory corruption or crash) or possibly
    for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14835">CVE-2019-14835</a>

    <p>Peter Pi of Tencent Blade Team discovered a missing bounds check
    in vhost_net, the network back-end driver for KVM hosts, leading
    to a buffer overflow when the host begins live migration of a VM.
    An attacker in control of a VM could use this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15117">CVE-2019-15117</a>

    <p>Hui Peng and Mathias Payer reported a missing bounds check in the
    usb-audio driver's descriptor parsing code, leading to a buffer
    over-read.  An attacker able to add USB devices could possibly use
    this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15118">CVE-2019-15118</a>

    <p>Hui Peng and Mathias Payer reported unbounded recursion in the
    usb-audio driver's descriptor parsing code, leading to a stack
    overflow.  An attacker able to add USB devices could use this to
    cause a denial of service (memory corruption or crash) or possibly
    for privilege escalation.  On the amd64 architecture, and on the
    arm64 architecture in buster, this is mitigated by a guard page
    on the kernel stack, so that it is only possible to cause a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15902">CVE-2019-15902</a>

    <p>Brad Spengler reported that a backporting error reintroduced a
    spectre-v1 vulnerability in the ptrace subsystem in the
    ptrace_get_debugreg() function.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 4.9.189-3+deb9u1.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 4.19.67-2+deb10u1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4531.data"
