#use wml::debian::translation-check translation="1975c9db16f264b28b074f2173a6bcda403cd1f1" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13723">CVE-2019-13723</a>

    <p>Yuxiang Li discovered a use-after-free issue in the bluetooth service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13724">CVE-2019-13724</a>

    <p>Yuxiang Li discovered an out-of-bounds read issue in the bluetooth
    service.</p></li>

</ul>

<p>For the oldstable distribution (stretch), security support for the chromium
package has been discontinued.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 78.0.3904.108-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4575.data"
