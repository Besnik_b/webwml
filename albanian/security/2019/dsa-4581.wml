#use wml::debian::translation-check translation="70be9894f7f65be4520c817dfd389d2ee7c87f04" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in git, a fast, scalable,
distributed revision control system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1348">CVE-2019-1348</a>

    <p>It was reported that the --export-marks option of git fast-import is
    exposed also via the in-stream command feature export-marks=...,
    allowing to overwrite arbitrary paths.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1387">CVE-2019-1387</a>

    <p>It was discovered that submodule names are not validated strictly
    enough, allowing very targeted attacks via remote code execution
    when performing recursive clones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19604">CVE-2019-19604</a>

    <p>Joern Schneeweisz reported a vulnerability, where a recursive clone
    followed by a submodule update could execute code contained within
    the repository without the user explicitly having asked for that. It
    is now disallowed for `.gitmodules` to have entries that set
    `submodule.&lt;name&gt;.update=!command`.</p></li>

</ul>

<p>In addition this update addresses a number of security issues which are
only an issue if git is operating on an NTFS filesystem (<a href="https://security-tracker.debian.org/tracker/CVE-2019-1349">CVE-2019-1349</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>).</p>
<p>For the oldstable distribution (stretch), these problems have been fixed
in version 1:2.11.0-3+deb9u5.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1:2.20.1-2+deb10u1.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>For the detailed security status of git please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4581.data"
