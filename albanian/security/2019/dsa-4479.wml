#use wml::debian::translation-check translation="6bde107f0f532a81d16326f47a68480ab5eb7957" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox web
browser, which could potentially result in the execution of arbitrary
code, cross-site scripting, spoofing, information disclosure, denial of
service or cross-site request forgery.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 60.8.0esr-1~deb9u1.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 60.8.0esr-1~deb10u1.</p>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2019-11719">\
CVE-2019-11719</a> and
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11729">\
CVE-2019-11729</a> are only addressed for stretch, in buster Firefox uses
the system-wide copy of NSS which will be updated separately.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>For the detailed security status of firefox-esr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4479.data"
