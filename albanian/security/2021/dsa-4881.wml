#use wml::debian::translation-check translation="1ee140ecea4124e2849e2bc5c2de4b63b838e5c5" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in cURL, an URL transfer library:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8169">CVE-2020-8169</a>

    <p>Marek Szlagor reported that libcurl could be tricked into prepending
    a part of the password to the host name before it resolves it,
    potentially leaking the partial password over the network and to the
    DNS server(s).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8177">CVE-2020-8177</a>

    <p>sn reported that curl could be tricked by a malicious server into
    overwriting a local file when using the -J (--remote-header-name) and
    -i (--include) options in the same command line.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8231">CVE-2020-8231</a>

    <p>Marc Aldorasi reported that libcurl might use the wrong connection
    when an application using libcurl's multi API sets the option
    CURLOPT_CONNECT_ONLY, which could lead to information leaks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8284">CVE-2020-8284</a>

    <p>Varnavas Papaioannou reported that a malicious server could use the
    PASV response to trick curl into connecting back to an arbitrary IP
    address and port, potentially making curl extract information about
    services that are otherwise private and not disclosed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8285">CVE-2020-8285</a>

    <p>xnynx reported that libcurl could run out of stack space when using
    the FTP wildcard matching functionality (CURLOPT_CHUNK_BGN_FUNCTION).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8286">CVE-2020-8286</a>

    <p>It was reported that libcurl didn't verify that an OCSP response
    actually matches the certificate it is intended to.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22876">CVE-2021-22876</a>

    <p>Viktor Szakats reported that libcurl does not strip off user
    credentials from the URL when automatically populating the Referer
    HTTP request header field in outgoing HTTP requests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22890">CVE-2021-22890</a>

    <p>Mingtao Yang reported that, when using an HTTPS proxy and TLS 1.3,
    libcurl could confuse session tickets arriving from the HTTPS proxy
    as if they arrived from the remote server instead. This could allow
    an HTTPS proxy to trick libcurl into using the wrong session ticket
    for the host and thereby circumvent the server TLS certificate check.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 7.64.0-4+deb10u2.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4881.data"
