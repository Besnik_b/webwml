#use wml::debian::translation-check translation="0f4f3e8210b7403850a5550da354ea08036f8c2f" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21148">CVE-2021-21148</a>

    <p>Mattias Buelens discovered a buffer overflow issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21149">CVE-2021-21149</a>

    <p>Ryoya Tsukasaki discovered a stack overflow issue in the Data Transfer
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21150">CVE-2021-21150</a>

    <p>Woojin Oh discovered a use-after-free issue in the file downloader.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21151">CVE-2021-21151</a>

    <p>Khalil Zhani discovered a use-after-free issue in the payments system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21152">CVE-2021-21152</a>

    <p>A buffer overflow was discovered in media handling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21153">CVE-2021-21153</a>

    <p>Jan Ruge discovered a stack overflow issue in the GPU process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21154">CVE-2021-21154</a>

    <p>Abdulrahman Alqabandi discovered a buffer overflow issue in the Tab Strip
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21155">CVE-2021-21155</a>

    <p>Khalil Zhani discovered a buffer overflow issue in the Tab Strip
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21156">CVE-2021-21156</a>

    <p>Sergei Glazunov discovered a buffer overflow issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21157">CVE-2021-21157</a>

    <p>A use-after-free issue was discovered in the Web Sockets implementation.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 88.0.4324.182-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4858.data"
