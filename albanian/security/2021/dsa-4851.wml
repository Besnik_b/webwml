#use wml::debian::translation-check translation="d006ce565fcf7220e57c35571bee0fdf83341df6" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Thomas Akesson discovered a remotely triggerable vulnerability in the
mod_authz_svn module in Subversion, a version control system. When using
in-repository authz rules with the AuthzSVNReposRelativeAccessFile
option an unauthenticated remote client can take advantage of this flaw
to cause a denial of service by sending a request for a non-existing
repository URL.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.10.4-1+deb10u2.</p>

<p>We recommend that you upgrade your subversion packages.</p>

<p>For the detailed security status of subversion please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/subversion">https://security-tracker.debian.org/tracker/subversion</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4851.data"
