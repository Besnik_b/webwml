#use wml::debian::translation-check translation="7d4e416de9b0d5870d3b56d250bdbed4f5cdde8b" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27815">CVE-2020-27815</a>

    <p>A flaw was reported in the JFS filesystem code allowing a local
    attacker with the ability to set extended attributes to cause a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27825">CVE-2020-27825</a>

    <p>Adam <q>pi3</q> Zabrocki reported a use-after-free flaw in the ftrace
    ring buffer resizing logic due to a race condition, which could
    result in denial of service or information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27830">CVE-2020-27830</a>

    <p>Shisong Qin reported a NULL pointer dereference flaw in the Speakup
    screen reader core driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28374">CVE-2020-28374</a>

    <p>David Disseldorp discovered that the LIO SCSI target implementation
    performed insufficient checking in certain XCOPY requests. An
    attacker with access to a LUN and knowledge of Unit Serial Number
    assignments can take advantage of this flaw to read and write to any
    LIO backstore, regardless of the SCSI transport settings.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29568">CVE-2020-29568 (XSA-349)</a>

    <p>Michael Kurth and Pawel Wieczorkiewicz reported that frontends can
    trigger OOM in backends by updating a watched path.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29569">CVE-2020-29569 (XSA-350)</a>

    <p>Olivier Benjamin and Pawel Wieczorkiewicz reported a use-after-free
    flaw which can be triggered by a block frontend in Linux blkback. A
    misbehaving guest can trigger a dom0 crash by continuously
    connecting / disconnecting a block frontend.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29660">CVE-2020-29660</a>

    <p>Jann Horn reported a locking inconsistency issue in the tty
    subsystem which may allow a local attacker to mount a
    read-after-free attack against TIOCGSID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29661">CVE-2020-29661</a>

    <p>Jann Horn reported a locking issue in the tty subsystem which can
    result in a use-after-free. A local attacker can take advantage of
    this flaw for memory corruption or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36158">CVE-2020-36158</a>

    <p>A buffer overflow flaw was discovered in the mwifiex WiFi driver
    which could result in denial of service or the execution of
    arbitrary code via a long SSID value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3347">CVE-2021-3347</a>

    <p>It was discovered that PI futexes have a kernel stack use-after-free
    during fault handling. An unprivileged user could use this flaw to
    crash the kernel (resulting in denial of service) or for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20177">CVE-2021-20177</a>

    <p>A flaw was discovered in the Linux implementation of string matching
    within a packet. A privileged user (with root or CAP_NET_ADMIN) can
    take advantage of this flaw to cause a kernel panic when inserting
    iptables rules.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 4.19.171-2.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4843.data"
