#use wml::debian::translation-check translation="0b9c89566ef13daffbc9185681dac5de2ae0d592" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in OpenSSL, a Secure
Sockets Layer toolkit. An overflow bug in the x64_64 Montgomery squaring
procedure, an integer overflow in CipherUpdate and a NULL pointer
dereference flaw X509_issuer_and_serial_hash() were found, which could
result in denial of service.</p>

<p>Additional details can be found in the upstream advisories
<a href="https://www.openssl.org/news/secadv/20191206.txt">\
https://www.openssl.org/news/secadv/20191206.txt</a> and
<a href="https://www.openssl.org/news/secadv/20210216.txt">\
https://www.openssl.org/news/secadv/20210216.txt</a>.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.1.1d-0+deb10u5.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4855.data"
