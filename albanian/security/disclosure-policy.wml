#use wml::debian::template title="Debian Vulnerability Disclosure Policy"
#use wml::debian::translation-check translation="5a5aa2dd64b9f93c369cb9c01eafa0bef14dafa8" maintainer="Besnik Bleta"

This document is the vulnerability disclosure and embargo policy of the
Debian project,
as required by Debian's status as a CVE Numbering Authority 
(Sub-CNA, <a href="https://cve.mitre.org/cve/cna/rules.html#section_2-3_record_management_rules">rule 2.3.3</a>).
Please also consult the <a href="faq">Security Team FAQ</a> for
additional information on Debian security procedures.

<h2>General Process</h2>

<p>The Debian security team will typically get back to vulnerability
reporters within several days. (See the FAQ entries for
<a href="faq#contact">contacting the security team</a>
and <a href="faq#discover">reporting vulnerabilities</a>.)

<p>Most of the software that is part of the Debian operating system
has not been specifically written for Debian. The Debian operating
system as a whole also serves as a the foundation for other GNU/Linux
distributions. This means that most vulnerabilities affecting Debian
also affect other distributions, and, in many cases, commercial
software vendors. As a result, the disclosure of vulnerabilities has
to be coordinated with other parties, not just the reporter and Debian
itself.</p>

<p>One forum for such coordination is the
<a href="https://oss-security.openwall.org/wiki/mailing-lists/distros">distros list</a>.
The Debian security team expects that experienced security
researchers contact the distros list and affected upstream projects
directly. The Debian security team will provide assistance to other
reporters as needed. Before involving third parties, permission from
reporters is obtained.</p>

<h2>Timelines</h2>

<p>As mentioned at the start, acknowledgment via email of the initial
report is expected to take several days at most.</p>

<p>Since addressing most vulnerabilities in Debian software requires
coordination among several parties (upstream developers, other
distributions), the time between initial report of a vulnerability and
its public disclosure varies a lot depending on the software and
organizations involved.</p>

<p>The distros list limits embargo periods (time between initial
report and disclosure) to two weeks. However, longer periods are not
uncommon, with additional coordination before sharing with the distros
list, to accommodate vendors with monthly or even quarterly release
cycles. Addressing Internet protocol vulnerabilities can take even
longer than that, and so does developing attempts to mitigate hardware
vulnerabilities in software.</p>

<h2>Avoiding Embargoes</h2>

<p>Since coordination in private tends to cause a lot of friction and
makes it difficult to involve the right subject matter experts, Debian
will encourage public disclosure of vulnerabilities even before a fix
has been developed, except when such an approach would clearly
endanger Debian users and other parties.</p>

<p>The Debian security team will often ask reporters of
vulnerabilities to file public bug reports in the appropriate bug
tracker(s) (such as the <a href="../Bugs/">Debian
bug tracking system</a>), providing assistance as needed.</p>

<p>An embargo is not needed for CVE assignment or credit in a security
advisory.</p>

<h2>CVE Assignment</h2>

<p>Debian, as a sub-CNA, only assigns CVE IDs for Debian
vulnerabilities. If a reported vulnerability does not meet this
criterion and is therefore out of scope for the Debian CNA, the Debian
security team will either arrange for CVE ID assignment from other
CNAs, or guide the reporter on submitting their own request for a CVE
ID.</p>

<p>A CVE assignment by the Debian CNA will be made public with the
publication of the Debian Security Advisory, or when the bug is filed
in the appropriate bug tracker(s).</p>

<h2>Vulnerability vs regular bug</h2>

<p>Due to the wide range of software that is part of the Debian
operating system, it is not possible to provide guidance what
constitutes a security vulnerability and what is just an ordinary
software bug.  When in doubt, please contact the Debian security
team.</p>

<h2>Bug Bounty Program</h2>

<p>Debian does not offer a bug bounty program. Independent parties may
encourage reporters to contact them about vulnerabilities in the
Debian operating system, but they are not endorsed by the Debian
project.</p>

<h2>Debian Infrastructure Vulnerabilities</h2>

<p>Reports of vulnerabilities in Debian infrastructure itself are
handled in the same way. If the infrastructure vulnerability is not
the result of a misconfiguration, but a vulnerability in the software
being used, the usual multi-party coordination is required, with
similar time frames as described above.</p>
