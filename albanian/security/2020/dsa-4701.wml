#use wml::debian::translation-check translation="90833ca5169a5ef4cdeac320dd3d7016a5d5f8d2" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>This update ships updated CPU microcode for some types of Intel CPUs and
provides mitigations for the Special Register Buffer Data Sampling
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>),
Vector Register Sampling
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0548">CVE-2020-0548</a>)
and L1D Eviction Sampling
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0549">CVE-2020-0549</a>)
hardware vulnerabilities.</p>

<p>The microcode update for HEDT and Xeon CPUs with signature 0x50654 which
was reverted in DSA 4565-2 is now included again with a fixed release.</p>

<p>The upstream update for Skylake-U/Y (signature 0x406e3) had to be
excluded from this update due to reported hangs on boot.</p>

<p>For details refer to
<a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html</a>,
<a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html</a></p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 3.20200609.2~deb9u1.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 3.20200609.2~deb10u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>For the detailed security status of intel-microcode please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4701.data"
