#use wml::debian::translation-check translation="73b976c71b8b4c13c331a478bd9111aa6f64627e" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues have been found in cacti, a server monitoring system,
potentially resulting in SQL code execution or information disclosure by
authenticated users.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16723">CVE-2019-16723</a>

    <p>Authenticated users may bypass authorization checks for viewing a graph
    by submitting requests with modified local_graph_id parameters.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17357">CVE-2019-17357</a>

    <p>The graph administration interface insufficiently sanitizes the
    template_id parameter, potentially resulting in SQL injection. This
    vulnerability might be leveraged by authenticated attackers to perform
    unauthorized SQL code execution on the database.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17358">CVE-2019-17358</a>

    <p>The sanitize_unserialize_selected_items function (lib/functions.php)
    insufficiently sanitizes user input before deserializing it,
    potentially resulting in unsafe deserialization of user-controlled
    data. This vulnerability might be leveraged by authenticated attackers
    to influence the program control flow or cause memory corruption.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 0.8.8h+ds1-10+deb9u1. Note that stretch was only affected by
<a href="https://security-tracker.debian.org/tracker/CVE-2018-17358">CVE-2018-17358</a>.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.2.2+ds1-2+deb10u2.</p>

<p>We recommend that you upgrade your cacti packages.</p>

<p>For the detailed security status of cacti please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cacti">\
https://security-tracker.debian.org/tracker/cacti</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4604.data"
