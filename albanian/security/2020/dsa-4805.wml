#use wml::debian::translation-check translation="5361d530d3775946c18aa99359098765fef27557" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in Apache Traffic Server, a reverse
and forward proxy server:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17508">CVE-2020-17508</a>

    <p>The ESI plugin was vulnerable to memory disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17509">CVE-2020-17509</a>

    <p>The negative cache option was vulnerable to cache poisoning.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 8.0.2+ds-1+deb10u4.</p>

<p>We recommend that you upgrade your trafficserver packages.</p>

<p>For the detailed security status of trafficserver please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/trafficserver">\
https://security-tracker.debian.org/tracker/trafficserver</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4805.data"
