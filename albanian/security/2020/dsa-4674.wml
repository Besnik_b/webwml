#use wml::debian::translation-check translation="d99c4fdf000143f5ceb1f98cf8f1c00088b2c7aa" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that roundcube, a skinnable AJAX based webmail
solution for IMAP servers, did not correctly process and sanitize
requests. This would allow a remote attacker to perform either a
Cross-Site Request Forgery (CSRF) forcing an authenticated user to be
logged out, or a Cross-Side Scripting (XSS) leading to execution of
arbitrary code.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 1.2.3+dfsg.1-4+deb9u4.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.3.11+dfsg.1-1~deb10u1.</p>

<p>We recommend that you upgrade your roundcube packages.</p>

<p>For the detailed security status of roundcube please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/roundcube">\
https://security-tracker.debian.org/tracker/roundcube</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4674.data"
