#use wml::debian::translation-check translation="2592e40c5d7143a6f575ff96f6127ba4fb3f18d5" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the Apache HTTPD server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1927">CVE-2020-1927</a>

    <p>Fabrice Perez reported that certain mod_rewrite configurations are
    prone to an open redirect.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1934">CVE-2020-1934</a>

    <p>Chamal De Silva discovered that the mod_proxy_ftp module uses
    uninitialized memory when proxying to a malicious FTP backend.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9490">CVE-2020-9490</a>

    <p>Felix Wilhelm discovered that a specially crafted value for the
    'Cache-Digest' header in a HTTP/2 request could cause a crash when
    the server actually tries to HTTP/2 PUSH a resource afterwards.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11984">CVE-2020-11984</a>

    <p>Felix Wilhelm reported a buffer overflow flaw in the mod_proxy_uwsgi
    module which could result in information disclosure or potentially
    remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11993">CVE-2020-11993</a>

    <p>Felix Wilhelm reported that when trace/debug was enabled for the
    HTTP/2 module certain traffic edge patterns can cause logging
    statements on the wrong connection, causing concurrent use of
    memory pools.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.4.38-3+deb10u4.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>For the detailed security status of apache2 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4757.data"
