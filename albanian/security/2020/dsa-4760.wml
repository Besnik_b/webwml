#use wml::debian::translation-check translation="669c87408de3af72c047aaa7ef3786903984b7ba" maintainer="Besnik Bleta"
<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in QEMU, a fast processor
emulator:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12829">CVE-2020-12829</a>

    <p>An integer overflow in the sm501 display device may result in denial of
    service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14364">CVE-2020-14364</a>

    <p>An out-of-bounds write in the USB emulation code may result in
    guest-to-host code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15863">CVE-2020-15863</a>

    <p>A buffer overflow in the XGMAC network device may result in denial of
    service or the execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16092">CVE-2020-16092</a>

    <p>A triggerable assert in the e1000e and vmxnet3 devices may result in
    denial of service.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 1:3.1+dfsg-8+deb10u8.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4760.data"
