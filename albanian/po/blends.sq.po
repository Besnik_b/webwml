msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Besnik Bleta <besnik@programeshqip.org>\n"
"Language-Team: \n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/blends/blend.defs:15
msgid "Metapackages"
msgstr "Tejpaketa"

#: ../../english/blends/blend.defs:18
msgid "Downloads"
msgstr "Shkarkime"

#: ../../english/blends/blend.defs:21
msgid "Derivatives"
msgstr "Të rrjedhur"

#: ../../english/blends/released.data:15
msgid ""
"The goal of Debian Astro is to develop a Debian based operating system that "
"fits the requirements of both professional and hobby astronomers. It "
"integrates a large number of software packages covering telescope control, "
"data reduction, presentation and other fields."
msgstr ""
"Synimi i Debian Astro-s është të zhvillojë një sistem operativ të bazuar në "
"Debian që plotëson domosdoshmëritë e të dy palëve të astronomëve, "
"profesionalët dhe amatorët. Integron një numër të madh paketash software-i "
"që mbulojnë kontroll teleskopi, reduktim të dhënash, paraqitje dhe fusha të "
"tjera."

#: ../../english/blends/released.data:23
msgid ""
"The goal of DebiChem is to make Debian a good platform for chemists in their "
"day-to-day work."
msgstr ""
"Synimi i DebiChem -it është ta bëjë Debian-in një platformë të përshtatshme "
"për kimistët gjatë punës së tyre të përditshme."

#: ../../english/blends/released.data:31
msgid ""
"The goal of Debian Games is to provide games in Debian from arcade and "
"adventure to simulation and strategy."
msgstr ""
"Synimi i Debian Games është të japë lojëra në Debian, nga ato të llojit "
"<em>arcade</em> dhe aventurë, deri te ato për simulime dhe strategji."

#: ../../english/blends/released.data:39
msgid ""
"The goal of Debian Edu is to provide a Debian OS system suitable for "
"educational use and in schools."
msgstr ""
"Synimi i Debian Edu-së është të furnizojë një sistem OS Debian të "
"përshtatshëm për përdorim edukativ dhe në shkolla."

#: ../../english/blends/released.data:47
msgid ""
"The goal of Debian GIS is to develop Debian into the best distribution for "
"Geographical Information System applications and users."
msgstr ""
"Synimi i Debian GIS-it është ta zhvillojë Debian-in drejt të qenit "
"shpërndarja më e mirë për aplikacione dhe përdorues Sistemesh Informacioni "
"Gjeografik (Geographical Information System)."

#: ../../english/blends/released.data:57
msgid ""
"The goal of Debian Junior is to make Debian an OS that children will enjoy "
"using."
msgstr ""
"Synimi i Debian Junior-it është ta bëjë Debian-in një OS përdorimi i të "
"cilit do t’u shijojë fëmijëve."

#: ../../english/blends/released.data:65
msgid ""
"The goal of Debian Med is a complete free and open system for all tasks in "
"medical care and research. To achieve this goal Debian Med integrates "
"related free and open source software for medical imaging, bioinformatics, "
"clinic IT infrastructure, and others within the Debian OS."
msgstr ""
"Synimi i Debian Med-it është një sistem plotësisht i lirë dhe i hapur për "
"krejt punët në kujdesin dhe kërkimin shëndetësor. Për të arritur këtë synim, "
"Debian Med integron <em>software</em> të lirë dhe me burim të hapur përkatës "
"për përftim dhe përdorim figurash diagnostikimi, bioinformatikë, "
"infrastrukturë TI klinikash, dhe të tjera, brenda Debian OS."

#: ../../english/blends/released.data:73
msgid ""
"The goal of Debian Multimedia is to make Debian a good platform for audio "
"and multimedia work."
msgstr ""
"Synimi i Debian Multimedia është ta bëjë Debian-in një platformë të "
"përshtatshme për punë me zërin dhe multimedian."

#: ../../english/blends/released.data:81
msgid ""
"The goal of Debian Science is to provide a better experience when using "
"Debian to researchers and scientists."
msgstr ""
"Synimi i Debian Science është t’u lejojë kërkuesve dhe shkencëtarëve një "
"punim më të mirë kur përdorin Debian-in."

#: ../../english/blends/released.data:89
msgid ""
"The goal of FreedomBox is to develop, design and promote personal servers "
"running free software for private, personal communications. Applications "
"include blogs, wikis, websites, social networks, email, web proxy and a Tor "
"relay on a device that can replace a wireless router so that data stays with "
"the users."
msgstr ""
"Synimi i FreedomBox-it është të zhvillojë, hartojë dhe promovojë shërbyes "
"personalë që xhirojnë <em>software</em> të lirë, për komunikime private, "
"personale. Aplikacionet përfshijnë blogje, wiki, sajte, rrjete shoqërore, "
"email, ndërmjetës web dhe një rele Tor në një pajisje që mund të zëvendësojë "
"një rrugëzues pa fill, që kështu të dhënat të mbeten me përdoruesit."

#: ../../english/blends/unreleased.data:15
msgid ""
"The goal of Debian Accessibility is to develop Debian into an operating "
"system that is particularly well suited for the requirements of people with "
"disabilities."
msgstr ""
"Synimi i Debian Accessibility është ta zhvillojë Debian-in në një sistem "
"operativ që është veçanërisht i përshtatshëm për nevoja personash me "
"paaftësi."

#: ../../english/blends/unreleased.data:23
msgid ""
"The goal of Debian Design is to provide applications for designers. This "
"includes graphic design, web design and multimedia design."
msgstr ""
"Synimi i Debian Design-it është të furnizojë aplikacione për dizajner. Ky "
"përfshin programe për punë grafike, <em>web design</em> dhe <em>multimedia "
"design</em>."

#: ../../english/blends/unreleased.data:30
msgid ""
"The goal of Debian EzGo is to provide culture-based open and free technology "
"with native language support and appropriate user friendly, lightweight and "
"fast desktop environment for low powerful/cost hardwares to empower human "
"capacity building and technology development  in many areas and regions, "
"like Africa, Afghanistan, Indonesia, Vietnam  using Debian."
msgstr ""
"Synimi i Debian EzGo-së është të furnizojë teknologji të lirë dhe të hapët, "
"të bazuar në kultura, me mbulim gjuhësh amtare dhe mjedis desktop të "
"përshtatshëm, miqësor ndaj përdoruesit, të peshës së lehtë, dhe të shpejtë, "
"për <em>hardware</em> me fuqi/kosto të ulët, për të fuqizuar krijim aftësish "
"njerëzore dhe zhvillim teknologjie në mjaft zona dhe rajone, bie fjala, "
"Afrikë, Afganistan, Indonezi, Vietnam, përmes përdorimit të Debian-it."

#: ../../english/blends/unreleased.data:38
msgid ""
"The goal of Debian Hamradio is to support the needs of radio amateurs in "
"Debian by providing logging, data mode and packet mode applications and more."
msgstr ""
"Synimi i Debian Hamradio-s është të mbulojë nevoja radioamatoristësh në "
"Debian, duke furnizuar aplikacione për regjistrim, <em>data mode</em> dhe "
"<em>packet mode</em>, etj."

#: ../../english/blends/unreleased.data:47
msgid ""
"The goal of DebianParl is to provide applications to support the needs of "
"parliamentarians, politicians and their staffers all around the world."
msgstr ""
"Synimi i DebianParl-it është të furnizojë aplikacione për të mbuluar nevoja "
"parlamentarësh, politikanësh dhe ndihmësve të tyre, anembanë botës."
