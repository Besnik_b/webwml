#use wml::debian::template title="Debian Merchandise" GEN_TIME="yes" BARETITLE=true
#use wml::debian::translation-check translation="3c06310c5d261bd2102e562e1b811d587413a5e4" maintainer="Besnik Bleta"

<p>
As Debian's popularity has grown, we have received many requests for Debian
merchandise. Being a non-profit organization, we don't make or sell anything
ourselves. Luckily, a number of companies have seen a potential market and
are trying to fill it. The information below is provided as a favor to our
loyal users. The ordering is purely alphabetical and no ranking or
endorsement of a specific vendor.
</p>

<p>
Some vendors contribute a portion of sales of Debian merchandise
back to Debian. This is indicated under each vendor entry.
We hope that vendors consider making donations to Debian.
</p>

<p>
In addition, some Debian contributors or local groups have produced Debian
branded merchandise and may have some stock still available, information about
that is available on the <a href="https://wiki.debian.org/Merchandise">wiki</a>.
</p>

#include "$(ENGLISHDIR)/events/merchandise.data"

<p>If you sell merchandise with a Debian theme and would like to be listed on
this page, send mail <strong>in English</strong> to
&lt;<a href="mailto:events@debian.org">events@debian.org</a>&gt;.
<br>
Please provide us with the following information:
<ul>
  <li>the company name,
  <li>the URL for your main page,
  <li>the URL where Debian merchandise can be found,
  <li>the list of Debian merchandise sold,
  <li>the list of available languages on the website,
  <li>the original country,
  <li>informations about delivery (international or not).
</ul>
We are only interested in advertising Debian related gear. 

<p>Since we are protective of our reputation, we take customer service seriously.
If we receive complaints about your service, you will be removed from the page.
</p>
