<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19961">CVE-2018-19961</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-19962">CVE-2018-19962</a>

    <p>Paul Durrant discovered that incorrect TLB handling could result in
    denial of service, privilege escalation or information leaks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19965">CVE-2018-19965</a>

    <p>Matthew Daley discovered that incorrect handling of the INVPCID
    instruction could result in denial of service by PV guests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19966">CVE-2018-19966</a>

    <p>It was discovered that a regression in the fix to address
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-15595">CVE-2017-15595</a>
    could result in denial of service, privilege
    escalation or information leaks by a PV guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19967">CVE-2018-19967</a>

    <p>It was discovered that an error in some Intel CPUs could result in
    denial of service by a guest instance.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 4.8.5+shim4.10.2+xsa282-1+deb9u11.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>For the detailed security status of xen please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4369.data"
# $Id: $
