<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that sphinxsearch, a fast standalone full-text SQL
search engine, could allow arbitrary files to be read by abusing a
configuration option.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
2.2.11-1.1+deb9u1.</p>

<p>We recommend that you upgrade your sphinxsearch packages.</p>

<p>For the detailed security status of sphinxsearch please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sphinxsearch">https://security-tracker.debian.org/tracker/sphinxsearch</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2882.data"
# $Id: $
