<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in the Dovecot email
server. The Common Vulnerabilities and Exposures project identifies the
following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14461">CVE-2017-14461</a>

     <p>Aleksandar Nikolic of Cisco Talos and <q>flxflndy</q> discovered that
     Dovecot does not properly parse invalid email addresses, which may
     cause a crash or leak memory contents to an attacker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15130">CVE-2017-15130</a>

     <p>It was discovered that TLS SNI config lookups may lead to excessive
     memory usage, causing imap-login/pop3-login VSZ limit to be reached
     and the process restarted, resulting in a denial of service. Only
     Dovecot configurations containing local_name { } or local { }
     configuration blocks are affected.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15132">CVE-2017-15132</a>

     <p>It was discovered that Dovecot contains a memory leak flaw in the
     login process on aborted SASL authentication.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:2.1.7-7+deb7u2.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1333.data"
# $Id: $
