<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The linux-base package has been updated to support the package of
Linux 4.9 that was recently added to Debian 8.  This resolves a
dependency that was not satisfiable by the jessie and jessie-security
suites.</p>

<p>This update also fixes a bug in version ordering in the linux-version
command, corrects the package name printed by the perf command when
linux-perf-4.9 is needed, and adds bash-completion support for the
perf command.</p>

<p>For Debian 8 <q>Jessie</q>, the new version is 4.5~deb8u1.</p>

<p>We recommend that you upgrade your linux-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1434.data"
# $Id: $
