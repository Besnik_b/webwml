<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues were discovered in mysql-connector-java, a Java database
(JDBC) driver for MySQL, that allow attackers to update, insert or
delete access to some of MySQL Connectors accessible data, unauthorized
read access to a subset of the data, and partial denial of service.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.1.49-0+deb8u1.</p>

<p>We recommend that you upgrade your mysql-connector-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2245.data"
# $Id: $
