<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were found in the Archive_Tar PHP module, used by
Drupal, which could result in the execution of arbitrary code if a
malicious user is allowed to upload tar archives.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
7.52-2+deb9u13.</p>

<p>We recommend that you upgrade your drupal7 packages.</p>

<p>For the detailed security status of drupal7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/drupal7">https://security-tracker.debian.org/tracker/drupal7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2466.data"
# $Id: $
