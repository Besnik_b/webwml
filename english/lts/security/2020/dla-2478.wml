<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the PostgreSQL database system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25694">CVE-2020-25694</a>

    <p>Peter Eisentraut found that database reconnections may drop options
    from the original connection, such as encryption, which could lead
    to information disclosure or a man-in-the-middle attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25695">CVE-2020-25695</a>

    <p>Etienne Stalmans reported that a user with permissions to create
    non-temporary objects in an schema can execute arbitrary SQL
    functions as a superuser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25696">CVE-2020-25696</a>

    <p>Nick Cleaton found that the \gset command modified variables that
    control the psql behaviour, which could result in a compromised or
    malicious server executing arbitrary code in the user session.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
9.6.20-0+deb9u1.</p>

<p>We recommend that you upgrade your postgresql-9.6 packages.</p>

<p>For the detailed security status of postgresql-9.6 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-9.6">https://security-tracker.debian.org/tracker/postgresql-9.6</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2478.data"
# $Id: $
