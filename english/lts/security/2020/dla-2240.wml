<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was reported that the BlueZ's HID and HOGP profile implementations
don't specifically require bonding between the device and the host.
Malicious devices can take advantage of this flaw to connect to a target
host and impersonate an existing HID device without security or to cause
an SDP or GATT service discovery to take place which would allow HID
reports to be injected to the input subsystem from a non-bonded source.</p>

<p>For the HID profile an new configuration option (ClassicBondedOnly) is
introduced to make sure that input connections only come from bonded
device connections. The options defaults to <q>false</q> to maximize device
compatibility.</p>

<p>Note that as a result of the significant changes between the previous
version in Jessie (based on upstream release 5.23) and the available
patches to address this vulnerability, it was decided that a backport of
the bluez package from Debian 9 <q>Stretch</q> was the only viable way to
address the referenced vulnerability.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.43-2+deb9u2~deb8u1.</p>

<p>We recommend that you upgrade your bluez packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2240.data"
# $Id: $
