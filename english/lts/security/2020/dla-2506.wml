<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Awstats, a web server log analyzer, was
vulnerable to path traversal attacks. A remote unauthenticated
attacker could leverage that to perform arbitrary code execution. The
previous fix did not fully address the issue when the default
/etc/awstats/awstats.conf is not present.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
7.6+dfsg-1+deb9u2.</p>

<p>We recommend that you upgrade your awstats packages.</p>

<p>For the detailed security status of awstats please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/awstats">https://security-tracker.debian.org/tracker/awstats</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2506.data"
# $Id: $
