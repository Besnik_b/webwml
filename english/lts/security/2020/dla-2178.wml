<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Following CVEs were reported against the awl source package:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11728">CVE-2020-11728</a>

    <p>An issue was discovered in DAViCal Andrew's Web Libraries (AWL)
    through 0.60. Session management does not use a sufficiently
    hard-to-guess session key. Anyone who can guess the microsecond
    time (and the incrementing session_id) can impersonate a session.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11729">CVE-2020-11729</a>

    <p>An issue was discovered in DAViCal Andrew's Web Libraries (AWL)
    through 0.60. Long-term session cookies, uses to provide
    long-term session continuity, are not generated securely, enabling
    a brute-force attack that may be successful.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.55-1+deb8u1.</p>

<p>We recommend that you upgrade your awl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2178.data"
# $Id: $
