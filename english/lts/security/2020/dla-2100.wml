<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>an out-of-bounds write vulnerability due to an integer overflow was reported in
libexif, a library to parse exif files. This flaw might be leveraged by remote
attackers to cause denial of service, or potentially execute arbitrary code via
crafted image files.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.6.21-2+deb8u1.</p>

<p>We recommend that you upgrade your libexif packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2100.data"
# $Id: $
