<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in node-bl, a Node.js module to access multiple
buffers with Buffer interface.
Due to a buffer over-read, uninitialized memory might be exposed by
providing crafted user input.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.1.2-1+deb9u1.</p>

<p>We recommend that you upgrade your node-bl packages.</p>

<p>For the detailed security status of node-bl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-bl">https://security-tracker.debian.org/tracker/node-bl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2698.data"
# $Id: $
