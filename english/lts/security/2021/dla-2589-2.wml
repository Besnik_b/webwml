<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>DLA 2589-1 incorrectly fixed <a href="https://security-tracker.debian.org/tracker/CVE-2020-26519">CVE-2020-26519</a> and also induced
regression where opening a PDF document resulted in a
SIGFPE crash, a floating point exception.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.9a+ds1-4+deb9u7.</p>

<p>We recommend that you upgrade your mupdf packages.</p>

<p>For the detailed security status of mupdf please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mupdf">https://security-tracker.debian.org/tracker/mupdf</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2589-2.data"
# $Id: $
