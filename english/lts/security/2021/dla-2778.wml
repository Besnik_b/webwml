<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in fig2dev, utilities
for converting XFig figure files. Buffer overflows, out-of-bounds reads and
NULL pointer dereferences could lead to a denial-of-service or other
unspecified impact.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1:3.2.6a-2+deb9u4.</p>

<p>We recommend that you upgrade your fig2dev packages.</p>

<p>For the detailed security status of fig2dev please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/fig2dev">https://security-tracker.debian.org/tracker/fig2dev</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2778.data"
# $Id: $
