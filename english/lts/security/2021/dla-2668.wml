<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Samba, SMB/CIFS file,
print, and login server for Unix</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10218">CVE-2019-10218</a>

    <p>A flaw was found in the samba client, where a malicious server can
    supply a pathname to the client with separators. This could allow
    the client to access files and folders outside of the SMB network
    pathnames. An attacker could use this vulnerability to create
    files outside of the current working directory using the
    privileges of the client user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14833">CVE-2019-14833</a>

    <p>A flaw was found in Samba, in the way it handles a user password
    change or a new password for a samba user. The Samba Active
    Directory Domain Controller can be configured to use a custom
    script to check for password complexity. This configuration can
    fail to verify password complexity when non-ASCII characters are
    used in the password, which could lead to weak passwords being set
    for samba users, making it vulnerable to dictionary attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14847">CVE-2019-14847</a>

    <p>A flaw was found in samba where an attacker can crash AD DC LDAP
    server via dirsync resulting in denial of service. Privilege
    escalation is not possible with this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14861">CVE-2019-14861</a>

    <p>Samba have an issue, where the (poorly named) dnsserver RPC pipe
    provides administrative facilities to modify DNS records and
    zones. Samba, when acting as an AD DC, stores DNS records in LDAP.
    In AD, the default permissions on the DNS partition allow creation
    of new records by authenticated users. This is used for example to
    allow machines to self-register in DNS. If a DNS record was
    created that case-insensitively matched the name of the zone, the
    ldb_qsort() and dns_name_compare() routines could be confused into
    reading memory prior to the list of DNS entries when responding to
    DnssrvEnumRecords() or DnssrvEnumRecords2() and so following
    invalid memory as a pointer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14870">CVE-2019-14870</a>

    <p>Samba have an issue, where the S4U (MS-SFU) Kerberos delegation
    model includes a feature allowing for a subset of clients to be
    opted out of constrained delegation in any way, either S4U2Self or
    regular Kerberos authentication, by forcing all tickets for these
    clients to be non-forwardable. In AD this is implemented by a user
    attribute delegation_not_allowed (aka not-delegated), which
    translates to disallow-forwardable. However the Samba AD DC does
    not do that for S4U2Self and does set the forwardable flag even if
    the impersonated client has the not-delegated flag set.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14902">CVE-2019-14902</a>

    <p>There is an issue in samba, where the removal of the right to
    create or modify a subtree would not automatically be taken away
    on all domain controllers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14907">CVE-2019-14907</a>

    <p>samba have an issue where if it is set with "log level = 3" (or
    above) then the string obtained from the client, after a failed
    character conversion, is printed. Such strings can be provided
    during the NTLMSSP authentication exchange. In the Samba AD DC in
    particular, this may cause a long-lived process(such as the RPC
    server) to terminate. (In the file server case, the most likely
    target, smbd, operates as process-per-client and so a crash there is harmless).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20254">CVE-2021-20254</a>

    <p>A flaw was found in samba. The Samba smbd file server must map
    Windows group identities (SIDs) into unix group ids (gids). The
    code that performs this had a flaw that could allow it to read
    data beyond the end of the array in the case where a negative
    cache entry had been added to the mapping cache. This could cause
    the calling code to return those values into the process token
    that stores the group membership for a user. The highest threat
    from this vulnerability is to data confidentiality and integrity.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2:4.5.16+dfsg-1+deb9u4.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>For the detailed security status of samba please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/samba">https://security-tracker.debian.org/tracker/samba</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2668.data"
# $Id: $
