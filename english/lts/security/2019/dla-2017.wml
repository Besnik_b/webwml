<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilites are fixed in Asterisk, an Open Source PBX and telephony toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13161">CVE-2019-13161</a>

    <p>An attacker was able to crash Asterisk when handling an SDP answer to an
outgoing T.38 re-invite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18610">CVE-2019-18610</a>

    <p>Remote authenticated Asterisk Manager Interface (AMI) users without
system authorization could execute arbitrary system commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18790">CVE-2019-18790</a>

    <p>A SIP call hijacking vulnerability.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:11.13.1~dfsg-2+deb8u7.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2017.data"
# $Id: $
