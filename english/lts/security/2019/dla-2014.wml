<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been identified in the VNC code of vino, a
desktop sharing utility for the GNOME desktop environment.</p>

<p>The vulnerabilities referenced below are issues that have originally been
reported against Debian source package libvncserver. The vino source
package in Debian ships a custom-patched and stripped down variant of
libvncserver, thus some of libvncserver's security fixes required porting
over.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6053">CVE-2014-6053</a>

    <p>The rfbProcessClientNormalMessage function in
    libvncserver/rfbserver.c in LibVNCServer did not properly handle
    attempts to send a large amount of ClientCutText data, which allowed
    remote attackers to cause a denial of service (memory consumption or
    daemon crash) via a crafted message that was processed by using a
    single unchecked malloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7225">CVE-2018-7225</a>

    <p>An issue was discovered in LibVNCServer.
    rfbProcessClientNormalMessage() in rfbserver.c did not sanitize
    msg.cct.length, leading to access to uninitialized and potentially
    sensitive data or possibly unspecified other impact (e.g., an integer
    overflow) via specially crafted VNC packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15681">CVE-2019-15681</a>

    <p>LibVNC contained a memory leak (CWE-655) in VNC server code, which
    allowed an attacker to read stack memory and could be abused for
    information disclosure. Combined with another vulnerability, it could
    be used to leak stack memory and bypass ASLR. This attack appeared to
    be exploitable via network connectivity.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.14.0-2+deb8u1.</p>

<p>We recommend that you upgrade your vino packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2014.data"
# $Id: $
