<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>ghostscript is vulnerable to multiple issues that can lead
to denial of service when processing untrusted content.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10219">CVE-2016-10219</a>

    <p>Application crash with division by 0 in scan conversion code triggered
    through crafted content.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10220">CVE-2016-10220</a>

    <p>Application crash with a segfault in gx_device_finalize() triggered
    through crafted content.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5951">CVE-2017-5951</a>

    <p>Application crash with a segfault in ref_stack_index() triggered
    through crafted content.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.05~dfsg-6.3+deb7u5.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-905.data"
# $Id: $
