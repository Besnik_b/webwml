<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Chris Evans discovered that the GStreamer 0.10 plugin used to decode
files in the FLIC format allowed execution of arbitrary code. Further
details can be found in his advisory at
<a href="https://scarybeastsecurity.blogspot.de/2016/11/0day-exploit-advancing-exploitation.html">\
https://scarybeastsecurity.blogspot.de/2016/11/0day-exploit-advancing-exploitation.html</a></p>

<p>This update removes the insecure FLIC file format plugin.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.10.31-3+nmu1+deb7u1.</p>

<p>We recommend that you upgrade your gst-plugins-good0.10 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-727.data"
# $Id: $
