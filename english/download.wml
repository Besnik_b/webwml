#use wml::debian::template title="Thank you for downloading Debian!" 
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/releases/arches.data"

{#meta#:
<meta http-equiv="refresh" content="3;url=<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">
:#meta#}

<p>This is Debian <:=substr '<current_initial_release>', 0, 2:>, codenamed <em><current_release_name></em>, netinst, for <: print $arches{'amd64'}; :>.</p>

<p>If your download does not start automatically, click <a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso</a>.</p>
<p>Download checksum: <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS">SHA512SUMS</a> <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS.sign">Signature</a></p>



<div class="tip">
	<p><strong>Important</strong>: Make sure to <a href="$(HOME)/CD/verify">verify your download with the checksum</a>.</p>
</div>

<p>Debian installer ISOs are hybrid images, which means they can be written directly to CD/DVD/BD media OR to <a href="https://www.debian.org/CD/faq/#write-usb">USB sticks</a>.</p>

<h2 id="h2-1">
	Other Installers</h2>

<p>Other installers and images, such as live systems, offline installers for systems without a network connection, installers for other CPU architectures, or cloud instances, can be found at <a href="$(HOME)/distrib/">Getting Debian</a>.</p>

<p>Unofficial installers with <a href="https://wiki.debian.org/Firmware"><strong>non-free firmware</strong></a>, helpful for some network and video adapters, can be downloaded from <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">Unofficial non-free images including firmware packages</a>.</p>

<h2 id="h2-2">Related Links</h2>

<p><a href="$(HOME)/releases/<current_release_name>/installmanual">Installation Guide</a></p>
<p><a href="$(HOME)/releases/<current_release_name>/releasenotes">Release Notes</a></p>
<p><a href="$(HOME)/CD/verify">ISO Verification Guide</a></p>
<p><a href="$(HOME)/CD/http-ftp/#mirrors">Alternate Download Sites</a></p>
<p><a href="$(HOME)/releases">Other Releases</a></p>


