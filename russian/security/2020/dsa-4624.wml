#use wml::debian::translation-check translation="8be684d647389ee3db99d941206fa9b5cbef2621" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В evince, простой программе для просмотра многостраничных документов, было обнаружено
несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000159">CVE-2017-1000159</a>

    <p>Тобиас Мюллер сообщил, что код для экспорта в DVI в evince
    может содержать уязвимость, приводящую к введению команд с помощью
    специально сформированных имён файлов.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>

    <p>Энди Нгуен сообщил, что функции tiff_document_render() и
    tiff_document_get_thumbnail() в движке документов в формате TIFF
    не обрабатывают ошибки функции TIFFReadRGBAImageOriented(), что приводит к
    раскрытию неинициализированной памяти при обработке файлов с TIFF-изображениями.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010006">CVE-2019-1010006</a>

    <p>В движке tiff было обнаружено переполнение буфера, которое может
    приводить к отказу в обслуживании или выполнению произвольного кода при открытии
    специально сформированного PDF-файла.</p></li>

</ul>

<p>В предыдущем стабильном выпуске (stretch) эти проблемы были исправлены
в версии 3.22.1-3+deb9u2.</p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 3.30.2-3+deb10u1. Стабильный выпуск подвержен только уязвимости
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>.</p>

<p>Рекомендуется обновить пакеты evince.</p>

<p>С подробным статусом поддержки безопасности evince можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/evince">\
https://security-tracker.debian.org/tracker/evince</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4624.data"
