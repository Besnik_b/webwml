#use wml::debian::template title="Мировые серверы-зеркала Debian" BARETITLE=true
#use wml::debian::translation-check translation="c1ef6c3811be792e129656bbfcf09866d9880eb5" maintainer="Lev Lamberov"

<p>Debian распространяется (<em>зеркалированием</em>) сотнями серверов в
Интернет. Используя ближайший к вам сервер, вы ускорите скачивание, а
также сократите нагрузку на наши центральные серверы и Интернет в
целом.</p>

<p class="centerblock">
  Зеркала Debian имеются во многих странах, и для некоторых стран у нас
  имеются псевдонимы вида <code>ftp.&lt;страна&gt;.debian.org</code>. Эти
  псевдонимы обычно указывают на зеркало, которое регулярно сихнронизируется, является быстрым
  и содержит все архитектуры Debian. Архив Debian
  всегда доступен по протоколу <code>HTTP</code> в каталоге <code>/debian</code>
  на сервере.
</p>

<p class="centerblock">
  Другие <strong>зеркала</strong> могут иметь ограничения на то, что
  они зеркалируют (из-за ограничения свободного места). То, что какой-то сайт
  не имеет псевдоним вида <code>ftp.&lt;страна&gt;.debian.org</code>, не обязательно означает, что
  он медленнее или содержит менее актуальные пакеты, чем зекрало
  <code>ftp.&lt;страна&gt;.debian.org</code>.
  В действительности, зеркало, содержащее вашу архитектуру и находящееся к вам ближе всего как
  к пользователю, а потому более быстрое, почти всегда является предпочтительным по отношению
  к другим зеркалам, находящимся дальше.
</p>

<p>Используйте ближайший к вам сайт для более быстрой загрузки, независимо
от того, имеется у него псевдоним страны или нет.
Для определения задержки доступа к сайту можно использовать программу
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a>; для определения скорости скачивания можно
использовать программу
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> или
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a>.
Заметим, что географическая близость часто не является важным фактором при
выборе зеркала.</p>

<p>
Если ваша система часто меняет своё местоположение, то вам лучше всего использовать <q>зеркало</q>,
определяемое с помощью глобальной системы <abbr title="Content Delivery Network">CDN</abbr>.
Для этой цели у Проекта Debian
имеется специальная служба, <code>deb.debian.org</code>,
вы можете использовать его в вашем файле sources.list, подробную информацию
можно найти на <a href="http://deb.debian.org/">веб-сайте этой службы</a>.

<p>
Дополнительная информация по зеркалам Debian:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">Зеркала Debian с псевдонимами стран</h2>

<table border="0" class="center">
<tr>
  <th>Страна</th>
  <th>Сайт</th>
  <th>Архитектуры</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Список зеркал архива Debian</h2>

<table border="0" class="center">
<tr>
  <th>Имя узла</th>
  <th>HTTP</th>
  <th>Архитектуры</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
