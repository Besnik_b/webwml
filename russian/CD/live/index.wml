#use wml::debian::cdimage title="Live-образы установки"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="8c86ac02236495359e82eed0e3b9e29905514fd7" maintainer="Lev Lamberov"

<p><q>Live-образы установки</q> содержат систему Debian, которая может запускаться
без изменения каких-либо файлов на жёстком диске, а также позволяет провести установку Debian
с использованием пакетов, содержащихся в этом образе.
</p>

<p><a name="choose_live"><strong>Подходит ли мне live-образ?</strong></a> Для того, чтобы разрешить
для себя этот вопрос, рассмотрите следующие пункты.
<ul>
<li><b>Сборки:</b> Живые образы поставлюятся нескольких <q>видов</q>, которые
позволяют выбрать окружение рабочего стола (GNOME, KDE, LXDE, Xfce,
Cinnamon и MATE). Многим пользователям подходит изначальный выбор
пакетов в этих сборках, а дополнительные пакеты, необходимые пользователям, можно
впоследствии установить по сети.
<li><b>Архитектура:</b> В настоящее время поставляются образы только для двух наиболее
популярных архитектур&nbsp;&mdash; 32-битного ПК (i386) и 64-битного ПК (amd64).
<li><b>Установщик:</b> Начиная с Debian 10 Buster, <q>живые</q> образы содержат
дружественный <a href="https://calamares.io">установщик Calamares</a>,
назависящую от дистрибутива установочную платформу, которая является альтернативой нашему
широко известному установщику <a href="$(HOME)/devel/debian-installer">Debian-Installer</a>.
<li><b>Размер:</b> Каждый отдельный live-образ намного меньше, чем полный набор образов DVD,
но больше, чем сетевой установочный образ.
<li><b>Языки:</b> Образы не содержат полный набор пакетов
языковой поддержки. Если вам требуются методы ввода, шрифты и дополнительные языковые
пакеты для вашего языка, то придётся устанавливать их дополнительно.
</ul>

<p>Для загрузки доступны следующие установочные live-образы:</p>

<ul>

  <li>Официальные <q>установочные live-образы</q> для <q>стабильного</q> выпуска &mdash; <a
  href="#live-install-stable">см. ниже</a></li>

</ul>


<h2 id="live-install-stable">Официальные установочные live-образы для <q>стабильного</q> выпуска</h2>

<p>Эти образы предлагаются в разных сборках, каждая из которых отличается от других по размеру, что разъяснено выше,  эти
образы подходят для того, чтобы попробовать систему Debian, включающую подобранный набор пакетов
по умолчанию, и затем установить её с того же самого носителя.</p>

<div class="line">
<div class="item col50">
<p><strong>DVD/USB (via <a
href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<p><q>Гибридные</q> ISO-образы подходят для записи на DVD-R(W) носители,
а также карты USB соответствующего размера. Если вы можете использовать
BitTorrent, пожалуйста, воспользуйтесь этим протоколом, поскольку он снижает загрузку наших серверов.</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol"><p><strong>DVD/USB</strong></p>
<p><q>Гибридные</q> ISO-файлы образов, подходящие для записи
на DVD-R(W) носители в зависимости от размера, а также на карты USB
соответствующего размера.</p>
	  <stable-live-install-iso-cd-images />
</div></div>

<p>Подробную информацию о том, что это за файлы и как их использовать, см.
в <a href="../faq/">ЧаВО</a>.</p>

<p>Если вы собираетесь установить Debian с загруженного <q>живого</q> образа,
ознакомьтесь с
<a href="$(HOME)/releases/stable/installmanual">подробной
информацией о процессе установки</a>.</p>

<p>Дополнительную информацию о системах Debian Live, предоставляемых этими образами,
см. на <a href="$(HOME)/devel/debian-live">странице проекта Debian Live</a>.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">
<p>
Если для какого-то оборудования в вашей системе <strong>требуется загрузка несвободных
микропрограмм</strong> вместе с драйвером устройства, то вы можете использовать один из
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tar-архивов распространённых пакетов с микропрограммами</a> или загрузить <strong>неофициальный</strong> образ,
включающий эти <strong>несвободные</strong> микропрограммы. Инструкции о том, как использовать эти tar-архивы,
а также общую информацию о загрузке микропрограмм во время установки можно найти
в <a href="../../releases/stable/amd64/ch06s04">руководстве по установке</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/">неофициальные
установочные образы для <q>стабильного</q> выпуска с микропрограммами</a>
</p>
</div>
