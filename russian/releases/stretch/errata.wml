#use wml::debian::template title="Debian 9 &mdash; Известные ошибки" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="ba4da87abca9b144e1cc1ff151bf45311e5da2b1" maintainer="Lev Lamberov"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Известные проблемы</toc-add-entry>
<toc-add-entry name="security">Проблемы безопасности</toc-add-entry>

<p>Команда безопасности Debian выпускает обновления пакетов в стабильном выпуске,
в которых они обнаружили проблемы, относящиеся к безопасности. Информацию о всех
проблемах безопасности, найденных в <q>Stretch</q>, смотрите на
<a href="$(HOME)/security/">страницах безопасности</a>.</p>

<p>Если вы используете APT, добавьте следующую строку в <tt>/etc/apt/sources.list</tt>,
чтобы получить доступ к последним обновлениям безопасности:</p>

<pre>
  deb http://security.debian.org/ stretch/updates main contrib non-free
</pre>

<p>После этого запустите <kbd>apt-get update</kbd> и затем
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Редакции выпусков</toc-add-entry>

<p>Иногда, в случаях множества критических проблем или обновлений безопасности, выпущенный
дистрибутив обновляется. Обычно эти выпуски обозначаются как
редакции выпусков.</p>

<ul>
  <li>Первая редакция, 9.1, была выпущена
      <a href="$(HOME)/News/2017/20170722">22 июля 2017 года</a>.</li>
  <li>Вторая редакция, 9.2, была выпущена
      <a href="$(HOME)/News/2017/20171007">7 октября 2017 года</a>.</li>
  <li>Третья редакция, 9.3, была выпущена
      <a href="$(HOME)/News/2017/2017120902">9 декабря 2017 года</a>.</li>
  <li>Четвёртая редакция, 9.4, была выпущена
      <a href="$(HOME)/News/2018/20180310">10 марта 2018 года</a>.</li>
  <li>Пятая редакция, 9.5, была выпущена
      <a href="$(HOME)/News/2018/20180714">14 июля 2018 года</a>.</li>
  <li>Шестая редакция, 9.6, была выпущена
      <a href="$(HOME)/News/2018/20181110">10 ноября 2018 года</a>.</li>
  <li>Седьмая редакция, 9.7, была выпущена
      <a href="$(HOME)/News/2019/20190123">23 января 2019 года</a>.</li>
  <li>Восьмая редакция, 9.8, была выпущена
      <a href="$(HOME)/News/2019/20190216">16 февраля 2019 года</a>.</li>
  <li>Девятая редакция, 9.9, была выпущена
      <a href="$(HOME)/News/2019/20190427">27 апреля 2019 года</a>.</li>
  <li>Десятая редакция, 9.10, была выпущена
      <a href="$(HOME)/News/2019/2019090702">7 сентября 2019 года</a>.</li>
  <li>Одиннадцатая редакция, 9.11, была выпущена
      <a href="$(HOME)/News/2019/20190908">8 сентября 2019 года</a>.</li>
  <li>Двенадцатая редакция, 9.12, была выпущена
      <a href="$(HOME)/News/2020/2020020802">8 февраля 2020 года</a>.</li>
  <li>Тринадцатая редакция, 9.13, была выпущена
      <a href="$(HOME)/News/2020/20200718">18 июля 2020 года</a>.</li>
</ul>

<ifeq <current_release_stretch> 9.0 "

<p>Для выпуска Debian 9 пока нет редакций.</p>" "

<p>Подробную информацию об изменениях между версиями 9.0 и <current_release_stretch/> смотрите в <a
href=http://http.us.debian.org/debian/dists/stretch/ChangeLog>\
журнале
изменений</a>.</p>"/>


<p>Исправления выпущенного стабильного дистрибутива часто должны пройти
усиленное тестирование, прежде чем они будут помещены в архив. Тем не менее,
эти исправления уже доступны в каталоге
<a href="http://ftp.debian.org/debian/dists/stretch-proposed-updates/">\
dists/stretch-proposed-updates</a> на всех зеркалах
Debian.</p>

<p>Если для обновления пакетов вы используете APT, то можете
установить предлагаемые обновления, добавив следующую строку в файл
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# предлагаемые дополнения для редакции 9-ого выпуска
  deb http://ftp.us.debian.org/debian stretch-proposed-updates main contrib non-free
</pre>

<p>После этого запустите <kbd>apt-get update</kbd> и затем
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="installer">Система установки</toc-add-entry>

<p>
Информацию об известных ошибках и обновлениях в системе установки смотрите
на страницах <a href="debian-installer/">системы установки</a>.
</p>
