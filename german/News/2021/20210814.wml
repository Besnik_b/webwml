<define-tag pagetitle>Debian 11 <q>Bullseye</q> veröffentlicht</define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff" maintainer="Erik Pfannenstein"

<p>
Nach zwei Jahren, einem Monat und neun Tagen Entwicklungszeit darf das
Debian-Projekt mit Stolz die neue stabile Version 11 (Codename <q>Bullseye</q>)
vorstellen. Diese Version wird dank der vereinten Kräfte des
<a href="https://security-team.debian.org/">Debian Security-Teams</a> 
und des <a href="https://wiki.debian.org/LTS">Debian Long Term Support</a>-Teams 
über die nächsten fünf Jahre Unterstützung erhalten.
</p>

<p>
Debian 11 <q>Bullseye</q> wird mit mehreren Desktop-Anwendungen und -Umgebungen
ausgeliefert. Dazu zählen unter anderem die Desktop-Umgebungen 
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24 und </li>
<li>Xfce 4.16.</li>
</ul>

<p>
Diese Veröffentlichung enthält insgesamt 59.551 Softwarepakete, davon 11.294
neue. 9.519 wurden als <q>obsolet</q> markiert und entfernt. 42.821 Pakete
wurden aktualisiert und 5.434 sind nicht geändert worden. 
</p>

<p>
<q>Bullseye</q> wird unsere erste Veröffentlichung, deren Linux-Kernel das
exFAT-Dateisystem unterstützt und die diese Unterstützung standardmäßig
verwendet, um exFAT-Dateisysteme einzuhängen. Daher ist es nicht länger
notwendig, die Dateisystem-im-Userspace-Implementation aus dem Paket exfat-fuse
einzusetzen. Werkzeuge zum Erzeugen und Überprüfen von exFAT-Dateiystemen
werden über das Paket exfatprogs bereitgestellt.
</p>

<p>
Die meisten modernen Drucker ermöglichen ein treiberloses Drucken und
Scannen, ohne einen herstellerspezifischen (und oft unfreien) Druckertreiber
heranziehen zu müssen.

<q>Bullseye</q> bringt ein neues Paket namens ipp-usb, welches das
von diesen Druckern unterstützte herstellerneutrale IPP-over-USB-Protokoll
verwendet. Somit kann ein USB-Gerät als Netzwerkdrucker angesprochen werden.
Das offizielle treiberlose SANE-Backend wird von sane-escl in libsane1
bereitgestellt und verwendet das eSCL-Protokoll.
</p>

<p>
Systemd aktiviert in <q>Bullseye</q> standardmäßig seine persistente
Journal-Funktionalität mit einem impliziten Rückfall auf flüchtigen Speicher.
Das erlaubt Anwendern, die nicht auf irgendwelche Spezialfunktionen der
traditionellen Protokollierungs-Daemons angewiesen sind, diese zu
deinstallieren und komplett aufs systemd-Journal umzustellen.
</p>

<p>
Das Debian Med-Team beteiligt sich am Kampf gegen COVID-19, indem es
Software zum Erforschen der Virus-Sequenzierungen und zum Bekämpfen der
Pandemie mit den Mitteln der Epidemiologie paketiert hat; ab hier wird es
sich auf Machine-Learning-Werkzeuge für diese beiden Felder konzentrieren.
Die Verwendung der Qualitätssicherung und der Continuous Integration ist
dabei essenziell für die in der Wissenschaft wichtigen durchgehend
reproduzierbaren Resulatate.

Debian Med Blend umfasst eine Reihe von leistungskritischen Anwendungen, die
jetzt von SIMD Everywhere profitieren. Um die Software-Pakete des Debian Med-Teams
zu erhalten, installieren Sie die Metapakete mit med-* im Namen, welche die
Version 3.6.x haben.
</p>

<p>
Für Chinesisch, Japanisch, Koreanisch und viele andere Sprachen steht
jetzt die neue Fcitx-5-Eingabemethode zur Verfügung. Sie ist die
Nachfolgerin der beliebten Fcitx4 in <q>Buster</q> und besitzt eine
wesentlich bessere Unterstützung für Wayland-Erweiterungen (dem
standardmäßigen Display-Server).
</p>

<p>
Für Debian 11 <q>Bulleye</q> sind zahlreiche Software-Pakete (über 72% aller
Pakete aus der Vorversion) aktualisiert worden, unter anderem:
</p>

<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux-Kernel 5.10</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>mehr als 59.000 andere gebrauchsfertige Softwarepakete, die aus
mehr als 30.000 Quellpaketen erzeugt worden sind.</li>
</ul>

<p>
Mit dieser umfangreichen Paketauswahl und der wie immer umfassenden
Architekturunterstützung bleibt Debian wieder einmal seinem Ziel des
›Universalen Betriebssystems‹ treu. Es eignet sich für viele verschiedene
Einsatzgebiete: Vom Desktoprechner bis zum Netbook, vom Entwicklungsserver
bis zum Rechencluster, sowie für Datenbank-, Web- und Storage-Server.
Gleichzeitig stellen zusätzliche Qualitätssicherungsmaßnahmen wie die
automatischen Installations- und Upgrade-Tests für alle Pakete
im Debian-Archiv sicher, dass <q>Bullseye</q> die hohen Erwartungen, die
seine Anwender an eine stabile Debian-Veröffentlichung stellen, erfüllt.
</p>

<p>
Insgesamt werden neun Architekturen unterstützt:
64-Bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-Bit PC / Intel IA-32 (<code>i386</code>),
64-Bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-Bit IBM S/390 (<code>s390x</code>),
<code>armel</code> für ARM und
<code>armhf</code> für ältere und neuere 32-Bit-Hardware,
außerdem <code>arm64</code> für die 64-Bit-<q>AArch64</q>-Architektur und für
MIPS, die <code>mipsel</code>-Architektur (little-endian) für 32-Bit-Hardware
und die <code>mips64el</code>-Architektur für 64-Bit-little-endian-Hardware.
</p>

<h3>Möchten Sie es versuchen?</h3>
<p>
Falls Sie Debian 11 <q>Bullseye</q> ohne Installation einfach ausprobieren
wollen, stehen Ihnen <a href="$(HOME)/CD/live/">Live-Abbilder</a> zur
Verfügung, die das gesamte Betriebssystem schreibgeschützt im Arbeitsspeicher
Ihres Rechners laufen lassen.</p>

<p>
Diese Live-Abbilder werden für die Architekturen <code>amd64</code> und
<code>i386</code> angeboten und sind für DVDs, USB-Sticks und Netboot-Umgebungen
verfügbar. Es stehen mehrere Desktop-Umgebungen zur Wahl: GNOME, KDE Plasma,
LXDE, LXQt, MATE und Xfce. Darüber hinaus gibt es für Debian Live
<q>Bullseye</q> ein Standard-Live-Abbild mit einem Basissystem ohne grafische Oberfläche.
</p>

<p>
Sollte Ihnen das Betriebssystem gefallen, haben Sie die Möglichkeit, es
aus dem Live-Abbild heraus auf die Festplatte Ihres Rechners zu installieren.
Das Live-Abbild enthält sowohl den unabhängigen Calamares-Installer als auch den
Standard-Debian-Installer. Weitere Informationen finden Sie in den
<a href="$(HOME)/releases/bullseye/releasenotes">Veröffentlichungshinweisen</a>
und dem 
<a href="$(HOME)/CD/live/">Live-Installations-Images</a>-Bereich auf der
Website.
</p>

<p>
Zur direkten Installation von Debian 11 <q>Bullseye</q> auf die Festplatte
Ihres Computers haben Sie die Auswahl aus einer Vielfalt von
Installationsmedien wie Blu-Ray Disc, DVD, CD, USB-Stick oder Netzwerk. Durch
diese Abbilder können mehrere Desktopumgebungen wie Cinnamon, GNOME, KDE
Plasma-Desktop und -Anwendungen, LXDE, LXQT, MATE und Xfce installiert werden.
<q>Multi-Architektur</q>-CDs ermöglichen die Installation verschiedener
Architekturen mit nur einem Datenträger. Sie können aber auch jederzeit
startfähige USB-Installationsmedien erzeugen 
(siehe die <a href="$(HOME)/releases/bullseye/installmanual">Installationsanleitung</a>
für zusätzliche Details).
</p>

<p>
Cloud-Anwendern offeriert Debian direkte Unterstützung für viele der
bekanntesten Cloud-Plattformen. In jedem Image-Marktplatz können offizielle
Debian-Images ausgewählt werden; darüber hinaus veröffentlicht Debian
<a href="https://cloud.debian.org/images/openstack/current/">vorgefertigte
OpenStack-Abbilder</a> für die Architekturen <code>amd64</code> und
<code>arm64</code>, die einfach heruntergeladen und in lokalen Clouds
eingesetzt werden können.
</p>

<p>
Debian kann jetzt in 76 Sprachen installiert werden, von denen die meisten
sowohl auf der textbasierten wie auch auf der grafischen Benutzeroberfläche
einstellbar sind.
</p>

<p>
Die Installationsmedien können ab sofort via 
<a href="$(HOME)/CD/torrent-cd/">BitTorrent</a> (die empfohlene Methode),
<a href="$(HOME)/CD/jigdo-cd/#which">Jigdo</a> oder 
<a href="$(HOME)/CD/http-ftp/">HTTP</a> heruntergeladen werden; siehe
<a href="$(HOME)/CD/">Debian auf CDs</a> für weitere Informationen.
<q>Bullseye</q> wird bald auch bei zahlreichen
<a href="$(HOME)/CD/vendors">Händlern</a> auf pysikalischen DVDs, CD-ROMs und
Blu-ray Discs zu erstehen sein.
</p>


<h3>Upgrade von Debian</h3>
<p>
Das Upgrade von der Vorversion Debian 10 (Codename <q>Buster</q>) auf Debian 11
wird bei den meisten Konfigurationen von der APT-Paketverwaltung automatisch
erledigt. Wie gewohnt können Debian-Systeme schmerzfrei, im laufenden Betrieb
und ohne erzwungene Ausfallzeiten auf die neue Version gebracht werden, das
Durchlesen der
<a href="$(HOME)/releases/bullseye/releasenotes">Veröffentlichungshinweise</a>
sowie der
<a href="$(HOME)/releases/bullseye/installmanual">Installationsanleitung</a>
nach möglichen Schwierigkeiten und für detaillierte Anweisungen wird trotzdem
sehr empfohlen. Die Veröffentlichungshinweise werden auch in den Wochen
nach der Veröffentlichung weiter ergänzt und in zusätzliche Sprachen übersetzt.
</p>

<h2>Über Debian</h2>

<p>
Debian ist ein freies Betriebssystem, das von tausenden Freiwilligen
aus aller Welt, die übers Internet vernetzt sind, entwickelt wird. Die
grundlegenden Stärken des Debian-Projekts sind seine Freiwilligenbasis, sein
Einsatz für den Debian-Gesellschaftsvertrag und für Freie Software sowie
sein Engagement, das bestmögliche Betriebssystem anzubieten. Diese neue
Veröffentlichung ist ein wichtiger Schritt in diese Richtung.
</p>

<h2>Kontaktionformationen</h2>

<p>
Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a> oder senden eine E-Mail (auf
Englisch) an &lt;press@debian.org&gt;.
</p>
