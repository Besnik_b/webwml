#use wml::debian::translation-check translation="2913230d58de12a2d0daeab2fd1f532a65fa5c2a"
<define-tag pagetitle>Uppdaterad Debian 9; 9.12 utgiven</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin tolfte uppdatering till dess
oldstabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den gamla stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction base-files "Update for the point release">
<correction cargo "New upstream version, to support Firefox ESR backports; fix bootstrap for armhf">
<correction clamav "New upstream release; fix denial of service issue [CVE-2019-15961]; remove ScanOnAccess option, replacing with clamonacc">
<correction cups "Fix validation of default language in ippSetValuetag [CVE-2019-2228]">
<correction debian-installer "Rebuild against oldstable-proposed-updates; set gfxpayload=keep in submenus too, to fix unreadable fonts on hidpi displays in netboot images booted with EFI; update USE_UDEBS_FROM default from unstable to stretch, to help users performing local builds">
<correction debian-installer-netboot-images "Rebuild against stretch-proposed-updates">
<correction debian-security-support "Update security support status of several packages">
<correction dehydrated "New upstream release; use ACMEv2 API by default">
<correction dispmua "New upstream release compatible with Thunderbird 68">
<correction dpdk "New upstream stable release; fix vhost regression introduced by the fix for CVE-2019-14818">
<correction fence-agents "Fix incomplete removal of fence_amt_ws">
<correction fig2dev "Allow Fig v2 text strings ending with multiple ^A [CVE-2019-19555]">
<correction flightcrew "Security fixes [CVE-2019-13032 CVE-2019-13241]">
<correction freetype "Correctly handle deltas in TrueType GX fonts, fixing rendering of variable hinted fonts in Chromium and Firefox">
<correction glib2.0 "Ensure libdbus clients can authenticate with a GDBusServer like the one in ibus">
<correction gnustep-base "Fix UDP amplification vulnerability">
<correction italc "Security fixes [CVE-2018-15126 CVE-2018-15127 CVE-2018-20019 CVE-2018-20020 CVE-2018-20021 CVE-2018-20022 CVE-2018-20023 CVE-2018-20024 CVE-2018-20748 CVE-2018-20749 CVE-2018-20750 CVE-2018-6307 CVE-2018-7225 CVE-2019-15681]">
<correction libdate-holidays-de-perl "Mark International Childrens Day (Sep 20th) as a holiday in Thuringia from 2019 onwards">
<correction libdatetime-timezone-perl "Update included data">
<correction libidn "Fix denial of service vulnerability in Punycode handling [CVE-2017-14062]">
<correction libjaxen-java "Fix build failure by allowing test failures">
<correction libofx "Fix NULL pointer dereference issue [CVE-2019-9656]">
<correction libole-storage-lite-perl "Fix interpretation of years from 2020 onwards">
<correction libparse-win32registry-perl "Fix interpretation of years from 2020 onwards">
<correction libperl4-corelibs-perl "Fix interpretation of years from 2020 onwards">
<correction libpst "Fix detection of get_current_dir_name and return truncation">
<correction libsixel "Fix several security issues [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libsolv "Fix heap buffer overflow [CVE-2019-20387]">
<correction libtest-mocktime-perl "Fix interpretation of years from 2020 onwards">
<correction libtimedate-perl "Fix interpretation of years from 2020 onwards">
<correction libvncserver "RFBserver: don't leak stack memory to the remote [CVE-2019-15681]; resolve a freeze during connection closure and a segmentation fault on multi-threaded VNC servers; fix issue connecting to VMWare servers; fix crashing of x11vnc when vncviewer connects">
<correction libxslt "Fix dangling pointer in xsltCopyText [CVE-2019-18197]">
<correction limnoria "Fix remote information disclosure and possibly remote code execution in the Math plugin [CVE-2019-19010]">
<correction linux "New upstream stable release">
<correction linux-latest "Update for Linux kernel ABI 4.9.0-12">
<correction llvm-toolchain-7 "Disable the gold linker from s390x; bootstrap with -fno-addrsig, stretch's binutils doesn't work with it on mips64el">
<correction mariadb-10.1 "New upstream stable release [CVE-2019-2974 CVE-2020-2574]">
<correction monit "Implement position independent CSRF cookie value">
<correction node-fstream "Clobber a Link if it's in the way of a File [CVE-2019-13173]">
<correction node-mixin-deep "Fix prototype polution [CVE-2018-3719 CVE-2019-10746]">
<correction nodejs-mozilla "New package to support Firefox ESR backports">
<correction nvidia-graphics-drivers-legacy-340xx "New upstream stable release">
<correction nyancat "Rebuild in a clean environment to add the systemd unit for nyancat-server">
<correction openjpeg2 "Fix heap overflow [CVE-2018-21010], integer overflow [CVE-2018-20847] and division by zero [CVE-2016-9112]">
<correction perl "Fix interpretation of years from 2020 onwards">
<correction php-horde "Fix stored cross-site scripting issue in Horde Cloud Block [CVE-2019-12095]">
<correction postfix "New upstream stable release; work around poor TCP loopback performance">
<correction postgresql-9.6 "New upstream release">
<correction proftpd-dfsg "Fix NULL pointer dereference in CRL checks [CVE-2019-19269]">
<correction pykaraoke "Fix path to fonts">
<correction python-acme "Switch to POST-as-GET protocol">
<correction python-cryptography "Fix test suite failures when built against newer OpenSSL versions">
<correction python-flask-rdf "Fix missing dependencies in python3-flask-rdf">
<correction python-pgmagick "Handle version detection of graphicsmagick security updates that identify themselves as version 1.4">
<correction python-werkzeug "Ensure Docker containers have unique debugger PINs [CVE-2019-14806]">
<correction ros-ros-comm "Fix buffer overflow issue [CVE-2019-13566]; fix integer overflow [CVE-2019-13445]">
<correction ruby-encryptor "Ignore test failures, fixing build failures">
<correction rust-cbindgen "New package to support Firefox ESR backports">
<correction rustc "New upstream version, to support Firefox ESR backports">
<correction safe-rm "Prevent installation in (and thereby breaking of) merged /usr environments">
<correction sorl-thumbnail "Workaround a pgmagick exception">
<correction sssd "sysdb: sanitize search filter input [CVE-2017-12173]">
<correction tigervnc "Security updates [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Security fixes [CVE-2014-6053 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-8287 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction tmpreaper "Add <q>--protect '/tmp/systemd-private*/*'</q> to cron job to prevent breaking systemd services that have PrivateTmp=true">
<correction tzdata "New upstream release">
<correction ublock-origin "New upstream version, compatible with Firefox ESR68">
<correction unhide "Fix stack exhaustion">
<correction x2goclient "Strip ~/, ~user{,/}, ${HOME}{,/} and $HOME{,/} from destination paths in scp mode; fixes regression with newer libssh versions with fixes for CVE-2019-14889 applied">
<correction xml-security-c "Fix <q>DSA verification crashes OpenSSL on invalid combinations of key content</q>">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den gamla stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2019 4474 firefox-esr>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4509 apache2>
<dsa 2019 4509 subversion>
<dsa 2019 4511 nghttp2>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4522 faad2>
<dsa 2019 4523 thunderbird>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4528 bird>
<dsa 2019 4529 php7.0>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux>
<dsa 2019 4532 spip>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4537 file-roller>
<dsa XXXX 4539 openssl>
<dsa 2019 4540 openssl1.0>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4548 openjdk-8>
<dsa XXXX 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4552 php7.0>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4557 libarchive>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4564 linux>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4571 thunderbird>
<dsa 2019 4573 symfony>
<dsa 2019 4574 redmine>
<dsa 2019 4576 php-imagick>
<dsa 2019 4578 libvpx>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4587 ruby2.3>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4594 openssl1.0>
<dsa 2019 4595 debian-lan-config>
<dsa 2019 4596 tomcat8>
<dsa XXXX 4596 tomcat-native>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4607 openconnect>
<dsa 2020 4609 python-apt>
<dsa XXXX 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4614 sudo>
<dsa 2020 4615 spamassassin>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction firetray "Incompatible with current Thunderbird versions">
<correction koji "Security issues">
<correction python-lamson "Broken by changes in python-daemon">
<correction radare2 "Security issues; upstream do not offer stable support">
<correction ruby-simple-form "Unused; security issues">
<correction trafficserver "Unsupportable">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
gamla stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella gamla stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Föreslagna uppdateringar till den gamla stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Information om den gamla stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>
