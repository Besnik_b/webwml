<define-tag pagetitle>Debian Edu / Skolelinux Bullseye — en komplett Linuxlösning för din skola</define-tag>
<define-tag release_date>2021-08-15</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="e7ce9859ec2794a8c0d0978182d2d513ed96cfa0" maintainer="Andreas Rönnquist"

<p>
Behöver du administrera datorlabb eller hela skolnätverk?
Skulle du vilja installera servrar, arbetsstationer och laptops som
sedan kommer att arbeta tillsammans?
Vill du ha Debians stabilitet med nätverkstjänster redan förkonfigurerade?
Vill du ha ett webbaserat verktyg för att hantera system och hundratals
eller till och med ännu fler användarkonton?
Har du frågat dig själv om och hur gamla datorer kan användas?
</p>

<p>
I sådana fall är Debian Edu för dig. Lärarna själva eller deras tekniska
support kan rulla ut en komplett multimaskinsstudiemiljö för flera användare på
bara några dagar. Debian Edu kommer med hundratals program förinstallerade,
men du kan alltid lägga till fler program från Debian.
</p>

<p>
Utvecklargruppen bakom Debian Edu tillkännager stolt Debian Edu 11
<q>Bullseye</q>, Debian Edu- / Skolelinuxutgåvan som baseras
på utgåvan Debian 11 <q>Bullseye</q>.
Vänligen testa den och rapportera till (&lt;debian-edu@lists.debian.org&gt;)
för att hjälpa oss att ytterligare förbättra den.
</p>


<h2>Om Debian Edu och Skolelinux</h2>

<p>
<a href="https://wiki.debian.org/DebianEdu"> Debian Edu, även känd som
Skolelinux</a>, är en Linuxdistribution baserad på Debian som tillhandahåller
en färdigkonstruerad miljö för ett fullständigt konfigurerat skolnätverk.
Direkt efter att du har installerat en skolserver som kör alla tjänster
som behövs för ett skolnätverk är det redo och väntar på användare och
maskiner som läggs till via GOsa², ett bekvämt webbgränssnitt. En
nätstartsmiljö förbereds, så efter den första installationen av
huvudservern från CD / DVD / BD eller USB-minne kan alla andra maskiner
installeras via nätverket.
Äldre datorer (till och med upp till runt 10 år gamla) kan användas som
LTSP - tunna klienter eller disklösa arbetsstationer, som startar från
nätverket utan någon installation eller några inställningar alls.
Skolservern Debian Edu tillhandahåller en LDAP-databas och
autentiseringstjänsten Kerberos, centraliserade hemkataloger, en DHCP-server,
en webbproxy och många andra tjänster. Skrivbordet innehåller mer än 60
pedagogiska mjukvarupaket och fler finns tillgängliga i Debianarkivet.
Skolor kan välja mellan skrivbordsmiljöerna Xfce, GNOME, LXDE, MATE,
KDE Plasma, Cinnamon och LXQt.
</p>

<h2>Nya funktioner för Debian Edu 11 <q>Bullseye</q></h2>

<p>Följande är några punkter från versionsfakta för Debian Edu 11 <q>Bullseye</q>,
baserad på utgåvan Debian 11 <q>Bullseye</q>.
Den fullständiga listan som inkluderar mer detaljerad information är en del av
det
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Features#New_features_in_Debian_Edu_Bullseye">relaterade kapitlet i Debian Edu-manualen</a>.
</p>

<ul>
<li>
Ny <a href="https://ltsp.org">LTSP</a> för att ge stöd till disklösa
arbetsstationer. Tunna klienter stöds fortfarande, nu med hjälp av
<a href="https://wiki.x2go.org">X2Go</a>-teknologi.
</li>
<li>
Uppstart via nätverket tillhandahålls med hjälp av iPXE istället PXELINUX för
att vara kompatibel med LTSP.
</li>
<li>
Debianinstallerarens grafiska läge används för iPXE-installationer.
</li>
<li>
Samba konfigureras nu som <q>fristående server</q> med stöd för SMB2/SMB3.
</li>
<li>
DuckDuckGo används som standardsökmotor för både Firefox ESR och Chromium.
</li>
<li>
Nytt verktyg tillagt för att sätta upp freeRADIUS med stöd för både
EAP-TTLS/PAP och PEAP-MSCHAPV2-metoder.
</li>
<li>
Förbättrat verktyg finns tillgängligt för att konfigurera ett nytt system med
<q>Minimal</q>-profilen som dedikerad gateway.
</li>
</ul>

<h2>Hämtningsalternativ, installationssteg samt manual</h2>

<p>
Separata CD-avbildningar för Nätverksinstallation för 64-bitars och
32-bitars PC finns tillgängliga. 32-bitarsavbildningen är endast nödvändig
i sällsynta fall (för PC som är äldre än runt 15 år). Avbildningarna kan
hämtas från följande platser:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Alternativt finns även officiella BD-avbildningar (större än 5 GB) tillgängliga.
Det är möjligt att sätta upp ett fullständigt Debian Edu-nätverk utan en
internetanslutning (alla skrivbordsmiljöer med stöd, alla språk som stöds av
Debian). Dessa avbildningar kan hämtas på följande platser:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
Avbildningarna kan verifieras med hjälp av signerade kontrollsummor som
tillhandahålls i hämtningsmappen.
<br />
När du har hämtat en avbildning kan du kontrollera att
<ul>
<li>
dess kontrollsumma matchar den förväntade från filen checksum; och att
</li>
<li>
filen checksum inte har manipulerats.
</li>
</ul>
För ytterligare information om hur man utför dessa steg, läs
<a href="https://www.debian.org/CD/verify">verifieringsguiden</a>.
</p>

<p>
Debian Edu 11 <q>Bullseye</q> baseras fullständigt på Debian 11 <q>Bullseye</q>;
så källkoden för alla paket finns tillgänglig från Debianarkivet.
</p>

<p>
Vänligen se
<a href="https://wiki.debian.org/DebianEdu/Status/Bullseye">statussidan för Debian Edu Bullseye</a>
för uppdaterad information om Debian Edu 11 <q>Bullseye</q> inklusive instruktioner
för hur du använder <code>rsync</code> för att hämta ISO-avbildningarna.
</p>

<p>
När du uppgraderar från Debian Edu 10 <q>Buster</q> var vänlig läs det
relaterade
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Upgrades">kapitlet i Debian Edu-manualen</a>.
</p>

<p>
För installationsanvisningar vänligen läs det relaterade
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Installation#Installing_Debian_Edu">kapitlet i Debian Edu-manualen</a>.
</p>

<p>
Efter installationen måste du ta dessa 
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/GettingStarted">första steg</a>.
</p>


<p>
Vänligen se <a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/">Debian Edus wikisidor</a>
för den senaste engelska versionen av Debian Edu <q>Bullseye</q>-manualen.
Manualen har kompletta översättningar till tyska, franska, italienska, danska, holländska,
norskt bokmål, japanska, förenklad kinesiska och portugisiska (portugal). Delvis
översatta versioner finns för spanska, rumänska och polska.
En översikt över <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
de senast översatta versionerna av manualen</a> finns tillgänglig.
</p>

<p>
Mer information om Debian 11 <q>Bullseye</q> självt kommer att tillhandahållas i
versionsfakta och installationsmanualen; se <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt fria
operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post till
&lt;press@debian.org&gt;.</p>
