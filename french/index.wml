#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="fe2d0a6290a91814fb8d336efc57e158b56b319e" maintainer="Jean-Pierre Giraud"

<div id="splash">
  <h1>Debian</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>La communauté</h1>
      <h2>Debian est une communauté de personnes !</h2>

#include "$(ENGLISHDIR)/index.inc"

      <div class="row">
      <div class="community column">
        <a href="intro/people" aria-hidden="true">
          <img src="Pics/users.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/people">Les acteurs</a></h2>
        <p>Qui sommes-nous et que faisons-nous</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/philosophy" aria-hidden="true">
          <img src="Pics/heartbeat.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/philosophy">Notre philosophie</a></h2>
        <p>Pourquoi et comment le faisons-nous</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/" aria-hidden="true">
          <img src="Pics/user-plus.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">S'impliquer et contribuer</a></h2>
        <p>Vous pouvez faire partie de la communauté !</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#community" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#community">Et plus encore...</a></h2>
        <p>Informations supplémentaires sur la communauté Debian</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>Le système d'exploitation</h1>
      <h2>Debian est un système d'exploitation libre complet !</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
        <a href="$(HOME)/download" class="os-dl-btn">Télécharger</a>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">Pourquoi Debian</a></h2>
        <p>Qu'est-ce qui rend Debian particulier et qui fait que vous devriez l'utiliser</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">Assistance</a></h2>
        <p>Obtenir de l'aide et la documentation</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">Mises à jour de sécurité</a></h2>
        <p>Annonces de sécurité de Debian (DSA)</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#software" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#software">Et plus encore...</a></h2>
        <p>Davantage de liens vers les téléchargements et les logiciels</p>
      </div>
    </div>
  </div>
</div>

<hr>
# <!-- An optional row highlighting events happening now, such as releases, point releases, debconf and minidebconfs, and elections (dpl, GRs...). -->
# <div class="row">
#  <div class="column styled-href-blue column-left">
#    <div style="text-align: center">
#      <h2><a href="https://debconf21.debconf.org/">DebConf21</a> est en cours!</h2>
#      <p>La Conférence Debian 2021 se tient en ligne du 24 août au 28 août.</p>
#    </div>
#  </div>
# </div>


<!-- The next row of columns on the site. -->
<!-- The News will be selected by the press team. -->


<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>Nouvelles et annonces de Debian</h2>
    </div>

    <:= get_top_news() :>
 
   <!-- No more News entries behind this line! -->
    <div class="project-news">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">Toutes les nouvelles</a> &emsp;&emsp;
        <a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Nouvelles de Debian" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Nouvelles du projet Debian" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Annonces de sécurité de Debian (titres uniquement)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Annonces de sécurité de Debian (résumés)" href="security/dsa-long">
:#rss#}

