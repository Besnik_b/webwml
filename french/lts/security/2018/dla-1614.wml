#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans openjpeg2, le codec
JPEG 2000 au source ouvert.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>

<p>Itération excessive dans la fonction opj_t1_encode_cblks (openjp2/t1.c).
Des attaquants distants pourraient exploiter cette vulnérabilité pour provoquer
un déni de service à l'aide d'un fichier bmp contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14423">CVE-2018-14423</a>

<p>Vulnérabilités de division par zéro dans les fonctions pi_next_pcrl,
pi_next_cprl et pi_next_rpcl dans (lib/openjp3d/pi.c). Des attaquants distants
pourraient exploiter cette vulnérabilité pour provoquer un déni de service
(plantage d'application).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 2.1.0-2+deb8u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjpeg2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1614.data"
# $Id: $
