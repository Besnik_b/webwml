#use wml::debian::translation-check translation="bc586bcf36854cdde56fe0c0ae4fb3e715ff2817" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans l'analyseur de trafic
réseau Wireshark.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22207">CVE-2021-22207</a>

<p>Consommation excessive de mémoire dans le dissecteur MS-WSP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22235">CVE-2021-22235</a>

<p>Plantage dans le dissecteur DNP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39921">CVE-2021-39921</a>

<p>Exception de pointeur NULL dans le dissecteur Modbus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39922">CVE-2021-39922</a>

<p>Dépassement de tampon dans le dissecteur C12.22.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39923">CVE-2021-39923</a>

<p>Grande boucle dans le dissecteur PNRP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39924">CVE-2021-39924</a>

<p>Grande boucle dans le dissecteur Bluetooth DHT.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39925">CVE-2021-39925</a>

<p>Dépassement de tampon dans le dissecteur Bluetooth SDP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39928">CVE-2021-39928</a>

<p>Exception de pointeur NULL dans le dissecteur IEEE 802.11.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39929">CVE-2021-39929</a>

<p>Récursion non contrôlée dans le dissecteur Bluetooth DHT.</p></li>

</ul>

<p>Pour Debian 9 Stretch, ces problèmes ont été corrigés dans la version
2.6.20-0+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wireshark,"
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wireshark">\
https://security-tracker.debian.org/tracker/wireshark</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2849.data"
# $Id: $
