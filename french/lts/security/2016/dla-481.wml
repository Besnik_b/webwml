#use wml::debian::translation-check translation="7e6b86e512d1e203a20619cdee7230be27c2eae5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour de sécurité corrige un certain nombre de problèmes de
sécurité dans phpMyAdmin. Nous vous recommandons de mettre à niveau vos
paquets phpmyadmin.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1927">CVE-2016-1927</a>

<p>La fonction suggestPassword génère des phrases secrètes faibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2038">CVE-2016-2038</a>

<p>Divulgation d'informations au moyen de requêtes contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2039">CVE-2016-2039</a>

<p>Valeur faible de jetons CSRF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2040">CVE-2016-2040</a>

<p>Vulnérabilités de script intersite (XSS) dans les utilisateurs
authentifiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2041">CVE-2016-2041</a>

<p>Brèche d'information dans la comparaison de jetons CSRF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2045">CVE-2016-2045</a>

<p>Injection de script intersite (XSS) au moyen de requêtes SQL
contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2560">CVE-2016-2560</a>

<p>Injection de script intersite (XSS).</p></li>

</ul>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4:3.4.11.1-2+deb7u3 de phpmyadmin </p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-481.data"
# $Id: $
