#use wml::debian::translation-check translation="e5ddd80a868f614ed40d5dfed75813f1cfa79825" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème de nettoyage de nom de fichier
dans <tt>php-pear</tt>, un système de publication pour des composants PHP
réutilisables.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28948">CVE-2020-28948</a>

<p>Archive_Tar jusqu’à la version 1.4.10 permet une attaque par désérialisation
car <q>phar:</q> est bloqué mais <q>PHAR:</q> ne l’est pas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28949">CVE-2020-28949</a>

<p>Archive_Tar jusqu’à la version 1.4.10 avait un nettoyage de nom de fichier
:// seulement pour contrer les attaques de phar, et par conséquent n’importe
quelle attaque par enveloppe de flux (telle que file:// pour écraser des
fichiers) peut encore réussir.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.10.1+submodules+notgz-9+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-pear.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2465.data"
# $Id: $
