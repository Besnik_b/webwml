#use wml::debian::translation-check translation="1adf122749849c18ccecc1b77a316565cd20f445" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans transfig, un convertisseur de
fichiers de graphismes d’XFig.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16140">CVE-2018-16140</a>

<p>Vulnérabilité d’écriture de tampon hors limite dans get_line() permettant
à un attaquant d’écrire avant le début du tampon à l'aide d'un fichier .fig
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14275">CVE-2019-14275</a>

<p>Dépassement de pile dans la fonction calc_arrow dans bound.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19555">CVE-2019-19555</a>

<p>Dépassement de pile dû à un sscanf incorrect.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:3.2.5.e-4+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets transfig.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2073.data"
# $Id: $
