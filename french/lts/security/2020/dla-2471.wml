#use wml::debian::translation-check translation="5e34db70bb9e758f6b8446f514ea1daa00d48b47" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert qu’XStream est vulnérable à une exécution de code
à distance. Cette vulnérabilité peut permettre à un attaquant distant d’exécuter
des commandes arbitraires d’interpréteur de commandes simplement en manipulant
le flux d’entrées traité. Les utilisateurs s’appuyant sur des listes noires sont
touchés (comportement par défaut dans Debian). Nous recommandons fortement
l’approche par listes blanches du cadriciel de sécurité d’XStream car il y a
vraisemblablement plus de combinaisons de classes auxquelles l’approche par listes
noires ne peut remédier.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.4.9-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxstream-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxstream-java, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxstream-java">https://security-tracker.debian.org/tracker/libxstream-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2471.data"
# $Id: $
