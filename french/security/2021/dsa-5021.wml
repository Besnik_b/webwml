#use wml::debian::translation-check translation="a12fa528db97a9eb77a097b657d4e437985b141a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans MediaWiki, un
moteur de site web pour travail collaboratif : des vulnérabilités dans les
actions mcrundo et rollback peuvent permettre à un attaquant de divulguer
des contenus de pages issus de wikis privés ou de contourner des
restrictions d'édition.</p>

<p>Pour davantage d'informations, veuillez consulter
<a href="https://www.mediawiki.org/wiki/2021-12_security_release/FAQ">\
https://www.mediawiki.org/wiki/2021-12_security_release/FAQ</a>.</p>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 1:1.31.16-1+deb10u2.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1:1.35.4-1+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mediawiki.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mediawiki, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mediawiki">\
https://security-tracker.debian.org/tracker/mediawiki</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5021.data"
# $Id: $
