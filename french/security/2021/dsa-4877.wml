#use wml::debian::translation-check translation="9b6e2575a6c497232661fc247cef61ab70393208" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27918">CVE-2020-27918</a>

<p>Liu Long a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29623">CVE-2020-29623</a>

<p>Simon Hunt a découvert que des utilisateurs peuvent se trouver dans
l'incapacité de supprimer complètement leur historique de navigation dans
certaines circonstances.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1765">CVE-2021-1765</a>

<p>Eliya Stein a découvert qu'un contenu web contrefait peut violer la
politique de bac à sable d'iframe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1789">CVE-2021-1789</a>

<p>@S0rryMybad a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1799">CVE-2021-1799</a>

<p>Gregory Vishnepolsky, Ben Seri et Samy Kamkar ont découvert qu'un site
web malveillant peut être capable d'accéder à des ports d'accès restreint
sur des serveurs arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1801">CVE-2021-1801</a>

<p>Eliya Stein a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1870">CVE-2021-1870</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web
contrefait pourrait conduire à l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.30.6-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4877.data"
# $Id: $
