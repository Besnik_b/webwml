#use wml::debian::translation-check translation="5c427e44dc4a1503d5262b5edfba60b490ea0ab1" maintainer="Med"

<define-tag pagetitle>تحديث دبيان 10 : الإصدار 10.7</define-tag>
<define-tag release_date>2020-12-05</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
يُسعد مشروع دبيان الإعلان عن التحديث السابع لتوزيعته المستقرة دبيان <release> (الاسم الرمزي <q><codename></q>).
بالإضافة إلى تسوية بعض المشكلات الحرجة يُصلح هذا التحديث بالأساس مشاكلات الأمان. تنبيهات الأمان أُعلنت بشكل منفصِل وفقط مُشار إليها في هذا الإعلان.
</p>

<p>
يُرجى ملاحظة أن هذا التحديث لا يُشكّل إصدار جديد لدبيان 10 بل فقط تحديثات لبعض الحُزم المُضمّنة
وبالتالي ليس بالضرورة رميُ الوسائط القديمة للإصدار <q><codename></q>، يمكن تحديث الحُزم باستخدام مرآة دبيان مُحدّثة.
</p>

<p>
الذين يُثبّتون التحديثات من security.debian.org باستمرار لن يكون عليهم تحديث العديد من الحُزم،
أغلب التحديثات مُضمّنة في هذا التحديث.
</p>

<p>
صور جديدة لأقراص التثبيت ستكون متوفرة في موضِعها المعتاد.
</p>

<p>
يمكن الترقية من  تثبيت آنِيّ إلى هذه المراجعة بتوجيه نظام إدارة الحُزم إلى إحدى مرايا HTTP الخاصة بدبيان.
قائمة شاملة لمرايا دبيان على المسار :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>إصلاح العديد من العِلاّت</h2>

<p>هذا التحديث للإصدار المستقر أضاف بعض الإصلاحات المهمة للحُزم التالية :</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السّبب</th></tr>
<correction base-files "Update for the point release">
<correction choose-mirror "Update mirror list">
<correction cups "Fix 'printer-alert' invalid free">
<correction dav4tbsync "New upstream release, compatible with newer Thunderbird versions">
<correction debian-installer "Use 4.19.0-13 Linux kernel ABI; add grub2 to Built-Using">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction distro-info-data "Add Ubuntu 21.04, Hirsute Hippo">
<correction dpdk "New upstream stable release; fix remote code execution issue [CVE-2020-14374], TOCTOU issues [CVE-2020-14375], buffer overflow [CVE-2020-14376], buffer over read [CVE-2020-14377] and integer underflow [CVE-2020-14377]; fix armhf build with NEON">
<correction eas4tbsync "New upstream release, compatible with newer Thunderbird versions">
<correction edk2 "Fix integer overflow in DxeImageVerificationHandler [CVE-2019-14562]">
<correction efivar "Add support for nvme-fabrics and nvme-subsystem devices; fix uninitialized variable in parse_acpi_root, avoiding possible segfault">
<correction enigmail "Introduce migration assistant to Thunderbird's built-in GPG support">
<correction espeak "Fix using espeak with mbrola-fr4 when mbrola-fr1 is not installed">
<correction fastd "Fix memory leak when receiving too many invalid packets [CVE-2020-27638]">
<correction fish "Ensure TTY options are restored on exit">
<correction freecol "Fix XML External Entity vulnerability [CVE-2018-1000825]">
<correction gajim-omemo "Use 12-byte IV, for better compatibility with iOS clients">
<correction glances "Listen only on localhost by default">
<correction iptables-persistent "Don't force-load kernel modules; improve rule flushing logic">
<correction lacme "Use upstream certificate chain instead of an hardcoded one, easing support for new Let's Encrypt root and intermediate certificates">
<correction libdatetime-timezone-perl "Update included data to tzdata 2020d">
<correction libimobiledevice "Add partial support for iOS 14">
<correction libjpeg-turbo "Fix denial of service [CVE-2018-1152], buffer over read [CVE-2018-14498], possible remote code execution [CVE-2019-2201], buffer over read [CVE-2020-13790]">
<correction libxml2 "Fix denial of service [CVE-2017-18258], NULL pointer dereference [CVE-2018-14404], infinite loop [CVE-2018-14567], memory leak [CVE-2019-19956 CVE-2019-20388], infinite loop [CVE-2020-7595]">
<correction linux "New upstream stable release">
<correction linux-latest "Update for 4.19.0-13 kernel ABI">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction lmod "Change architecture to <q>any</q> - required due to LUA_PATH and LUA_CPATH being determined at build time">
<correction mariadb-10.3 "New upstream stable release; security fixes [CVE-2020-14765 CVE-2020-14776 CVE-2020-14789 CVE-2020-14812 CVE-2020-28912]">
<correction mutt "Ensure IMAP connection is closed after a connection error [CVE-2020-28896]">
<correction neomutt "Ensure IMAP connection is closed after a connection error [CVE-2020-28896]">
<correction node-object-path "Fix prototype pollution in set() [CVE-2020-15256]">
<correction node-pathval "Fix prototype pollution [CVE-2020-7751]">
<correction okular "Fix code execution via action link [CVE-2020-9359]">
<correction openjdk-11 "New upstream release; fix JVM crash">
<correction partman-auto "Increase /boot sizes in most recipes to between 512 and 768M, to better handle kernel ABI changes and larger initramfses; cap RAM size as used for swap partition calculations, resolving issues on machines with more RAM than disk space">
<correction pcaudiolib "Cap cancellation latency to 10ms">
<correction plinth "Apache: Disable mod_status [CVE-2020-25073]">
<correction puma "Fix HTTP injection and HTTP smuggling issues [CVE-2020-5247 CVE-2020-5249 CVE-2020-11076 CVE-2020-11077]">
<correction ros-ros-comm "Fix integer overflow [CVE-2020-16124]">
<correction ruby2.5 "Fix potential HTTP request smuggling vulnerability in WEBrick [CVE-2020-25613]">
<correction sleuthkit "Fix stack buffer overflow in yaffsfs_istat [CVE-2020-10232]">
<correction sqlite3 "Fix division by zero [CVE-2019-16168], NULL pointer dereference [CVE-2019-19923], mishandling of NULL pathname during an update of a ZIP archive [CVE-2019-19925], mishandling of embedded NULs in filenames [CVE-2019-19959], possible crash (unwinding WITH stack) [CVE-2019-20218], integer overflow [CVE-2020-13434], segmentation fault [CVE-2020-13435], use-after-free issue [CVE-2020-13630], NULL pointer dereference [CVE-2020-13632], heap overflow [CVE-2020-15358]">
<correction systemd "Basic/cap-list: parse/print numerical capabilities; recognise new capabilities from Linux kernel 5.8; networkd: do not generate MAC for bridge device">
<correction tbsync "New upstream release, compatible with newer Thunderbird versions">
<correction tcpdump "Fix untrusted input issue in the PPP printer [CVE-2020-8037]">
<correction tigervnc "Properly store certificate exceptions in native and java VNC viewer [CVE-2020-26117]">
<correction tor "New upstream stable release; multiple security, usability, portability, and reliability fixes">
<correction transmission "Fix memory leak">
<correction tzdata "New upstream release">
<correction ublock-origin "New upstream version; split plugin to browser-specific packages">
<correction vips "Fix use of uninitialised variable [CVE-2020-20739]">
</table>


<h2>تحديثات الأمان</h2>


<p>
أضافت هذه المراجعة تحديثات الأمان التالية للإصدار المستقر.
سبَق لفريق الأمان نشر تنبيه لكل تحديث :
</p>

<table border=0>
<tr><th>مُعرَّف التنبيه</th>  <th>الحزمة</th></tr>
<dsa 2020 4766 rails>
<dsa 2020 4767 mediawiki>
<dsa 2020 4768 firefox-esr>
<dsa 2020 4769 xen>
<dsa 2020 4770 thunderbird>
<dsa 2020 4771 spice>
<dsa 2020 4772 httpcomponents-client>
<dsa 2020 4773 yaws>
<dsa 2020 4774 linux-latest>
<dsa 2020 4774 linux-signed-amd64>
<dsa 2020 4774 linux-signed-arm64>
<dsa 2020 4774 linux-signed-i386>
<dsa 2020 4774 linux>
<dsa 2020 4775 python-flask-cors>
<dsa 2020 4776 mariadb-10.3>
<dsa 2020 4777 freetype>
<dsa 2020 4778 firefox-esr>
<dsa 2020 4779 openjdk-11>
<dsa 2020 4780 thunderbird>
<dsa 2020 4781 blueman>
<dsa 2020 4782 openldap>
<dsa 2020 4783 sddm>
<dsa 2020 4784 wordpress>
<dsa 2020 4785 raptor2>
<dsa 2020 4786 libexif>
<dsa 2020 4787 moin>
<dsa 2020 4788 firefox-esr>
<dsa 2020 4789 codemirror-js>
<dsa 2020 4790 thunderbird>
<dsa 2020 4791 pacemaker>
<dsa 2020 4792 openldap>
<dsa 2020 4793 firefox-esr>
<dsa 2020 4794 mupdf>
<dsa 2020 4795 krb5>
<dsa 2020 4796 thunderbird>
<dsa 2020 4798 spip>
<dsa 2020 4799 x11vnc>
<dsa 2020 4800 libproxy>
</table>


<h2>الحُزم المزالة</h2>

<p>
الحُزم التالية أزيلت لأسباب خارجة عن سيطرتنا :
</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السّبب</th></tr>
<correction freshplayerplugin "Unsupported by browsers; discontinued upstream">
<correction nostalgy "Incompatible with newer Thunderbird versions">
<correction sieve-extension "Incompatible with newer Thunderbird versions">

</table>

<h2>مُثبِّت دبيان</h2>
<p>
حُدِّث المُثبِّت ليتضمن الإصلاحات المندرجة في هذا الإصدار المستقر.
</p>

<h2>المسارات</h2>

<p>
القائمة الكاملة للحُزم المُغيّرة في هذه المراجعة :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>التوزيعة المستقرة الحالية :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>التحديثات المقترحة للتوزيعة المستقرة :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>معلومات حول التوزيعة المستقرة (ملاحظات الإصدار والأخطاء إلخ) :</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>معلومات وإعلانات الأمان :</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>حول دبيان</h2>

<p>
مشروع دبيان هو اتحاد لمطوري البرمجيات الحُرّة تطوعوا بالوقت والمجهود لإنتاج نظام تشعيل دبيان حُر بالكامل.
</p>

<h2>معلومات الاتصال</h2>

<p>
لمزيد من المعلومات يُرجى زيارة موقع دبيان
<a href="$(HOME)/">https://www.debian.org/</a>
أو الاتصال بفريق إصدار المستقرة على
&lt;debian-release@lists.debian.org&gt;.</p>
