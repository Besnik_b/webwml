#use wml::debian::translation-check translation="1e5f62f34bcc8da30f41325ca8cb664f8713412c"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrió que la lista negra por omisión de XStream, una biblioteca Java
para serialización de objetos a XML y su reconstrucción, era vulnerable a
ejecución de órdenes arbitrarias del intérprete de órdenes («shell») mediante la manipulación del flujo de
datos de entrada («input stream») procesado.</p>

<p>Como una defensa en profundidad adicional, se recomienda cambiar a la
estrategia de lista blanca que proporciona la infraestructura de soporte de seguridad de XStream. Para información
adicional, consulte
<a href="https://github.com/x-stream/xstream/security/advisories/GHSA-mw36-7c6c-q4q2">\
https://github.com/x-stream/xstream/security/advisories/GHSA-mw36-7c6c-q4q2</a></p>

<p>Para la distribución «estable» (buster), este problema se ha corregido en
la versión 1.4.11.1-1+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de libxstream-java.</p>

<p>Para información detallada sobre el estado de seguridad de libxstream-java, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4811.data"
