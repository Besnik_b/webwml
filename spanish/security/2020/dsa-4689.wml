#use wml::debian::translation-check translation="139ced0522f792565594fd4bc65bf27ae29bd20d"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varias vulnerabilidades en BIND, una implementación
de servidor DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6477">CVE-2019-6477</a>

    <p>Se descubrió que las consultas a través de una conexión TCP con segmentación («pipelining») habilitada podían eludir los límites
    del cliente TCP, dando lugar a denegación de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8616">CVE-2020-8616</a>

    <p>Se descubrió que BIND no limita suficientemente el número
    de descargas realizadas al procesar derivaciones («referrals»). Un atacante puede
    aprovechar este defecto para provocar denegación de servicio (degradación de
    rendimiento) o usar el servidor recursivo en un ataque de reflejo con
    un alto factor de amplificación.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8617">CVE-2020-8617</a>

    <p>Se descubrió que se puede usar un error en la lógica del código que verifica
    la validez TSIG para desencadenar un fallo de aserción, dando lugar a
    denegación de servicio.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 1:9.10.3.dfsg.P4-12.3+deb9u6.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1:9.11.5.P4+dfsg-5.1+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de bind9.</p>

<p>Para información detallada sobre el estado de seguridad de bind9, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4689.data"
