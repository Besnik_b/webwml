#use wml::debian::translation-check translation="9b6e2575a6c497232661fc247cef61ab70393208"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto las siguientes vulnerabilidades en el motor web
webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27918">CVE-2020-27918</a>

    <p>Liu Long descubrió que el procesado de contenido web preparado
    maliciosamente puede dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29623">CVE-2020-29623</a>

    <p>Simon Hunt descubrió que los usuarios pueden no ser capaces de borrar completamente
    su historial de navegación en determinadas circunstancias.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1765">CVE-2021-1765</a>

    <p>Eliya Stein descubrió que contenido web preparado maliciosamente puede
    violar la política de entorno aislado para iframes («iframe sandboxing policy»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1789">CVE-2021-1789</a>

    <p>@S0rryMybad descubrió que el procesado de contenido web preparado
    maliciosamente puede dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1799">CVE-2021-1799</a>

    <p>Gregory Vishnepolsky, Ben Seri y Samy Kamkar descubrieron que un
    sitio web malicioso podría acceder a puertos restringidos de
    servidores arbitrarios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1801">CVE-2021-1801</a>

    <p>Eliya Stein descubrió que el procesado de contenido web preparado
    maliciosamente puede dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1870">CVE-2021-1870</a>

    <p>Un investigador anónimo descubrió que el procesado de contenido
    web preparado maliciosamente puede dar lugar a ejecución de código arbitrario.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.30.6-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de webkit2gtk.</p>

<p>Para información detallada sobre el estado de seguridad de webkit2gtk, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4877.data"
