#use wml::debian::template title="Sitio web de Debian en diferentes idiomas" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="80696988195221adfe32e0c037530145acd35b48"

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">Navegación de contenidos</a></li>
    <li><a href="#howtoset">Cómo establecer la configuración de idioma de un navegador web</a></li>
    <li><a href="#override">Cómo hacer que se ignore la configuración</a></li>
    <li><a href="#fix">Resolución de problemas</a></li>
  </ul>
</div>

<h2><a id="intro">Navegación de contenidos</a></h2>

<p>
Un equipo de <a href="../devel/website/translating">traductores</a>
trabaja para traducir el sitio web de Debian a un número
creciente de idiomas. Pero ¿cómo funciona la selección del
idioma en el navegador web? Un estándar denominado
<a href="$(HOME)/devel/website/content_negotiation">negociación de contenido</a>
permite a los usuarios y usuarias configurar su(s) idioma(s) preferido(s) para el contenido web. La
versión que ven es el resultado de una negociación entre el navegador y el servidor web:
el navegador envía las preferencias al servidor y, a continuación, el servidor
decide qué versión enviar en base a las preferencias del usuario y
a las versiones disponibles.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">Lea más en W3C</a></button></p>

<p>
No todo el mundo conoce la negociación de contenido, por lo que hay enlaces en la parte inferior
de las páginas de Debian que apuntan a las versiones disponibles. Tenga en cuenta
que seleccionar un idioma distinto en esa lista solo afecta
a la página actual. No cambia el idioma por omisión de su navegador
web. Si sigue un enlace a otra página, la verá
de nuevo en el idioma por omisión.
</p>

<p>
Para cambiar el idioma por omisión tiene dos opciones:
</p>

<ul>
<li><a href="#howtoset">Configure el navegador web</a></li>
<li><a href="#override">Haga que se ignoren las preferencias de idioma del navegador</a></li>
</ul>

<p>
Vaya directamente a las instrucciones de configuración para estos navegadores:</p>

<toc-display />

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> El idioma original del sitio web de Debian es el inglés. Por lo tanto, es una buena idea añadir el inglés (<code>en</code>) al final de la lista de idiomas para el caso de que una página todavía no esté traducida a ninguno de sus idiomas preferidos.</p>
</aside>

<h2><a name="howtoset">Cómo establecer la configuración de idioma de un navegador web</a></h2>

<p>
Antes de describir cómo configurar el idioma en distintos
navegadores web, algunas consideraciones generales. En primer lugar, es una buena idea incluir
todos los idiomas que habla en la lista de idiomas preferidos. Por ejemplo,
si es hablante nativo o nativa de español, puede elegir <code>es</code> como el
primer idioma, seguido del inglés con el código de idioma <code>en</code>.
</p>

<p>
En segundo lugar, en algunos navegadores puede introducir códigos de idioma en lugar de
seleccionarlos en un menú. Si este es su caso, tenga en cuenta que crear
una lista como <code>es, en</code> no define sus preferencias. Por el contrario,
define opciones indiferentes y el servidor web puede decidir
ignorar el orden y limitarse a elegir uno cualquiera de los idiomas. Si quiere
especificar realmente una preferencia, tiene que trabajar con los llamados «quality values»,
que son números reales entre 0 y 1 que representan prioridades: un valor más alto indica una
mayor prioridad. Volviendo al ejemplo con el español y el inglés,
puede modificar la lista dejándola así:
</p>

<pre>
es; q=1.0, en; q=0.5
</pre>

<h3>Tenga cuidado con los códigos de países</h3>

<p>
Un servidor web que reciba la solicitud de un documento con los idiomas
preferidos <code>en-GB, es</code> <strong>no siempre</strong> servirá la
versión inglesa antes que la española. Solo lo hará si existe una
página con la extensión de idioma <code>en-gb</code>. Sin embargo, a la
inversa sí funciona: un servidor puede devolver una página <code>en-us</code> si
solo se incluye <code>en</code> en la lista de idiomas preferidos.
</p>

<p>
Por lo tanto, recomendamos no añadir códigos de país de dos letras como
<code>en-GB</code> o <code>en-US</code> a no ser que tenga una buena razón para hacerlo. Si
lo hace, asegúrese de incluir también el código de idioma sin
la extensión: <code>en-GB, en, es</code>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://httpd.apache.org/docs/current/content-negotiation.html">Más sobre negociación de contenido</a></button></p>

<h3>Instrucciones para distintos navegadores web</h3>

<p>
Hemos compilado una lista de navegadores web populares y algunas instrucciones sobre cómo
cambiar en su configuración el idioma preferido para el contenido web:
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
  En la parte superior derecha, abra el menú y pulse en <em>Configuración</em> -&gt; <em>Mostrar opciones avanzadas</em> -&gt; <em>Idiomas</em>. Abra el menú <em>Idiomas</em> para ver una lista de idiomas. Haga click en los tres puntos junto a cada entrada para cambiar el orden. También puede añadir más idiomas si es necesario.</li>
        <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
  Establecer el idioma por omisión a través de <em>Configuración</em> -&gt; <em>Idioma</em> también cambia el idioma solicitado a los sitios web. Puede cambiar este comportamiento y afinar la <em>cabecera Accept-Language</em> en <em>Configuración</em> -&gt; <em>Gestor de opciones</em> -&gt; <em>Protocolos</em> -&gt; <em>HTTP</em>.</li>
        <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
  Abra <em>Preferencias</em> en el menú principal y cambie a la pestaña <em>Idioma</em>. Aquí puede añadir, quitar y ordenar los idiomas.</li>
        <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
  Despliegue el menú de la barra superior y abra <em>Preferencias</em>. En el panel <em>General</em>, baje hasta <em>Idioma y apariencia</em> -&gt; <em>Idioma</em>. Haga click en el botón <em>Seleccionar...</em> para establecer su idioma preferido para visualizar sitios web. En el diálogo mostrado puede añadir, eliminar y ordenar los idiomas.</li>
        <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
  Vaya a <em>Preferencias</em> -&gt; <em>Configuración</em> -&gt; <em>Red</em>. <em>Accept language</em> probablemente mostrará un *, que es el valor por omisión. Si hace click en el botón <em>Locale</em>, debería poder añadir su idioma preferido. En caso contrario, lo puede introducir manualmente.</li>
        <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br> (versión en inglés)
  <em>Edit</em> -&gt; <em>Preferences</em> -&gt; <em>Browser</em> -&gt; <em>Fonts, Languages</em></li>
        <li><strong><toc-add-entry name="iceweasel">IceCat (Iceweasel)</toc-add-entry></strong><br>
  <em>Editar</em> -&gt; <em>Preferencias</em> -&gt; <em>Contenido</em> -&gt; <em>Idiomas</em> -&gt; <em>Seleccionar...</em></li>
        <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
  Haga click en el icono <em>Herramientas</em>, seleccione <em>Opciones de Internet</em>, cambie a la pestaña <em>General</em> y haga click en el botón <em>Idiomas</em>. Pulse <em>Establecer preferencias de Idioma</em> y, en el diálogo mostrado, podrá añadir, eliminar y ordenar los idiomas.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
        Edite el fichero <em>~/.kde/share/config/kio_httprc</em> e incluya una línea como la siguiente:<br>
        <code>Languages=es;q=1.0, en;q=0.5</code></li>
        <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br> (versión en inglés)
        Edite el fichero <em>~/.lynxrc</em> e incluya una línea como la siguiente:<br>
        <code>preferred_language=es; q=1.0, en; q=0.5</code><br>
        Alternativamente, puede abrir la configuración del navegador pulsando [O]. Baje hasta <em>Preferred language</em> y añada <code>es; q=1.0, en; q=0.5</code>.</li>
        <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
        <em>Configuración y más</em>  -&gt; <em>Configuración</em> -&gt; <em>Idiomas</em> -&gt; <em>Agregar idiomas</em><br>
        Haga click en el botón con tres puntos junto a un idioma para más opciones y para cambiar el orden.</li>
        <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br> (versión en inglés)
        <em>Settings</em> -&gt; <em>Browser</em> -&gt; <em>Languages</em> -&gt; <em>Preferred languages</em></li>
        <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
        Safari usa la configuración del sistema de macOS y de iOS. Para definir su idioma preferido, abra <em>Preferencias del Sistema</em> (macOS) o <em>Configuración</em> (iOS).</li>
        <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br> (versión en inglés)
        Pulse [O] para abrir el <em>Option Setting Panel</em>, baje hasta <em>Network Settings</em> -&gt; <em>Accept-Language header</em>. Pulse [Intro] para cambiar la configuración (por ejemplo <code>es; q=1.0, en; q=0.5</code>) y confirme el cambio con [Intro]. Baje hasta abajo y pulse [OK] para guardar la configuración.</li>
        <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br> (versión en inglés)
        Vaya a <em>Settings</em> -&gt; <em>General</em> -&gt; <em>Language</em> -&gt; <em>Accepted Languages</em>, haga click en <em>Add Language</em> y escoja uno del menú. Use las flechas para cambiar el orden de preferencia.</li>
      </ul>
    </div>
  </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Aunque siempre es mejor establecer sus preferencias de idioma en la configuración del navegador, existe la opción de hacer que se ignoren estas preferencias con una cookie.</p>
</aside>

<h2><a name="override">Cómo hacer que se ignore la configuración</a></h2>

<p>
Si por alguna razón no puede establecer su idioma preferido en
el navegador, dispositivo o entorno de computación, puede hacer que se ignoren
las preferencias por medio de una cookie como último recurso. Pulse uno de los botones mostrados más abajo para poner
un idioma el primero de la lista.
</p>

<p>
Tenga en cuenta que esto establecerá una <a
href="https://en.wikipedia.org/wiki/HTTP_cookie">cookie</a>. El
navegador borrará la cookie si no visita este sitio web
en el plazo de un mes. También puede borrar la cookie en cualquier momento
eligiendo la opción <em>Browser default</em> («Predeterminado del navegador»).
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>

<h2><a id="fix">Resolución de problemas</a></h2>

<p>
En ocasiones, el sitio web de Debian se muestra en el idioma equivocado a pesar
de todos los esfuerzos por configurar un idioma preferido. Nuestra primera sugerencia es
limpiar la caché local (tanto en disco como en memoria) del navegador
y, a continuación, intentar recargar la página web. Si tiene la seguridad de que
ha <a href="#howtoset">configurado el navegador</a> correctamente, el
problema podría ser una caché que se comporte mal o que esté mal configurada. Este se está convirtiendo
en un problema serio hoy en día, ya que cada vez más proveedores de servicios de Internet ven en el uso de cachés una manera
de reducir su tráfico de red. Lea la <a href="#cache">sección</a>
sobre servidores proxy aunque piense que no está usando ninguno.
</p>

<p>
Por supuesto, siempre es posible que haya algún problema en <a
href="https://www.debian.org/">www.debian.org</a>. Aunque solo un
puñado de los problemas de idioma reportados en los últimos años estaban causados
por un fallo por nuestra parte, es algo totalmente posible. Por lo tanto, le sugerimos
que investigue su configuración y un potencial problema de caché
primero, antes de <a href="../contact">ponerse en contacto</a> con nosotros. Si <a
href="https://www.debian.org/">https://www.debian.org/</a> funciona
pero una de las <a href="https://www.debian.org/mirror/list">réplicas</a>
no, repórtelo para que nos pongamos en contacto con los responsables de la réplica.
</p>

<h3><a name="cache">Problemas potenciales con servidores proxy</a></h3>

<p>
Esencialmente, un servidor proxy es un servidor web que no tiene contenido
propio. Se encuentra en el medio entre los usuarios y los servidores web reales. El proxy toma
las peticiones de páginas web y obtiene las páginas. A continuación, envía
el contenido a los navegadores web de los usuarios, pero también hace una copia local,
que mantiene en la caché para servir peticiones posteriores. Esto puede disminuir
el tráfico de la red cuando muchos usuarios piden la misma página.
</p>

<p>
Si bien esto puede ser una buena idea la mayoría de las veces, también causa problemas
cuando la caché tiene fallos. En particular, algunos servidores proxy antiguos no
entienden la negociación de contenido. Esto tiene como consecuencia que guarden en la caché una página en
un idioma y la sirvan en lo sucesivo, aunque más tarde se solicite en un idioma
diferente. La única solución es actualizar o sustituir el software de la caché.
</p>

<p>
Históricamente, los servidores proxy solo se usaban cuando se configuraba el
navegador a tal efecto. Sin embargo, este ya no es el caso. Su proveedor de servicio de Internet (ISP, por sus siglas en inglés)
puede estar redirigiendo todas las peticiones HTTP a través de un proxy transparente. Si
el proxy no gestiona correctamente la negociación de contenido, los usuarios pueden
recibir páginas procedentes de la caché en el idioma equivocado. La única manera de corregir
esto es quejándose al ISP de manera que actualice o cambie
su software.
</p>
