#use wml::debian::template title="Debian 10 -- Fejl" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="3d34024ec1ac001614d2682a12ceb687bfb5f2ae"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>

# <toc-add-entry name="known_probs">Kendte problemer</toc-add-entry>
<toc-add-entry name="security">Sikkerhedsproblemer</toc-add-entry>

<p>Debian Security-holdet udsender opdateringer til pakker i den stabile udgave, 
hvor der er registreret sikkerhedsrelaterede problemer.  Besøg 
<a href="$(HOME)/security/">sikkerhedssiderne</a> for oplysninger om alle 
sikkerhedsproblemer registreret i <q>buster</q>.</p>

<p>Hvis du anvender APT, kan følende linje føjes til 
<tt>/etc/apt/sources.list</tt>, for at kunne tilgå de seneste 
sikkerhedsopdateringer:</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>Derefter køres <kbd>apt update</kbd> efterfulgt af 
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Punktudgivelser</toc-add-entry>

<p>Nogle gange, i tilfælde af kritiske problemer eller sikkerhedsopdateringer, 
opdateres den udgivne distribution.  Generelt kaldes disse for punktudgivelser)
(eller <q>point releases</q> på engelsk).</p>

<ul>
  <li>Den første punktudgivelse, 10.1, blev udgivet den
      <a href="$(HOME)/News/2019/20190907">7. september 2019</a>.</li>
  <li>Den anden punktopdatering, 10.2, blev udgivet den
      <a href="$(HOME)/News/2019/20191116">16. november 2019</a>.</li>
  <li>Den tredje punktopdatering, 10.3, blev udgivet den
      <a href="$(HOME)/News/2020/20200208">8. februar 2020</a>.</li>
  <li>Den fjerde punktopdatering, 10.4, blev udgivet den
      <a href="$(HOME)/News/2020/20200509">9. maj 2020</a>.</li>
  <li>Den femte punktopdatering, 10.5, blev udgivet den
      <a href="$(HOME)/News/2020/20200801">1. august 2020</a>.</li>
  <li>Den sjette punktopdatering, 10.6, blev udgivet den
      <a href="$(HOME)/News/2020/20200926">26. september 2020</a>.</li>
  <li>Den syvende punktopdatering, 10.7, blev udgivet den
      <a href="$(HOME)/News/2020/20201205">5. december 2020</a>.</li>
  <li>Den ottende punktopdatering, 10.8, blev udgivet den
      <a href="$(HOME)/News/2021/20210206">6. februar 2021</a>.</li>
  <li>Den niende punktopdatering, 10.9, blev udgivet den
      <a href="$(HOME)/News/2021/20210327">27. marts 2021</a>.</li>
  <li>Den tiende punktopdatering, 10.10, blev udgivet den
      <a href="$(HOME)/News/2021/20210619">19. juni 2021</a>.</li>
  <li>Den ellevete punktopdatering, 10.11, blev udgivet den
      <a href="$(HOME)/News/2021/2021100902">9. oktober 2021</a>.</li>
</ul>

<ifeq <current_release_stretch> 10.0 "

<p>Der er endnu ingen punktudgivelser til Debian 10.</p>" "

<p>Se 
<a href="http://http.us.debian.org/debian/dists/buster/ChangeLog">\
ChangeLog</a> for detaljerede ændringer mellem 10.0 og 
<current_release_buster/>.</p>"/>

<p>Rettelser til den udgivne stabile distribution gennemgår ofte en udvidet 
testperiode, før de accepteret i arkivet.  Dog er disse ændringer tilgængelige 
i mappen 
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> på alle Debians arkivfilspejle.</p>

<p>Hvis du anvender APT til at opdatere dine pakker, kan du installere de 
foreslåede opdateringer, ved at føje følgende linje til 
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 10 point release
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>Derefter køres <kbd>apt update</kbd> efterfulgt af 
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installeringssystem</toc-add-entry>

<p>For oplysninger om fejl og opdateringer til installeringssystemet, se 
siden med <a href="debian-installer/">installeringsoplysninger</a>.</p>
