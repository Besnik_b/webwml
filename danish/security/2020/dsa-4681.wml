#use wml::debian::translation-check translation="9366447bdb2abdccc3962d87981f5cce43bf471c" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Følgende sårbarheder er opdaget i webmotoren webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3885">CVE-2020-3885</a>

    <p>Ryan Pickren opdagede at en fil-URL kunne blive behandlet på ukorrekt 
    vis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3894">CVE-2020-3894</a>

    <p>Sergei Glazunov opdagede at en kapløbstilstand kunne gøre det muligt for 
    en applikation at læse hukommelse, som der er begrænset adgang til.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3895">CVE-2020-3895</a>

    <p>grigoritchy opdagede at behandling af ondsindet fremstillet webindhold 
    kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3897">CVE-2020-3897</a>

    <p>Brendan Draper opdagede at en fjernangriber kunne være i stand til at 
    forårsage udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3899">CVE-2020-3899</a>

    <p>OSS-Fuzz opdagede at en fjernangriber kunne være i stand til at 
    forårsage udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3900">CVE-2020-3900</a>

    <p>Dongzhuo Zhao opdagede at behandling af ondsindet fremstillet 
    webhindhold kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3901">CVE-2020-3901</a>

    <p>Benjamin Randazzo opdagede at behandling af ondsindet fremstillet 
    webindhold kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3902">CVE-2020-3902</a>

    <p>Yigit Can Yilmaz opdagede at behandling af ondsindet fremstillet 
    webindhold kunne føre til et skriptangreb på tværs af servere.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.28.2-2~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine webkit2gtk-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende webkit2gtk, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4681.data"
