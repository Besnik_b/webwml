#use wml::debian::template title="예시"
#use wml::debian::translation-check translation="ceca0da4950426a38fa87b51efcfd25749d8995a" maintainer="Sebul"

<h3>번역 시작하기 예시</h3>

<p>한국어를 예시로 쓸 겁니다:

<pre>
   git pull
   cd webwml
   mkdir korean
   cd korean
   cp ../english/.wmlrc ../english/Make.* .
   echo '<protect>include $(subst webwml/french,webwml/english,$(CURDIR))/Makefile</protect>' &gt; Makefile
   mkdir po
   git add Make* .wmlrc
   cp Makefile po
   make -C po init-po
   git add po/Makefile po/*.ko.po
</pre>

<p><tt>.wmlrc</tt> 파일을 편집해서 아래와 같이 바꿈:
<ul>
  <li>'-D CUR_LANG=English'를 '-D CUR_LANG=Korean'으로
  <li>'-D CUR_ISO_LANG=en'를 '-D CUR_ISO_LANG=ko'로
  <li>'-D CUR_LOCALE=en_US'를 '-D CUR_LOCALE=ko_KR.UTF-8'로
  <li>'-D CHARSET=utf-8'로.<br>
새 언어는 이 설정을 조정할 필요가 있을 수 있습니다.
</ul>

<p>Make.lang을 편집하여 'LANGUAGE := en'을 'LANGUAGE := ko'로 바꿉니다.
다중 바이트 문자집합을 쓰는 언어로 번역하는 경우,
파일에서 다른 변수를 바꿔야 할 수 있으며, 자세한 정보는 ../Makefile.common 과
다른 작업 예(중국어 같은 번역)를 읽어보세요.

<p>korean/po로 가서 PO 파일 안의 항목을 번역하세요. 이건 아주 간단할 겁니다.

<p>언제나 여러분이 번역하는 각 디렉터리에 Makefile을 복사하세요.
이것은 꼭 필요한데 왜냐면 <code>make</code> 프로그램이 .wml 파일을 HTML로 변환하고,
<code>make</code>가 Makefile을 사용하기 때문입니다.

<p>여러분이 페이지를 추가하고 편집하는 것을 다 하면,

<pre>
   git commit -m "여기에 커밋 메시지"
   git push
</pre>

위 명령을 webwml 디렉터리에서 합니다. 이제 페이지 번역을 시작할 수 있습니다.

<h3>한 페이지 번역하기</h3>

<p>social contract의 번역이 이 예시로 쓰일 겁니다:

<pre>
   cd webwml
   ./copypage.pl english/social_contract.wml
   cd korean
</pre>

<p>이것은 자동으로 translation-check 헤더(복사된 원본의 버전을 가리킴)를 추가합니다.
그것은 대상 디렉터리와 Makefile을 만듭니다(빠진 경우).</p>

<p>social_contract.wml을 편집하고 텍스트를 번역하세요.
링크를 번역하거나 바꾸려고 하지 마세요 - 무언가 바꾸어야 한다면, 
debian-www에서 요청하세요. 다 되면, 아래를 치세요.

### 데비안 웹 git의 커밋 메시지는 영어로 해야 함. 아래 메시지 번역하지 말 것.
<pre>
   git add social_contract.wml
   git commit -m "Translated social contract to korean"
   git push
</pre>

<h3>새 디렉터리 추가하는 예</h3>

<p>이 예시는 한국어 번역에 intro/ 디렉터리를 추가하는 것을 보여줍니다:

<pre>
   cd webwml/korean
   mkdir intro
   cd intro
   cp ../Makefile .
   git add Makefile
   git commit -m "added the intro dir to git"
   git push
</pre>

새 디렉터리는 Makefile을 갖고 git에 커밋되었는지 확인하세요.
그러지 않으면, make 돌릴 때 그것을 시도하는 다른 모든 사람에게 에러 낼 겁니다.

<h3>충돌 예</h3>

<p>
이 예는 마지막 <kbd>git pull</kbd> 이후 저장소 사본이 바뀌어서 commit이 동작하지 않는 예를 보여준다.</p>

<p>
 파일 foo.wml을 변경했습니다. 그 다음:</p>

 <pre>
    git add foo.wml
    git commit -m "fixed a broken link"
    git push
 </pre>

 출력은:

 <pre>
To salsa.debian.org:webmaster-team/webwml.git
 ! [rejected]                master -> master (fetch first)
error: failed to push some refs to 'git@salsa.debian.org:webmaster-team/webwml.git'
 </pre>

 <p>또는 그 비슷한 것이 될 겁니다 :)
       <br />
       <br />
충돌 때문에, 변경이 git 저장소에 push <strong>안</strong> 되었음을 뜻합니다.
       <br />
무엇이 잘못되었는지 조사하고 충돌을 해결하고 commit 및 push를 다시 시도해야 할 겁니다.</p>

